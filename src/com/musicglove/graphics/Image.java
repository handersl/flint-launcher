package com.musicglove.graphics;

import java.util.ArrayList;

public class Image {

	public Sprite spriteRef;
	public float[] position = {0, 0, 0}; // x, y, r
	private float[] origPos = {0, 0, 0};
	private float[]	prevPos = {0, 0, 0};
	public float scaleX;
	public float scaleY;
	public boolean timed;
	private int	timer;
	public Boolean hidden;
	public ArrayList<MotionCue> motionQueue;
	private int motionTimer;

	//######################################################################
	public Image (Sprite spriteReference, float xPos, float yPos) {
		this(spriteReference, xPos, yPos, false, 0, 0.0f, 1.0f, 1.0f);
	}

	//######################################################################
	public Image (Sprite spriteReference, float inxPos, float inyPos, boolean intimed, int intimer, float inrotation, float inscaleX, float inscaleY ) {       
		spriteRef = spriteReference;                                                          
		position[0] = inxPos;
		position[1] = inyPos;                                                                
		position[2] = inrotation; 
		prevPos[0] = position[0];                                                              
		prevPos[1] = position[1];
		prevPos[2] = position[2];
		origPos[0] = position[0];                                                              
		origPos[1] = position[1];
		origPos[2] = position[2];
		scaleX = inscaleX;                                                                  
		scaleY = inscaleY;                                                                  
		timed = intimed;                                                                  
		timer = intimer;                                                                    
		hidden = false;                                                                   
		motionQueue = new ArrayList<MotionCue>();                                            
		motionTimer = 0;                                                               
	}

	//######################################################################
	public void clear() {
		if(motionQueue!=null){motionQueue.clear();}
		if(spriteRef!=null){spriteRef.clear();}
		motionQueue=null;
		spriteRef=null;
	}

	//######################################################################
	public void nextMotion (  ) {                                                    
		motionTimer -= motionQueue.get(0).endTime;
		prevPos[0] = position[0];                                                
		prevPos[1] = position[1];
		prevPos[2] = position[2];
		if ( motionQueue.size() > 0 ) {                                                         
			if ( motionQueue.get(0).persistance != 0 ) {                                        
				if ( motionQueue.get(0).persistance > 0 ) {                                     
					motionQueue.get(0).persistance -= 1;                                        
				}
				motionQueue.add(motionQueue.get(0));                                      
			}
			motionQueue.remove(0);//popleft();                                        
		}
	}

	//######################################################################
	public void addMotion ( float dx, float dy, float dr, int time, int loop, int persistance, Boolean proceed, int ease, Boolean easeIn ) {          
		motionQueue.add( new MotionCue(dx, dy, dr, time, loop, persistance, proceed, ease, easeIn) );        
	}

	//######################################################################
	public void addMotion ( float dx, float dy, float dr, int time, int loop, int persistance) {                 
		addMotion( dx, dy, dr, time, loop, persistance, true, 1,true);                                   
	}
	
	//######################################################################
	public void addMotion ( float dx, float dy, float dr, int time) {                                    
		addMotion( dx, dy, dr, time, 0, 0, true, 1,true);                                          
	}
	
	//######################################################################
	public void prependMotion ( float dx, float dy, float dr, int time, int loop, int persistance, Boolean proceed, int ease, Boolean easeIn ) {       
		//nextMotion();                                                                                              
		motionQueue.add(0, new MotionCue(dx, dy, dr, time, loop, persistance, proceed, ease, easeIn) );           
	}

	//######################################################################
	public void setMotion ( float dx, float dy, float dr, int time, int loop, int persistance, Boolean proceed, int ease, Boolean easeIn ) {          
		motionQueue.clear();                                                                                                
		motionTimer = 0;                                                                                                    
		prevPos[0] = position[0];                                                                
		prevPos[1] = position[1]; 
		prevPos[2] = position[2]; 
		motionQueue.add( new MotionCue(dx, dy, dr, time, loop, persistance, proceed, ease, easeIn) );             
	}

	//######################################################################
	public void setMotion( float dx, float dy, float dr, int time) {
		setMotion(dx,  dy, dr, time, 0, 0, true, 1, true);
	}
	
	//######################################################################
	public void runMotion ( long tickTime ) {                                                                                    
		if ( motionQueue.size() > 0 ) {                                                                                    
			motionTimer += tickTime;                                                                                   
			position = motionQueue.get(0).getXYR(prevPos[0], prevPos[1], prevPos[2], motionTimer);
			if ( motionTimer > motionQueue.get(0).endTime ) {                                              
				if ( motionQueue.get(0).loopCount != 0 ) {                                                 
					motionTimer = 0;                                                         
					prevPos[0] = position[0];                                                 
					prevPos[1] = position[1]; 
					prevPos[2] = position[2]; 
					if ( motionQueue.get(0).loopCount > 0 ) {                                                   
						motionQueue.get(0).loopCount -= 1;                                                      
					}
				}
				else if ( motionQueue.get(0).loopCount == 0 ) {                           
					if ( motionQueue.get(0).proceed ) {                                    
						nextMotion();                                         
					}
				}
			}
		}
	}

	//######################################################################
	public void tick ( long tickTime ) {                              
		if ( timed && timer > 0 ) {                          
			timer -= tickTime;                                   
		}
		runMotion(tickTime);                                                 
	}

	//######################################################################
	public void resetCoordinates() {
		position[0] = origPos[0];
		position[1] = origPos[1];
	}
	
	//######################################################################
	public Boolean isDone ( ) {                                                        
		return ((timed) && (timer <= 0));                                    
	}
	
	//######################################################################
	public void addMotionSway ( float x, float y, float r, int t, int e ) {    
		addMotion( x/2f,  y/2f,  r/2f, t/4, 0, -1,true,e,false);                         
		addMotion(-x/2f, -y/2f, -r/2f, t/4, 0, -1,true,e,true);                          
		addMotion(-x/2f, -y/2f, -r/2f, t/4, 0, -1,true,e,false);                         
		addMotion( x/2f,  y/2f,  r/2f, t/4, 0,- 1,true,e,true);                         
	}
	
	//######################################################################
	public void addMotionSwayHalf ( float x, float y, float r, int t, int e ) {      
		addMotion(x/2f, y/2f, r/2f, t/4, 0, -1,true,e,true);                                     
		addMotion(x/2f, y/2f, r/2f, t/4, 0, -1,true,e,false);                                    
	}

} // END