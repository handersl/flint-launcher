// Converted from user.py

package com.musicglove.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.musicglove.helper.PassedValues;

public class User {

	private Context context;
	protected String currentUser = null;
	protected Boolean userIsSelected = false;
	public String currentSong = null;
	public String previousSong = null;
	public Boolean isSessionMode = false;
	public long sessionStartTime = 0;
	public int sessionLength = 0;
	public int songsPlayedThisSession = 0;
	public int difficulty = -1;
	public int numGrips = 3;
	public int hand = LEFT;
	private String password = null;
	private ArrayList<String> userList;
	private ArrayList<Integer> gripDictionary;
	private int h;
	private List<String> songs;

	public final static int EASY = 1;
	public final static int MEDIUM = 2;
	public final static int HARD = 3;
	public final static int ASSESSMENT = 4;

	public final static int RIGHT = 1;
	public final static int LEFT = 0;


	public final Boolean RESEARCH_VERSION = true;
	public final Boolean CLINICAL_VERSION = true;

	//######################################################################
	public User (Context c, PassedValues values) {
		context = c;
		songs = new ArrayList<String>();
		userList = new ArrayList<String>();
		gripDictionary = new ArrayList<Integer>();
		currentUser = values.userName;
		numGrips = values.numGrips;
		userIsSelected = false;
		loadUsers();
		currentSong = values.songName;
		previousSong = values.previousSong;
		isSessionMode = values.isSessionMode;
		sessionLength = values.sessionLength;
		sessionStartTime = values.sessionStartTime;
		songsPlayedThisSession = values.songsPlayedThisSession;
		if (values.difficulty != null) {
			if (values.difficulty.equals("easy")) {
				difficulty = User.EASY;
			} else if (values.difficulty.equals("medium")) {
				difficulty = User.MEDIUM;
			} else if (values.difficulty.equals("hard")) {
				difficulty = User.HARD;
			} else if (values.difficulty.equals("assessments")) {
				difficulty = User.ASSESSMENT;
			} else {
				difficulty = -1;
			}
		} else {
			difficulty = -1;
		}
		if (values.numGrips >= 3 && values.numGrips <=5) {
			numGrips = values.numGrips;
		}
		hand = User.LEFT;
		password = null;
 	}
	
	//######################################################################
	public void clear() {
		if (userList!=null) {userList.clear();}
		if (gripDictionary!=null) {gripDictionary.clear();}
		if (songs!=null) {songs.clear();}
		userList=null;
		gripDictionary=null;
		songs=null;
	}

	//######################################################################
	public PassedValues setValues (PassedValues values) {
		values.userName = currentUser;
		values.numGrips = numGrips;
		values.songName = currentSong;
		values.fullPath = values.gameMode+"_"+values.difficulty+"_"+values.songName;
		values.isSessionMode = isSessionMode;
		values.songsPlayedThisSession = songsPlayedThisSession;
		values.sessionLength = sessionLength;
		values.sessionStartTime = sessionStartTime;
		return values;
	}
	
	//######################################################################
	public void loadUsers (  ) {
		DataFile users = new DataFile(context, "users.dat", "file_users_dat", ":");
		users.loadData();
		users.resetIndex();
		for (int j=0; j<users.dataLines.size(); j++) {
			addUserGrip(users.dataLines.get(j).get(0), Integer.parseInt(users.dataLines.get(j).get(1)));
			}
		if (currentUser != null) {
			if (userList.indexOf(currentUser)>=0) {
				numGrips = gripDictionary.get(userList.indexOf(currentUser));
				userIsSelected = true;
			}
		}
	}

	//######################################################################
	public String getPassword (  ) {
		return this.password;

	}

	//######################################################################
	public Boolean checkPassword ( String chkPassword ) {
		DataFile info = new DataFile(context, "info.dat", "file_info_dat", ":");
		info.loadData();
		if (chkPassword == info.getPart(1,"password")) {
			return true;
		} else {
			return false;
		}
	}

	//######################################################################
	public String getUsername (  ) {
		return this.currentUser;

	}

	//######################################################################
	public String getHandString (  ) {
		if ( h == User.LEFT ) {
			return "Left";
		} else {
			return "Right";
		}
	}

	//######################################################################
	public String[] getUsers (  ) {
		return (String[]) userList.toArray();
	}

	//######################################################################
	public void createNewUser ( String username, int grips ) {
		DataFile users = new DataFile(context, "users.dat", "file_users_dat", ":");
		users.loadData();
		users.addParts(username, Integer.toString(grips));
		users.saveData();
		loadUsers();
	}

	//######################################################################
	public void deleteUser ( String username ) {
		DataFile users = new DataFile(context, "users.dat", "file_users_dat", ":");
		users.loadData();
		users.removeLine(users.findLine(username));
		users.saveData();
		loadUsers();
	}

	//######################################################################
	public int getGrips (  ) {
		int index = userList.indexOf(currentUser);
		if ( index >= 0 ) {
			return gripDictionary.get(index);
		} else {
			return 3;
		}
	}

	//######################################################################
	public String getCurrentSongPath (  ) {
		if ( this.difficulty == User.ASSESSMENT ) {
			return "songs_assessments_"+currentSong;
		} else if ( this.difficulty == User.EASY ) {
			return "songs_easy_"+currentSong;
		} else if ( this.difficulty == User.MEDIUM ) {
			return "songs_medium_"+currentSong;
		} else if ( this.difficulty == User.HARD ) {
			return "songs_hard_"+currentSong;
		} else {
			return null;
		}
	}
	
	//######################################################################
	public void addUserGrip(String user, int grips) {
		userList.add(user);
		gripDictionary.add(grips);
	}

	//######################################################################
	public void removeUserGrip (String user) {
		int index = userList.indexOf(user);
		if (index >=0) {
			userList.remove(index);
			gripDictionary.remove(index);
		}
	}

	//######################################################################
	public void removeUserGrip (int index) {
		if (index >=0) {
			userList.remove(index);
			gripDictionary.remove(index);
		}
	}

	//######################################################################
	public void setGrip(int numGrip) {
		this.numGrips = numGrip;
		int index = userList.indexOf(currentUser);
		if (index >= 0) {
			gripDictionary.set(index,numGrips);
		}
	}

} // END



