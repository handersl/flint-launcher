package com.musicglove.flintlauncher;

import java.util.ArrayList;
import java.util.List;

import com.flint.flintlauncher.R;
import com.musicglove.helper.PassedValues;
import com.musicglove.scenes.ActivityMainMenuScene;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity 
{
	private boolean runningDevice;
	
	public void onCreate(Bundle savedInstanceState) 
	{
		//Set up the window.
		super.onCreate(savedInstanceState);
		super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		runningDevice = true;
		loadDeviceScreen();
	}
	
	private void loadDeviceScreen()
	{
		//Grab the values.
		final PassedValues values = new PassedValues();
		values.receiveValues(getIntent().getExtras());
		
		//Used to place the buttons on the highest level.  Main layout.
		final RelativeLayout buttonLayout = new RelativeLayout(this);
		LinearLayout.LayoutParams paramMaster = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
		buttonLayout.setLayoutParams(paramMaster);
		buttonLayout.setGravity(Gravity.CENTER_VERTICAL);
		setContentView(buttonLayout);
		
		//Welcome message.
		TextView name = new TextView(this);
		name.setText("Welcome " + values.userName + "!");
		name.setTextSize(48);
		name.setX(0);
		name.setY(0);
		buttonLayout.addView(name);
		
		Button backButton = new Button(this);
		backButton.setBackgroundResource(R.drawable.backbutton);
		//TODO hardcoded for now.
		backButton.setX(1720);
		backButton.setY(1006);
		backButton.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				Intent intent = new Intent(MainActivity.this, ActivityMainMenuScene.class);
				startActivity(intent);
				finish();
			}
		});
		buttonLayout.addView(backButton);
		
		//The main layout to hang the lists on.  Child of the buttonLayout.
		final LinearLayout layoutMaster = new LinearLayout(this);
		layoutMaster.setOrientation(LinearLayout.VERTICAL);
		layoutMaster.setLayoutParams(paramMaster);
		buttonLayout.addView(layoutMaster);
		
		//TODO find out what devices are set and make sure there is more than one. Else just show/play the game.
		if(true)
		{
			//TODO find out how many devices are there are.
			int numberOfDevices = 6;
			
			//Two layouts that are child of the layoutMaster.
			final LinearLayout layoutTop = new LinearLayout(this);
			final LinearLayout layoutBottom = new LinearLayout(this);
			
			//Setup the layouts.
			//If there are the only three devices than we only need one row in the middle.
			if(numberOfDevices <= 3)
			{
				LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT, 1f);
				
				layoutTop.setLayoutParams(param);
				layoutTop.setGravity(Gravity.CENTER);
				layoutMaster.addView(layoutTop);
			}
			//Else we will need two.
			else
			{
				LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT, 1f);
				
				layoutTop.setLayoutParams(param);
				layoutTop.setGravity(Gravity.CENTER);
				layoutMaster.addView(layoutTop);
				
				layoutBottom.setLayoutParams(param);
				layoutBottom.setGravity(Gravity.CENTER);
				layoutMaster.addView(layoutBottom);
			}
			
			//Go through all the devices.
			for(int deviceNumber = 0; deviceNumber < numberOfDevices; deviceNumber++)
			{
				//Add 3 buttons to the top layout.
				//TODO find out the names of the devices(or whatever) to get the image.
				if(deviceNumber < 3)
				{
					final Button deviceButton = new Button(this);
					deviceButton.setBackgroundResource(R.drawable.device);
					deviceButton.setOnClickListener(new View.OnClickListener() 
					{	
						@Override
						public void onClick(View v) 
						{
							deviceButton.setBackgroundResource(0);
							layoutMaster.removeAllViews();
							layoutTop.removeAllViews();
							runningDevice = false;
							//TODO get the right menu for the right set of games.
							loadAct("musicglove");
						}
					});
					layoutTop.addView(deviceButton);
				}
				//Add the last 3 to the bottom layout.
				else
				{	
					final Button deviceButton = new Button(this);
					deviceButton.setBackgroundResource(R.drawable.device);
					deviceButton.setOnClickListener(new View.OnClickListener() 
					{	
						@Override
						public void onClick(View v) 
						{
							deviceButton.setBackgroundResource(0);
							layoutMaster.removeAllViews();
							layoutTop.removeAllViews();
							layoutBottom.removeAllViews();
							runningDevice = false;
							//TODO get the right menu for the right set of games.
							loadAct("musicglove");
						}
					});
					layoutBottom.addView(deviceButton);
				}
			}
		}
	}
	
	private void loadAct(String appPackName)
	{
		//Grab the values.
		final PassedValues values = new PassedValues();
		values.receiveValues(getIntent().getExtras());
		
		//Get the size of the screen.
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		final int screenWidth = size.x;
		
		//TODO find out what apps are installed by app package name(?) or some other method.
		
		//If there is more than one than display this or else just start the one.
		//Find out how many games there are using the package manager.
		final PackageManager pm = getPackageManager();
		
		//Get a list of installed apps.
		List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		final ArrayList<Intent> installedAppIntents = new ArrayList<Intent>();
		final ArrayList<Drawable> applicationDrawable = new ArrayList<Drawable>();
		
		//Go through all of the apps and look for the app package given.
		for (final ApplicationInfo packageInfo : packages) 
		{
			//TODO Remove this once icons are in all games.
			if(/*packageInfo.packageName.contains(appPackName) && !*/packageInfo.packageName.contains("flintlauncher"))
			{
				installedAppIntents.add(getPackageManager().getLaunchIntentForPackage(packageInfo.packageName));
				
				try {
					//TODO This commented out code is another way to get a (more blurry) icon image.
				    //Context otherAppCtxt = this.createPackageContext(packageInfo.packageName, Context.CONTEXT_IGNORE_SECURITY);
				    //applicationDrawable.add(otherAppCtxt.getResources().getDrawableForDensity(packageInfo.icon, DisplayMetrics.DENSITY_XXHIGH));
				    Resources res = this.getPackageManager().getResourcesForApplication(packageInfo.packageName);
				    applicationDrawable.add(res.getDrawable(res.getIdentifier("icon", "drawable", packageInfo.packageName)));
				} catch (PackageManager.NameNotFoundException e) {
				    e.printStackTrace();
				}
			}
		}
		
		//Used to place the buttons on the highest level.
		final RelativeLayout buttonLayout = new RelativeLayout(this);
		LinearLayout.LayoutParams paramMaster = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
		buttonLayout.setLayoutParams(paramMaster);
		buttonLayout.setGravity(Gravity.CENTER_VERTICAL);
		
		//Place the layoutMaster(which houses the layout with 2 horizontal layouts).  Gets horizontal layouts.
		final LinearLayout layoutHigherMaster = new LinearLayout(this);
		layoutHigherMaster.setLayoutParams(paramMaster);
		
		//The back button,
		Button backButton = new Button(this);
		backButton.setBackgroundResource(R.drawable.backbutton);
		//TODO hardcoded for now.
		backButton.setX(1720);
		backButton.setY(969);
		backButton.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				loadDeviceScreen();
			}
		});
		buttonLayout.addView(backButton);
			
		//TODO Replace with real check for more than one game by checking is the list of installed
		//packages has more than one game in it.
		if(true)
		{	
			//TODO Replace with real size. installedAppIntents.size();
			int numberOfGames = 11;
			
			//Create horizontal scroll.  This may ultimately hold the layoutHigherMaster above if we need it to.
			//If not it will not be used.
			final HorizontalScrollView scroll = new HorizontalScrollView(this);
			//Hide scroll bars.
			scroll.setVerticalScrollBarEnabled(false); 
			scroll.setHorizontalScrollBarEnabled(false);
			//Move it to the far left.
			scroll.setScrollX(0);
			//Disable the scrolling through swiping by intercepting the command.
			scroll.setOnTouchListener( new OnTouchListener()
			{ 
			    @SuppressLint("ClickableViewAccessibility")
				@Override
			    public boolean onTouch(View v, MotionEvent event) 
			    {
			        return true; 
			    }
			});
			
			//This layout holds a series of layouts that hold a 2 row layout.
			
			//If there are only six games than we only need one screen so we don't need to use
			//the scroll, only the linear layout(layoutHigherMaster).
			if(numberOfGames < 7)
			{
				//We will setup the layout much similarly to the top.
				layoutHigherMaster.setOrientation(LinearLayout.VERTICAL);
				//Pad to make the layout lower and match the device layout.
				layoutHigherMaster.setPadding(0,30,0,0);
				setContentView(layoutHigherMaster);
				Log.d("Han", "Only need one view.");
			}
			//Setup for the scroll.
			else
			{
				//We need to fit the layoutHigherMaster in the horizontal scroll so we need to layout UI elements
				//in a horizontal fashion.
				layoutHigherMaster.setOrientation(LinearLayout.HORIZONTAL);
				scroll.addView(layoutHigherMaster);
				buttonLayout.addView(scroll);
				
				setContentView(buttonLayout);
				Log.d("Han", "Setting up scroll.");
			}
			
			//Find out how many panels we will need.
			int numberOfSets = 0;
			for(int i = 0; i < numberOfGames; i++)
			{
				if(i%6 == 0)
				{
					numberOfSets++;
				}
			}
			
			Log.d("Han", "Sets: " + numberOfSets);
			
			//For each panel create the two column layout similar to the device layout.
			for(int setOfSix = 0; setOfSix < numberOfSets;setOfSix++)
			{
				//The main layout to hang the two rows on.
				final LinearLayout layoutMaster = new LinearLayout(this);
				layoutMaster.setOrientation(LinearLayout.VERTICAL);
				LinearLayout.LayoutParams paramMasterLow = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f);
				layoutMaster.setLayoutParams(paramMasterLow);
				
				final LinearLayout layoutTop = new LinearLayout(this);
				final LinearLayout layoutBottom = new LinearLayout(this);
				
				//If there are only three games than we only need one row.
				if(numberOfGames <= 3)
				{
					LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT, 1f);
					
					layoutTop.setLayoutParams(param);
					layoutTop.setGravity(Gravity.CENTER);
					layoutMaster.addView(layoutTop);
				}
				//Else we need two rows.
				else
				{
					LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT, 1f);
					
					layoutTop.setLayoutParams(param);
					layoutTop.setGravity(Gravity.CENTER);
					layoutMaster.addView(layoutTop);
					
					layoutBottom.setLayoutParams(param);
					layoutBottom.setGravity(Gravity.CENTER);
					layoutMaster.addView(layoutBottom);
				}
				
				//Go through the games and create the buttons for them.  Stop when we hit six games(only six per panel) or when there are no more games.
				for(int gameNumber = setOfSix*6; gameNumber < numberOfGames || (gameNumber < setOfSix*6 + 6 && numberOfGames > setOfSix*6 + 6); gameNumber++)
					//Start from intervals of 6.          Stop when there are no games left or when we got through an interval of six.          Inc. count.
				{
					final int number = gameNumber;
					//TODO find out the names of the games(or whatever) to get the image.
					//If this is the first panel than gameNumber is < 3 so put it in the top row.
					//Or if we are starting not from 0 we do some calculations to see if this is
					//within 3 numbers of the starting point(setOfSix*6) by using 3 less from the ending(setOfSix*6 + 6).
					if(gameNumber < 3 || gameNumber < ((setOfSix*6 + 6) - 3))
					{
						//Make the button.
						final Button gameButton = new Button(this);
						gameButton.setBackground(resize(applicationDrawable.get(0), 400,400));
						gameButton.setOnClickListener(new View.OnClickListener() 
						{	
							@Override
							public void onClick(View v) 
							{
								gameButton.setBackgroundResource(0);
								layoutHigherMaster.removeAllViews();
								layoutTop.removeAllViews();
								//Load in the game.
								//TODO Replace with real value. number
								startActivity(installedAppIntents.get(0));
							}
						});
						layoutTop.addView(gameButton);
					}
					//Similar calculations here.  If starting from 0 than gameNumber < 6.
					//Or if not than calculate that it is less than the end(setOfSix*6 + 6).
					else if(gameNumber < 6 || gameNumber < setOfSix*6 + 6)
					{	
						final Button gameButton = new Button(this);
						gameButton.setBackgroundResource(R.drawable.gamebutton);
						gameButton.setOnClickListener(new View.OnClickListener() 
						{	
							@Override
							public void onClick(View v)
							{
								gameButton.setBackgroundResource(0);
								layoutHigherMaster.removeAllViews();
								layoutTop.removeAllViews();
								layoutBottom.removeAllViews();
								//Load in the game.
								//TODO Replace with real value. number
								startActivity(installedAppIntents.get(0));
							}
						});
						layoutBottom.addView(gameButton);
						
						//Add padding to make the space match with the device layout.
						layoutBottom.setPadding(0, 115, 0, 0);
					}
				}
				
				//Add padding to make sure that each six game takes up one page.
				//Two different padding depending on the number of games per page.
				//Conditions are < 3 games and more than or equal to 3 games.
				/*if(layoutTop.getChildCount() < 3)
				{*/
					//TODO hardcoded.
					//400 comes from image width.
					layoutMaster.setPadding(screenWidth/2 - ((layoutTop.getChildCount()*400)/2),0,screenWidth/2 - ((layoutTop.getChildCount()*400)/2),90);
					Log.d("Han", "End padding.");
				/*}
				else
				{
					//TODO hardcoded.
					//360 comes from screen width /2 - 3 * image width /2.  90 is for space purposes.
					layoutMaster.setPadding(360,0,360,90);
					Log.d("Han", "Continue padding.");
				}*/
				
				layoutHigherMaster.addView(layoutMaster);
			}
			//Button for left and right scroll.
			Button leftButton = new Button(this);
			leftButton.setX(0);
			//TODO hardcoded.
			leftButton.setY(340);
			leftButton.setBackgroundResource(R.drawable.leftbutton);
			leftButton.setOnClickListener(new View.OnClickListener() 
			{	
				@Override
				public void onClick(View v) 
				{
					scroll.setScrollX((int) (scroll.getScrollX() - screenWidth));
					Log.d("Han", "Scrolling left: " + scroll.getScrollX());
				}
			});
			buttonLayout.addView(leftButton);
			
			//Button for left and right scroll.
			Button rightButton = new Button(this);
			//TODO hardcoded.
			rightButton.setX(1720);
			rightButton.setY(340);
			rightButton.setBackgroundResource(R.drawable.rightbutton);
			rightButton.setOnClickListener(new View.OnClickListener() 
			{	
				@Override
				public void onClick(View v) 
				{
					scroll.setScrollX((int) (scroll.getScrollX() + screenWidth));
					Log.d("Han", "Scrolling right: " + scroll.getScrollX());
				}
			});
			buttonLayout.addView(rightButton);
		}
		else
		{
			//Load in the game if there is only one.
			startActivity(installedAppIntents.get(0));
		}
	}	
	
	private Drawable resize(Drawable image, int xSize, int ySize) 
	{
	    Bitmap b = ((BitmapDrawable)image).getBitmap();
	    Bitmap bitmapResized = Bitmap.createScaledBitmap(b, xSize, ySize, false);
	    return new BitmapDrawable(getResources(), bitmapResized);
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    loadDeviceScreen();
	}
	
	@Override
	public void onRestart() {
	    super.onRestart();
	    loadDeviceScreen();
	}
	
	@Override
	public void onDestroy() {
	    super.onDestroy();
	}
}
