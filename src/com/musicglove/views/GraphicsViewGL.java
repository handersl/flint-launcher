package com.musicglove.views;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Image;

public class GraphicsViewGL extends GraphicsView implements Renderer {
	
	private boolean flag = false;

	public GraphicsViewGL (Context context, Graphics g, int w, int h) {
		super(context);
		graphics = g;
		imageList  = new ArrayList<Image>();
		width = w;
		height = h;
	}

	
	@Override
	public void onDrawFrame(GL10 gl) {
		if (changing == false) {
			updated = false;
			renderGL(gl);
			updated = true;
		}
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		//Log.i("MG","onSurfaceChanged "+flag);
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL10.GL_PROJECTION);
		if (!flag) {
			GLU.gluPerspective(gl, 45.0f, (float)width / (float)height, ((float)height/2)/(float)Math.tan(0.3926991)-100f, ((float)height/2)/(float)Math.tan(0.3926991)+100f);
	        if (!imageList.isEmpty()) {
				for (i=0;i<imageList.size();i++) {
					if ( imageList.get(i) != null) {
						imageList.get(i).spriteRef.textureLoaded = false;
					}
				}
			}
	        flag = true;
		}
        gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);        
		gl.glDisable(GL10.GL_DEPTH_TEST);
		gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL10.GL_BLEND);
		gl.glFrontFace(GL10.GL_CCW);
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);        
		gl.glLoadIdentity();
		GLU.gluLookAt(gl, width/2, height/2, ((float)height/2)/(float)Math.tan(0.3926991), width/2, height/2, 0f, 0f, 1.0f, 0.0f); 
	}


	@Override
	public void onSurfaceCreated(GL10 gl, javax.microedition.khronos.egl.EGLConfig config) {
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
		gl.glShadeModel(GL10.GL_SMOOTH);
		gl.glClearDepthf(1.0f);
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);	
	}

	
	//######################################################################
	public void renderGL (GL10 gl) {  
		for (i=0;i<imageList.size();i++) {
			if ( imageList.get(i) != null && imageList.get(i).hidden == false ) {
				imageList.get(i).spriteRef.renderGL( gl, ((imageList.get(i).position[0]-graphics.xCenter)*graphics.scaleX+graphics.xCenter)*(float)width-(float)imageList.get(i).spriteRef.w/2f,  (1-((imageList.get(i).position[1]-graphics.yCenter)*graphics.scaleY+graphics.yCenter)) * (float)height-(float)imageList.get(i).spriteRef.h/2f, imageList.get(i).position[2], imageList.get(i).scaleX, imageList.get(i).scaleY );
			}
		}
	}
	
}
