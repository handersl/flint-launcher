package com.musicglove.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import android.content.Context;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.musicglove.data.DataFile;

public class InfoDat extends DataFile {
	private static final String TAG = "MG_InfoDat";
	private String SHAHash;
	
	// backdoor password in case someone forgets their password, they can contact you, and you have a way of resetting the password
	// without requiring them to re-install the app (and potentially lose all their data/settings)
	private static final String hiddenPassword = "ResetMusicGlovePassword"; 
	private static final String hiddenChangeToHomeVersion = "ChangeMusicGloveVersionToHomeVersion";
	private static final String hiddenChangeToClinicalVersion = "ChangeMusicGloveVersionToClincalVersion";
	private static final String hiddenChangeToResearchVersion = "ChangeMusicGloveVersionToResearchVersion";
	private static final String hiddenChangeToSlate = "ChangeMusicGloveSubversionToSlate";
	private static final String hiddenChangeToTablet = "ChangeMusicGloveSubversionToTablet";
	private static final String hiddenChangeToTabletGL = "ChangeMusicGloveSubversionToTabletGL";

	
	//######################################################################
	public InfoDat(Context context) {
		super(context, "info.dat", "file_info_dat", ":");
	}
	
	//######################################################################
	@Override
	public void loadData() {
		File sdCard = Environment.getExternalStorageDirectory();
		File directory = new File (sdCard.getAbsolutePath()+"/MusicGlove/saves");
		try {
			File file = new File(directory, fileName);
			FileInputStream fIn = new FileInputStream(file);
			if (fIn != null ) {
				InputStreamReader inputStreamReader = new InputStreamReader(fIn);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            while ( (receiveString = bufferedReader.readLine()) != null ) {
        			String[] parts = receiveString.split(separator);
        			ArrayList<String> arrayPart = new ArrayList<String>();
        			for (String part : parts) {
        				arrayPart.add(part);
        			}
        			dataLines.add(arrayPart);
	            }
	            inputStreamReader.close();
			} 
		} catch (FileNotFoundException e) {
	        Log.e(TAG, "File not found: " + e.toString());
	        loadDefault();
	    } catch (IOException e) {
	        Log.e(TAG, "Can not read file: " + e.toString());
	        loadDefault();
	    }
	}
			
	//######################################################################
	@Override
	public void saveData() {
		File sdCard = Environment.getExternalStorageDirectory();
		File directory = new File (sdCard.getAbsolutePath()+"/MusicGlove/saves");
		directory.mkdirs();
		try {
			File file = new File(directory, fileName);
			FileOutputStream fOut = new FileOutputStream(file);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);
			outputStreamWriter.write(processOutData());
			outputStreamWriter.flush();
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e(TAG, "File write failed: " + e.toString());
		}
	}
	
	//######################################################################
	public void setDate(long date) {
		addUnique("date", Long.toString(date));		
	}
	
	//######################################################################
	public long getDate( ) {
		long myNum = 0; 	// replace with ordinal for jan 1, 2014

		try {
		    myNum = Long.parseLong(getPart(1,"date"));
		} catch(NumberFormatException nfe) {
		} 
		return myNum;	
	}
	
	//######################################################################
	public void setPassword(String password) {
		if (password != null) {
			addUnique("password", computeSHAHash(password));
		}		
	}
	
	//######################################################################
	public boolean checkPassword(String password) {	
		if (password.equals(hiddenChangeToHomeVersion)) {
			ConfigIni file = new ConfigIni(context);
			file.setVersion("home");
		} else if (password.equals(hiddenChangeToClinicalVersion)) {
			ConfigIni file = new ConfigIni(context);
			file.setVersion("clinical");
		} else if (password.equals(hiddenChangeToResearchVersion)) {
			ConfigIni file = new ConfigIni(context);
			file.setVersion("researc");
		} else if (password.equals(hiddenChangeToSlate)) {
			ConfigIni file = new ConfigIni(context);
			file.setSubversion("slate");
		} else if (password.equals(hiddenChangeToTablet)) {
			ConfigIni file = new ConfigIni(context);
			file.setSubversion("tablet");;
		} else if (password.equals(hiddenChangeToTabletGL)) {
			ConfigIni file = new ConfigIni(context);
			file.setSubversion("tabletGL");
		} else if (password.equals(hiddenPassword)) {
			addUnique("password", computeSHAHash("MusicGlove"));
		} else {
			if (getPart(1,"password").equals(computeSHAHash(password))) {
				return true;
			}
		}
		return false;
	}

	//######################################################################
	public String computeSHAHash(String password) {
		MessageDigest mdSha1 = null;
		try
		{
			mdSha1 = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e1) {
			Log.e(TAG, "Error initializing SHA1 message digest");
		}
		try {
			mdSha1.update(password.getBytes("ASCII"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		byte[] data = mdSha1.digest();
		try {
			SHAHash=convertToHex(data);
		} catch (IOException e) {
		}
		return SHAHash.trim();
	}
	
	//######################################################################
	private static String convertToHex(byte[] data) throws java.io.IOException {
		StringBuffer sb = new StringBuffer();
		String hex=null;
		hex=Base64.encodeToString(data, 0, data.length, 0);
		sb.append(hex);
		return sb.toString();
	}
	
} // END
