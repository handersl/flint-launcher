package com.musicglove.flintlauncher;

import java.io.File;

//import android.hardware.Camera.PreviewCallback;
//import android.util.Log;

//This class is similar to the Resource Folder class.
public class SDCard
{
	public boolean sdCardPresent;
	private boolean sdCardLoaded;
	//The path to the sdcard directory on the Android.
	public static String directory;
	
	private String SLATE_EXT = "/storage/sdcard1/";
	private String TABLET_EXT = "/mnt/external_sd/";
	
	public SDCard()
	{
		//Unfortunately there is a way to find out the external
		//storage reliably so it will have to be hard coded for each device.
	
		//The path for the sdcard on the All in One.
		File card = new File(SLATE_EXT);
		if(card.exists())
		{
			directory = SLATE_EXT;
		}
		//If the path is not valid then try to load
		//the path for the tablet.
		//The path for the tablet.
		else
		{
			//TODO this is now wrong and needs to be changed to the new tablets.
			card = new File(TABLET_EXT);
			directory = TABLET_EXT;
		}
		
		if(card.exists())
		{
			sdCardPresent = true;
		}
		else
		{
			sdCardPresent = false;
		}
		
		sdCardLoaded = false;
	}
	
	public void setSDCardState()
	{
		//The path for the sdcard on the All in One.
		File card = new File(SLATE_EXT + "LOST.DIR");
		if(card.exists())
		{
			sdCardPresent = true;
			directory = SLATE_EXT;
		}
		//The path for the tablet.
		else
		{
			card = new File(TABLET_EXT + "LOST.DIR");
			if(card.exists())
			{
				sdCardPresent = true;
				directory = TABLET_EXT;
			}
			else
			{
				sdCardPresent = false;
			}
		}
	}
	
	//Returns a list of raw file names as a list.
	public File[] getListOfFiles()
	{
		File card = new File(directory);
		File listFiles[] = card.listFiles();
		return listFiles;
	}
	
	public String getDir()
	{
		return directory;
	}
	
	public boolean isSDCardLoaded()
	{
		return sdCardLoaded;
	}
}