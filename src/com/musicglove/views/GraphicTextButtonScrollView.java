package com.musicglove.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.musicglove.graphics.Sprite;

// Scrollable view of clickable graphic items.
// The way the class works is that you can add a variable number of buttons 
// on top of the list (won't scroll).  Then run setupList to finalize them, 
// and permit adding of list items.  Adding buttons would then add them below.
// Implements a state machine as to whether an item is selected, toggled
// between one of two states, and using an alternate graphic.
// This means there are up to 8 images required for each list item.
public class GraphicTextButtonScrollView extends LinearLayout{

	private Context context;
	private LinearLayout innerContainer;
	private ScrollView scroll;
	protected LinearLayout itemContainer;
	public Sprite image;
	protected int idCounter = 0;
	
	protected List<GraphicTextButton> itemList;
	public List<String> descriptionList;
	public List<Integer> stateList;

	public int activeIndex; // currently selected item
	private int activeColor;
	private int inactiveColor;
	
	public boolean changedSelection;	
	
	// binary logic. 
	public static final byte SELECTED = 0x01;
	public static final byte TOGGLE = 0x02;
	public static final byte ALTERNATES= 0x04;
	// the order of images in the graphicbutton need to be
	// unselected 0 - state 0 - imagetype 0
	// selected   1   - state 0 - imagetype 0
	// unselected 0 - state 1 - imagetype 0
	// selected   1   - state 1 - imagetype 0
	// unselected 0 - state 0 - imagetype 1
	// selected   1   - state 0 - imagetype 1
	// unselected 0 - state 1 - imagetype 1
	// selected   1   - state 1 - imagetype 1
	
	public boolean toggleMode = true;
	//if not in toggle mode, the calling code needs to manage the states
	//it is assumed state 0 is unselected and state 1 is selected.
	// any other states need to be overridden by the calling code
	//(this was necessary because the analytics scene doesn't want toggle features)
	
	//######################################################################
	public GraphicTextButtonScrollView (Context contxt) {
		this(contxt, null, 0, 0, 0, 0);
	}
	
	//######################################################################
	public GraphicTextButtonScrollView (Context contxt, Sprite inImage, int padleft, int padtop, int padright, int padbottom) {
		super(contxt);
		context = contxt;
		activeIndex = -1;
		changedSelection = false;
		image = inImage;
		itemList = new ArrayList<GraphicTextButton>();
		descriptionList = new ArrayList<String>();
		stateList = new ArrayList<Integer>();

		LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(image.w, image.h);
		this.setOrientation(LinearLayout.VERTICAL);
		this.setBackground(new BitmapDrawable(getResources(), image.bitmap));
		this.setLayoutParams(Lparams);
		
		innerContainer = new LinearLayout(context);
		Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		innerContainer.setOrientation(LinearLayout.VERTICAL);
		innerContainer.setLayoutParams(Lparams);
		innerContainer.setPadding(padleft, padtop, padright, padbottom);
		this.addView(innerContainer);
		
		activeColor = Color.YELLOW;
		inactiveColor = Color.BLACK;
	}
	
	//######################################################################
	public void clear() {
		if(innerContainer!=null){innerContainer.removeAllViews();}
		if(scroll!=null){scroll.removeAllViews();}
		if(itemContainer!=null){itemContainer.removeAllViews();}
		if(image!=null){image.clear();}
		if(itemList!=null){
			for (int i=0; i<itemList.size(); i++) {
				itemList.get(i).clear();
			}
			itemList.clear();
		}
		if(descriptionList!=null){descriptionList.clear();}
		if(stateList!=null){stateList.clear();}
		innerContainer=null;
		scroll=null;
		itemContainer=null;
		image=null;
		itemList=null;
		descriptionList=null;
		stateList=null;
	}
	
	//######################################################################
	public void setColors(int active, int inactive) {
		activeColor = active;
		inactiveColor = inactive;
	}
	
	//######################################################################
	public void addButton(GraphicButton button) {
		LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		button.setLayoutParams(Lparams);
		innerContainer.addView(button);
	}
	
	//######################################################################
	public void addButton(GraphicTextButton button) {
		LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		button.setLayoutParams(Lparams);
		innerContainer.addView(button);
	}

	//######################################################################
	public void setupList() {
		scroll = new ScrollView(context);
		scroll.setScrollbarFadingEnabled(false);
		itemContainer = new LinearLayout(context);
		LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		itemContainer.setOrientation(LinearLayout.VERTICAL);
		itemContainer.setLayoutParams(Lparams);
		scroll.addView(itemContainer, Lparams);
		innerContainer.addView(scroll);
	}
	
	//######################################################################
	public void addListItem(GraphicTextButton item, String description) {
		addListItem(item, description, true);
	}
	
	//######################################################################
	public void addListItem(GraphicTextButton item, String description, Boolean clickable) {
		item.setId(1000+idCounter);  //button id is 1000 + idCounter
		idCounter++;
		itemList.add(item);
		stateList.add(0);
		descriptionList.add(description);
		LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(item.w, item.h);
		Lparams.setMargins(0, 1, 0, 1);
		item.setLayoutParams(Lparams);
		itemContainer.addView(item);
		if (clickable) {
			item.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					clicked(v);
				}
			});
		}
	}
	
	//######################################################################
	public void clicked(View v) {
		int i;
		for (i=0;i<itemList.size();i++) {
			if (v.getId() == itemList.get(i).getId()) {
				break;
			}
		}
		if (activeIndex != -1) {
			if (toggleMode) {
				stateList.set(activeIndex, stateList.get(activeIndex) & ~SELECTED);
			} else {
				stateList.set(activeIndex, 0);
			}
			itemList.get(activeIndex).setState(stateList.get(activeIndex));
			itemList.get(activeIndex).setTextColor(inactiveColor);
		}
		activeIndex = i;
		if (toggleMode) {
			stateList.set(activeIndex, (stateList.get(activeIndex) | SELECTED));
		} else {
			stateList.set(activeIndex, 1);
		}
		itemList.get(activeIndex).setState(stateList.get(activeIndex));
		itemList.get(activeIndex).setTextColor(activeColor);
		changedSelection = true;
	}
	
	//######################################################################
	public String getSelected() {
		if (activeIndex != -1) {
			return itemList.get(activeIndex).text;
		} else {
			return null;
		}
	}
	
	//######################################################################
	public String getDescription() {
		if (activeIndex != -1) {
			return descriptionList.get(activeIndex);
		} else {
			return null;
		}
	}
	
	//######################################################################
	public Boolean changedSelection() {
		if (changedSelection == true) {
			changedSelection = false;
			return true;
		} else {
			return false;
		}
	}
	
	//######################################################################
	public Boolean doesExist(String test) {
		for (int i=0; i<itemList.size(); i++) {
			if (itemList.get(i).text.equals(test)) {
				return true;
			}
		}
		return false;
	}
	
	//######################################################################
	public void setSelected(String name) {
		if (descriptionList.size() > 0) {
			int index = descriptionList.indexOf(name);
			if (index >= 0) {
				if (activeIndex != -1) {
					if (toggleMode) {
						stateList.set(activeIndex, stateList.get(activeIndex) & ~SELECTED);
					} else {
						stateList.set(activeIndex, 0);
					}
					itemList.get(activeIndex).setState(stateList.get(activeIndex));
					itemList.get(activeIndex).setTextColor(inactiveColor);
				}
				activeIndex = index;
				if (toggleMode) {
					stateList.set(activeIndex, (stateList.get(activeIndex) | SELECTED));
				} else {
					stateList.set(activeIndex, 1);
				}
				itemList.get(activeIndex).setState(stateList.get(activeIndex));
				itemList.get(activeIndex).setTextColor(activeColor);
				changedSelection = true;
			}
		}
	}
	
	//######################################################################
	public void refresh() { // use for toggle mode
		if (itemList.size() < 1) {
			return;
		}
		for (int i=0; i<itemList.size(); i++) {
			if (ALTERNATES*(i%2) == 0) {
				stateList.set(i, (stateList.get(i) & ~ALTERNATES) & ~SELECTED);
			} else {
				stateList.set(i, (stateList.get(i) | ALTERNATES) & ~SELECTED);
			}
			stateList.set(i, (stateList.get(i) | (ALTERNATES*(i%2))) & ~SELECTED);
			itemList.get(i).setState(stateList.get(i));
			itemList.get(i).setTextColor(inactiveColor);
		}
		if (activeIndex >= 0) {
			stateList.set(activeIndex, stateList.get(activeIndex) | SELECTED);
			itemList.get(activeIndex).setState(stateList.get(activeIndex));
			itemList.get(activeIndex).setTextColor(activeColor);
		}
	}
	
	//######################################################################
	public void reset() { // use for not toggle mode
		if (itemList.size() < 1) {
			return;
		}
		for (int i=0; i<itemList.size(); i++) {
			itemList.get(i).setState(stateList.get(i));
			itemList.get(i).setTextColor(inactiveColor);
		}
		if (activeIndex >= 0) {
			itemList.get(activeIndex).setTextColor(activeColor);
		}
	}

} // END
