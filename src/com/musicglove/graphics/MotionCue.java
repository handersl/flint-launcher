package com.musicglove.graphics;



public class MotionCue {             

	private float dX;
	private float dY;
	private float dR;
	public int endTime;
	private int loop;
	public int loopCount;
	public int persistance;
	public Boolean proceed;
	private int ease;
	private Boolean easeIn;
	private float percent;
	private float[] args = {0, 0, 0};

	//######################################################################
	public MotionCue ( float dx, float dy, float dr, int time, int inloop, int inpersistance, Boolean inproceed, int inease, Boolean ineaseIn ) {  
		dX = dx;       				// #how far this motion goes in the x direction
		dY = dy;                       // #how far this motion goes in the y direction
		dR = dr;                       // #how much this motion rotates the object
		endTime = time;                // #how long this animation cue takes to complete its motion
		loop = inloop;                   // #How many times this cue loops before stopping, and proceeding if ON_LOOP. -1=indefinite
		loopCount = loop;         // self.loopCount = self.loop    
		persistance = inpersistance;     // #How many times this cue remains in the queue after proceeding. -1=indefinite
		proceed = inproceed;             // #Determines whether the animation automatically goes onto the next cue when done
		ease = inease;                   // #Determines how much acceleration goes into the tween
		easeIn = ineaseIn;               // #Determines whether the motion is accelerating (in, True) or decelerating (out, False)
	}

	//######################################################################
	public float[] getXYR ( float baseX, float baseY, float baseR, long timer ) { 
		if ( endTime == 0 || timer >= endTime  ) {
			percent = 1.0f; 
		} else if ( easeIn ) {    
			percent = (float) Math.pow( (float) timer / (float) endTime, (float) (ease));    
		} else {    
			percent = 1.0f -  (float) Math.pow( 1.0f -((float)timer / (float)endTime), (float)(ease));                                                                               
		}
		if (percent < 0) {percent = 0;}
		else if (percent > 1) {percent = 1;}
		args[0] = (baseX+dX*percent);
		args[1] = (baseY+dY*percent);
		args[2] = (baseR+dR*percent);
		return args;     
	}
	
} // END


