package com.musicglove.data;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

// Basic data file class.  Takes a filename and a default filename stored in the apk.
public class DataFile {
	
	private static final String TAG = "MG_DataFile";
	
	protected Context context;
	protected String fileName;
	private String defaultName;
	protected String separator;
	public List<List<String>> dataLines;
	public List<String> currentLine;
	private int currentIndex;
	
	//######################################################################
	public DataFile (Context context, String name, String defName, String sep) {
		this.context = context;
		fileName = name;
		defaultName = defName;
		separator = sep;
		dataLines = new ArrayList<List<String>>();
		currentLine = new ArrayList<String>();
		currentIndex = -1;
	}
	
	//######################################################################
	public void clear () {
		if (dataLines!=null) {
			for (int i=0; i<dataLines.size(); i++) {
				dataLines.get(i).clear();
			}
			dataLines.clear();
		}
		if (currentLine!=null) {currentLine.clear();}
		dataLines = null;
		currentLine = null;
	}
	
	//######################################################################
	// just directly load the default file
	public void loadDefault() {
		dataLines.clear();
		currentLine.clear();
		if (defaultName != null) {
			Log.d("Han", defaultName + "raw" + context.getPackageName());
			loadData(context.getResources().openRawResource(context.getResources().getIdentifier(defaultName, "raw", context.getPackageName())));
		}
		resetIndex();
	}
	
	//######################################################################
	// load a file from the res/raw folder of the apk
	public void loadRaw() {
		dataLines.clear();
		currentLine.clear();
		if (fileName != null) {
			loadData(context.getResources().openRawResource(context.getResources().getIdentifier(fileName, "raw", context.getPackageName())));
		} else {
			loadDefault();
		}
		resetIndex();
	}
	
	//######################################################################
	// load a file from the SDCard.
	public void loadSDCard() {
		dataLines.clear();
		currentLine.clear();
		if (fileName != null) {
			File file = new File(fileName);
			FileInputStream FIS = null;
			try {
				FIS = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InputStream inputStream = new BufferedInputStream(FIS);
			loadData(inputStream);
			resetIndex();
		} else {
			loadDefault();
		}
		resetIndex();
	}
	//######################################################################
	// tries to load the main file, but if it fails, loads the default (which is in the apk)
	public void loadData() {
		try {
	        dataLines.clear();
	        currentLine.clear();
			InputStream inputStream = context.openFileInput(fileName);
			loadData(inputStream);
			resetIndex();
		} catch (FileNotFoundException e) {
	        Log.e(TAG, "File not found: " + e.toString());
	        Log.e(TAG, "Loading default ...");
	        loadDefault();
	    } 
	}
	
	//######################################################################
	// loads a specific file
	private void loadData(InputStream inputStream) {
		try {
			if (inputStream != null ) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            while ( (receiveString = bufferedReader.readLine()) != null ) {
        			String[] parts = receiveString.split(separator);
        			ArrayList<String> arrayPart = new ArrayList<String>();
        			for (String part : parts) {
        				arrayPart.add(part);
        			}
        			dataLines.add(arrayPart);
	            }
	            inputStream.close();
			}
		} catch (FileNotFoundException e) {
	        Log.e(TAG, "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e(TAG, "Can not read file: " + e.toString());
	    }
	}

	//######################################################################
	public void saveData() {
		try {
			if (dataLines != null) { // if dataLines equals null, don't do anything. Could occasionally not save data, but better that than wiping everything
		        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
		        outputStreamWriter.write(processOutData());
		        outputStreamWriter.flush();
		        outputStreamWriter.close();
			}
	    }
	    catch (IOException e) {
	        Log.e(TAG, "File write failed: " + e.toString());
	    }
	}
	
	//######################################################################	
	protected String processOutData() {
		String data = "";
		for (List<String> line : dataLines) {
			for (String part : line) {
				data += part + separator;
			}
			data = data.substring(0, data.length()-1); // remove last separator
			data += "\n";
		}
		return data;
	}
	
	//######################################################################
	private void addLine(List<String> line) {
		dataLines.add(line);
	}
	
	//######################################################################
	public void addParts(String... args) {
		List<String> line = new ArrayList<String>();
		for (String arg : args) {
			line.add(arg);
		}
		addLine(line);
	}
	
	//######################################################################
	public int findLine(String token) {
		for (List<String> line : dataLines) {
			if (line.get(0).equals(token)) {
				return dataLines.indexOf(line);
			}
		}
		return -1;
	}
	
	//######################################################################
	private List<String> getLine(int index) {
		return dataLines.get(index);
	}
	
	//######################################################################
	public String getPart(int part, String token) {
		int ind = findLine(token);
		if (ind >= 0) {
			return getLine(ind).get(part);
		} else {
			return "";
		}
	}
	
	//######################################################################
	public String getPart(int part, int index) {
		return dataLines.get(index).get(part);
	}

	//######################################################################
	public void addUnique (String... args) {
		int index = findLine(args[0]); 
		if (index != -1) {
			dataLines.get(index).clear();
			for (String arg : args) {
				dataLines.get(index).add(arg);
			}
		} else {
			addParts(args);
		}
	}
	
	//######################################################################
	public void resetIndex() {
		if (dataLines.size() > 0 ) {
			currentIndex = 0;
			currentLine = dataLines.get(0);
		} else {
			currentIndex = -1;
		}
	}
	
	//######################################################################
	public int getIndex () {
		return currentIndex;
	}
	
	//######################################################################
	public int getListSize () {
		if (dataLines!=null) {
			return dataLines.size();
		} else {
			return 0;
		}
	}
	
	//######################################################################
	public int getLineSize() {
		if (currentLine!=null){
			return currentLine.size();
		} else {
			return 0;
		}
	}
	
	//######################################################################
	public String getCurrent(int index) {
		if (currentIndex != -1) {
			return currentLine.get(index);
		} else {
			return null;
		}
	}
	
	//######################################################################
	public void setIndex(int index) {
		if (currentIndex != -1) {
			currentIndex = index;
			if (currentIndex > dataLines.size()-1) {
				currentIndex = dataLines.size()-1;
			} else if (currentIndex < 0) {
				currentIndex = 0;
			}
			currentLine = dataLines.get(currentIndex);
		}
	}
	
	//######################################################################
	public void incrIndex(int index) {
		setIndex(currentIndex+index);
	}
	
	//######################################################################
	public void incrIndex () {
		incrIndex(1);
	}
	
	//######################################################################
	public void removeLine(int index) {
		if (index != -1) {
			dataLines.remove(index);
			if (currentIndex == index) {
				setIndex(index-1);
			}
		}
	}
	
} // END
