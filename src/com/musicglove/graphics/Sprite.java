package com.musicglove.graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.opengl.GLUtils;
//import android.util.Log;

// Basic graphics object.  Two versions, one an image and the other graphical text.  
// bitmap handling triggers garbage collection, so, there is an option to make a fixed size
// text object that can be re-written so that it won't trigger garbage collection.
public class Sprite { 

	protected Graphics graphics;
	public int w;
	public int h;
	private Matrix matrix;
	public Bitmap bitmap;
	public String name = null;
	private String fontName;
	private float textSize;
	private int textColor;
	public Paint paint;
	private Typeface font;
	float baseline;
	private Canvas canvas;
	boolean fixedSize;
	
	//************** OpenGL **************
	
	protected float[] vertices;
	protected short[] indices = new short[] { 0, 1, 2, 1, 3, 2 };
	
	protected float[] textureCoordinates = { 	0.0f, 1.0f, 
			1.0f, 1.0f, 
			0.0f, 0.0f, 
			1.0f, 0.0f };
	
	protected FloatBuffer myVerticesBuffer = null;
	protected ShortBuffer myIndicesBuffer = null;
	protected FloatBuffer myTextureBuffer; 
	protected int[] TextureId;
	public boolean textureLoaded;
	public boolean useBitmapList = false;
	protected ArrayList<Bitmap> bitmapList;
	//protected boolean bitmapChanged = false;
	protected int bitmapIndex = 0;
	//protected int[] TextureIdList;
	//************************************

	/* two versions: bitmap, and text */
	//######################################################################	
	// bitmap versions 
	public Sprite ( Graphics g, Bitmap inImage) {
		graphics = g;
		bitmap = inImage;
		w = inImage.getWidth();
		h = inImage.getHeight();
		matrix = new Matrix();
		paint = new Paint();
		setupGL();
	}
	
	//######################################################################
	// bitmap versions 
	public Sprite ( Graphics g, String fileName ) {  
		this(g, fileName, 1.0f, 1.0f);
	}
	
	//######################################################################
	// bitmap versions 
	public Sprite ( Graphics g, String fileName, float scaleX, float scaleY ) {                                                                                      
		this(g, BitmapFactory.decodeResource(g.r, g.r.getIdentifier(fileName, "drawable", g.context.getPackageName()), g.opts), scaleX, scaleY);
		name = fileName;
	}
	
	//######################################################################
	// bitmap versions 
	public Sprite ( Graphics g, Bitmap inImage, float scaleX, float scaleY ) {    
		graphics = g;
		w = inImage.getWidth();
		h = inImage.getHeight();
		// uses the information in the universal Graphics class to optimally rescale all images to the true resolution of the display screen to conserve on memory space.
		bitmap = Bitmap.createScaledBitmap(inImage, (int) ((float) w * ((float) graphics.width / (float) graphics.XRES) * scaleX), (int) ((float) h * ((float) graphics.height / (float) graphics.YRES) * scaleY), true);
		inImage.recycle();
		w = bitmap.getWidth();
		h = bitmap.getHeight();
		matrix = new Matrix();
		paint = new Paint();
		setupGL();
	}

	//######################################################################
	// text versions
	public Sprite (Graphics g, String text, String fName, float tSize, int tColor) {
		this(g, text, fName, tSize, tColor, false);
	}
	
	//######################################################################
	// text versions
	public Sprite (Graphics g, String text, String fName, float tSize, int tColor, boolean fixedsize) {
		graphics = g;
		fixedSize = fixedsize;
		matrix = new Matrix();
		name = text;
		paint = new Paint();
		fontName = fName;
		textSize = tSize;
		textColor = tColor;
		font = Typeface.createFromAsset(graphics.context.getAssets(),fontName);
		paint.setTypeface(font);
		paint.setTextSize(textSize);
		paint.setColor(textColor);
		paint.setAntiAlias(true);
		paint.setTextAlign(Paint.Align.LEFT);
		w = (int) (paint.measureText(text) + 0.5f);
		baseline = (int) (-paint.ascent() + 0.5f);
		h = (int) (baseline + paint.descent() + 0.5f);
		bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		canvas.drawText(text, 0, baseline, paint);
		setupGL();
	}
	
	//######################################################################
	public void clear() {
		if (bitmap!=null) {bitmap.recycle();}
		bitmap=null;
	}
	
	//######################################################################
	public void setText (String newText) {
		name = newText;
		if (fixedSize) {
			bitmap.eraseColor(Color.TRANSPARENT);
		} else {
			w = (int) (paint.measureText(name) + 0.5f);
			h = (int) (baseline + paint.descent() + 0.5f);
			bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
			canvas.setBitmap(bitmap);
		}
		canvas.drawText(name, 0, baseline, paint);
	}

	//######################################################################
	public void setText (String newText, float textSize, int textColor) {
		if (textSize != -1) {
			paint.setTextSize(textSize);
		}
		if (textColor != -1) {
			paint.setColor(textColor);
		}
		setText(newText);
	}

	//######################################################################
	// right justified text
	public void setTextR (String newText) {
		name = newText;
		paint.setTextAlign(Paint.Align.RIGHT);
		if (fixedSize) {
			bitmap.eraseColor(Color.TRANSPARENT);
		} else {
			w = (int) (paint.measureText(name) + 0.5f);
			h = (int) (baseline + paint.descent() + 0.5f);
			bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
			canvas.setBitmap(bitmap);
		}
		canvas.drawText(name, w, baseline, paint);
	}
	
	//######################################################################
	public void setTextR(String newText, float textSize, int textColor) {
		paint.setTextSize(textSize);
		paint.setColor(textColor);
		setTextR(newText);		
	}
	
	//######################################################################
	public void render ( Canvas canvas, float x, float y, float rotation, float xScale, float yScale, Paint paint ) {     
		matrix.reset();
		matrix.postScale(xScale, yScale, w/2f, h/2f);
		matrix.postRotate(rotation, (w*xScale)/2f, (h*yScale)/2f);
		matrix.postTranslate(x, y);
		canvas.drawBitmap(bitmap, matrix, paint);
	}
	
	//######################################################################
	public void reScale(float xScale, float yScale) {
		bitmap = Bitmap.createScaledBitmap(bitmap,(int) (w*xScale),(int) (h*yScale), true);
		w = bitmap.getWidth();
		h = bitmap.getHeight();
		textureLoaded = false;
	}
	
	//######################################################################
	// utility to merge two sprites.  
	public Sprite addSprites(Sprite left, Sprite right) {
		int width = left.w + right.w;
		int height = (left.h > right .h) ? (left.h) : (right.h);  // use the highest of the two.
		Bitmap newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		Canvas combined = new Canvas(newBitmap);
		combined.drawBitmap(left.bitmap, 0f,  (height-left.h)/2f, null);
		combined.drawBitmap(right.bitmap, left.w, (height-right.h)/2f, null);
		return new Sprite(graphics, newBitmap);
	}
	
	//######################################################################
	// utility to merge two sprites.  
	public static Bitmap overlayBitmaps(Bitmap bottom, Bitmap top) {
		Canvas canvas = new Canvas(bottom);
		canvas.drawBitmap(top, bottom.getWidth()/2 - top.getWidth()/2, bottom.getHeight()/2 - top.getHeight()/2, new Paint());
		return bottom;
	}
	
	
	//######################################################################
	public void prepareList() {
		bitmapList = new ArrayList<Bitmap>();
	}
	
	public void addBitmap (Bitmap inbitmap) {
		bitmapList.add(inbitmap);
	}
	
	public void finalizeList() {
		if (bitmapList.isEmpty()) {
			TextureId = new int[1];
			useBitmapList = false;
			bitmapIndex = 0;
		} else {
			TextureId = new int[bitmapList.size()];
			useBitmapList = true;
			bitmapIndex = 0;
		}
	}
	

	public void setTexture(int index) {
		bitmapIndex = index;
	}
	
	protected void setupGL () {
		float dx = w/2f;
		float dy = h/2f;
		vertices = new float[] { -dx, -dy, 0.0f, dx, -dy, 0.0f,
				 				 -dx,  dy, 0.0f, dx,  dy, 0.0f };
		indices = new short[] { 0, 1, 2, 1, 3, 2 };
		setIndices(indices);
		setVertices(vertices);
		setTextureCoordinates(textureCoordinates);
		TextureId = new int[1];
	}
	
	protected void setVertices(float[] vertices) {
		ByteBuffer vbb = ByteBuffer.allocateDirect(48);
		vbb.order(ByteOrder.nativeOrder());
		myVerticesBuffer = vbb.asFloatBuffer();
		myVerticesBuffer.put(vertices);
		myVerticesBuffer.position(0);
	}

	protected void setIndices(short[] indices) {
		ByteBuffer ibb = ByteBuffer.allocateDirect(12);
		ibb.order(ByteOrder.nativeOrder());
		myIndicesBuffer = ibb.asShortBuffer();
		myIndicesBuffer.put(indices);
		myIndicesBuffer.position(0);
	}
	
	protected void setTextureCoordinates(float[] textureCoords) { 
		ByteBuffer byteBuf = ByteBuffer.allocateDirect(32);
		byteBuf.order(ByteOrder.nativeOrder());
		myTextureBuffer = byteBuf.asFloatBuffer();
		myTextureBuffer.put(textureCoords);
		myTextureBuffer.position(0);
	}
	
	protected void loadGLTexture(GL10 gl) { 
		if (useBitmapList == false) {
			int[] textures = new int[1];
			gl.glGenTextures(1, textures, 0);
			TextureId[0] = textures[0];
			gl.glBindTexture(GL10.GL_TEXTURE_2D, TextureId[0]);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		} else {
			int[] textures = new int[bitmapList.size()];
			gl.glGenTextures(bitmapList.size(), textures, 0);
			for (int i=0; i<bitmapList.size(); i++) {		
				TextureId[i] = textures[i];
				gl.glBindTexture(GL10.GL_TEXTURE_2D, TextureId[i]);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
				gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
				GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmapList.get(i), 0);
			}
		}
	}
	
	public void renderGL ( GL10 gl, float x, float y, float rotation, float xScale, float yScale ) {     

		gl.glPushMatrix();
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, myVerticesBuffer);
		if (textureLoaded == false) {
			loadGLTexture(gl);
			textureLoaded = true;
		}

		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, myTextureBuffer);
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, TextureId[bitmapIndex]);
		
		gl.glTranslatef(x+w/2, (graphics.height-y)-h/2, 0); 
		gl.glRotatef(rotation, 0, 0, 1);
		gl.glScalef(xScale, yScale, 0);
		
		gl.glDrawElements(GL10.GL_TRIANGLES, 8, GL10.GL_UNSIGNED_SHORT, myIndicesBuffer);
		
		gl.glPopMatrix();
	}
	
} // END


