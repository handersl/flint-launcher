package com.musicglove.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;

// button with a custom graphic background and text.
// permits a variable number of states.
public class GraphicTextButton extends Button {

	private Graphics graphics;
	private BitmapDrawable[] bdImages;
	public int state;
	public String text;
	private int size;
	private Typeface font;
	public int w;
	public int h;
	private int color;
	
	//######################################################################
	public GraphicTextButton (Context contxt) {
		super(contxt);
	}
	
	//######################################################################
	public GraphicTextButton (Context contxt, Graphics g, String txt, String fontName, int sze, int colr, float xS, float yS, String... args) {
		super(contxt);
		graphics = g;
		w = 0;
		h = 0;
		if (args != null) {
			Sprite temp = new Sprite(graphics, args[0], xS, yS);
			w = temp.w;
			h = temp.h;
			bdImages = new BitmapDrawable[args.length];
			for (int i=0; i<args.length; i++) {
				bdImages[i] = new BitmapDrawable(getResources(), (new Sprite(graphics, args[i], xS, yS)).bitmap);
			}
			this.setBackground(bdImages[0]);
		} 
		state = 0;
		font = Typeface.createFromAsset(graphics.context.getAssets(),fontName);
		this.setTypeface(font);
		this.setPadding(0,0,0,0);
		size = sze;
		this.setTextSize(size);
		text = txt;
		this.setText(text);
		color = colr;
		this.setTextColor(color);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(w, h);
		this.setLayoutParams(params);
	}
	
	//######################################################################
	public GraphicTextButton (Context context, Graphics g, String text, int size, int color, String... args) {
		this(context, g, text, "coolvetica_rg.ttf", size, color, 1.0f, 1.0f, args);
	}
		
	//######################################################################
	public GraphicTextButton(Context context, Graphics g, String text, String inImage) {
		this(context, g, text, "coolvetica_rg.ttf", 20, Color.BLACK, 1.0f, 1.0f, inImage);
	}

	//######################################################################
	public void clear() {
		if (bdImages!=null) {
			for (int i=0; i<bdImages.length; i++) {
				bdImages[i]=null;
			}
		}
		bdImages=null;
	}

	//######################################################################
	public void setState(int newState) {
		if (newState >= bdImages.length) {
			newState = newState%bdImages.length;
		} else if (newState < 0) {
			newState = -1*(newState%bdImages.length);
		}
		state = newState;
		this.setBackground(bdImages[state]);
	}
	
	//######################################################################
	@Override
	public boolean performClick() {
		return super.performClick();
	}

} // END
