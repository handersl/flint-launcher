package com.musicglove.helper;

import android.content.Intent;
import android.os.Bundle;

// Common class to pass variables between activities.  This is because android doesn't permit global classes.
// I am also using this class to store global constants.
public class PassedValues {
	
	public static final int CODE_EXIT = -3;
	public static final int CODE_LOADING = -2;
	public static final int CODE_PAUSED = -1;
	public static final int CODE_BUSY = 0;
	public static final int CODE_QUIT = 1;
	public static final int CODE_PLAY = 2;
	public static final int CODE_ANALYTICS = 3;
	public static final int CODE_OPTIONS = 4;
	public static final int CODE_MAINMENU = 5;
	public static final int CODE_RESULTS = 6;
	
	public int status = 5;

	public String version= null;
	public String subversion = null;
	public String userName= null;
	public int numGrips = 3;
	//Load always three grips for bluetooth but move them
	//according to this number.
	public int actualNumGrips = 1;
	
	public String fullPath= null;
	public String songPath= null;
	public String songName= null;
	public String gameMode= null;
	public String difficulty = null;

	public boolean isSessionMode = false;
	public boolean isAssessment = false;
	public int songsPlayedThisSession = 0;
	public int sessionLength = 0;
	public long sessionStartTime = 0;
	public String currentSong = null;
	public String previousSong = null;

	public boolean systemTimeValid = true;
	
	//######################################################################
	public PassedValues () {
		userName= "guest";
	}
	
	//######################################################################
	public Intent sendValues (Intent intent) {
		if (version != null) {
			intent.putExtra("version", version);
		}
		if (userName != null) {
			intent.putExtra("userName", userName);
		}
		if (fullPath != null) {
			intent.putExtra("fullPath", fullPath);
		}
		if (subversion != null) {
			intent.putExtra("subversion", subversion);
		}
		if (currentSong != null) {
			intent.putExtra("currentSong", currentSong);
		}
		if (previousSong != null) {
			intent.putExtra("previousSong", previousSong);
		}
		intent.putExtra("numGrips", numGrips);
		intent.putExtra("actualNumGrips", actualNumGrips);
		intent.putExtra("songsPlayedThisSession", songsPlayedThisSession);
		intent.putExtra("sessionLength", sessionLength);
		intent.putExtra("sessionStartTime", sessionStartTime);
		intent.putExtra("isSessionMode", isSessionMode);
		intent.putExtra("isAssessment", isAssessment);
		intent.putExtra("status", status);
		intent.putExtra("systemTimeValid", systemTimeValid);
		return intent;
	}
	
	//######################################################################
	public void receiveValues(Bundle passed) {
		if (passed != null) {
			status = passed.getInt("status");
			version = passed.getString("version");
			subversion = passed.getString("subversion");
			userName = passed.getString("userName");
			fullPath = passed.getString("fullPath");
			currentSong = passed.getString("currentSong");
			previousSong = passed.getString("previousSong");
			setSelection();
			numGrips = passed.getInt("numGrips");
			actualNumGrips = passed.getInt("actualNumGrips");
			isSessionMode = passed.getBoolean("isSessionMode");
			isAssessment = passed.getBoolean("isAssessment");
			if (isSessionMode) {
				songsPlayedThisSession = passed.getInt("songsPlayedThisSession");
				sessionLength = passed.getInt("sessionLength");
				sessionStartTime = passed.getLong("sessionStartTime");
			} else {
				songsPlayedThisSession = 0;
				sessionLength = 0;
				sessionStartTime = 0;
			}
			systemTimeValid = passed.getBoolean("systemTimeValid");
		}
	}
	
	//######################################################################
	public void setSelection() {
		if (fullPath != null) {
			String split[] = this.fullPath.split("_");
			gameMode = split[0];
			difficulty = split[1];
			songPath = split[0]+"_"+split[1]+"_";
			songName = split[2];
			currentSong = songName;
		}
	}
} // END
