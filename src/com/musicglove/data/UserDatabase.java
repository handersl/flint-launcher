package com.musicglove.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;

import com.musicglove.data.UserSav;


public  class UserDatabase {
	public static final String TAG = "MG_UserDatabase";

	private Context context;
	private User user;
	private List<String> songList;
	
	protected long ordinalStart;
	
	private UserSav file;

	//######################################################################
	public UserDatabase ( Context context) {
		this.context = context;
		songList = new ArrayList<String>();
		Calendar temp = Calendar.getInstance();
		temp.set(1,0,1,0,0,0);
		ordinalStart = temp.getTimeInMillis();
	}

	//######################################################################
	public void clear () {
		if(user!=null){user.clear();}
		if(songList!=null){songList.clear();}
		if(file!=null){file.clear();}
		user = null;
		songList = null;
		file = null;
	}

	//######################################################################
	public User setUser ( User userP ) {
		user = userP;
		// find users previous song
		if (user.userIsSelected){
			UserSav saves = new UserSav(context, user.currentUser);
			saves.loadData();
			if (!user.isSessionMode) {
				if (saves.dataLines.size() > 1) {
					int index = saves.dataLines.size()-1;
					user.currentSong = saves.dataLines.get(index).get(0);
					user.previousSong = saves.dataLines.get(index-1).get(0);
				} else if (saves.dataLines.size() == 1) {
					user.currentSong = saves.dataLines.get(0).get(0);
					user.previousSong = null;
				} 
			}
		}
		file = new UserSav(context, user.currentUser);
		file.loadData();
		return user;
	}

	//######################################################################
	public int milliToOrdinal (long ms) {
		return (int) ((ms-ordinalStart) / (1000*60*60*24)) - 1;
	}

	//######################################################################
	public List<int[]> getNotesHitList ( String songList, long startTime, long endTime) {
		List<String> list = new ArrayList<String>();
		list.add(songList);
		return getNotesHitList(list, startTime, endTime);
	}

	//######################################################################
	public List<int[]> getNotesHitList ( List<String> songList, long startTime, long endTime) {
		if (startTime == endTime) {startTime -= 24*60*60*1000+1;} // minimum search of 24 hours.
		List<int[]> notesList = new ArrayList<int[]>();
		String[] notesHitString;																									
		for (List<String> line : file.dataLines) {
			if ( (songList.indexOf(line.get(0)) >= 0) && (milliToOrdinal(startTime) < Integer.parseInt(line.get(1))) && (Integer.parseInt(line.get(1)) <= milliToOrdinal(endTime)) ) {
				notesHitString = (line.get(3)).split( "\\." );
				int[] notesHitList = {0, 0, 0, 0, 0};
				for (int i=0; i<5; i++) {
					notesHitList[i] = Integer.parseInt(notesHitString[i]); 
				}
				notesList.add(notesHitList);
			}
		}
		if (notesList.size() == 0) {
			int[] notesHitList = {0, 0, 0, 0, 0};
			notesList.add(notesHitList);
		}
		return notesList;
	}

	//######################################################################
	public List<int[]> getNotesAttemptedList ( String songList, long startTime, long endTime) {
		List<String> list = new ArrayList<String>();
		list.add(songList);
		return getNotesAttemptedList(list, startTime, endTime);
	}

	//######################################################################
	public List<int[]> getNotesAttemptedList ( List<String> songList, long startTime, long endTime) {
		if (startTime == endTime) {startTime -= 24*60*60*1000+1;}
		List<int[]> notesList = new ArrayList<int[]>();
		String[] notesHitString;																											
		for (List<String> line : file.dataLines) {
			if ( (songList.indexOf(line.get(0)) >= 0) && (milliToOrdinal(startTime) < Integer.parseInt(line.get(1))) && (Integer.parseInt(line.get(1)) <= milliToOrdinal(endTime)) ) {
				int[] notesHitList = {0, 0, 0, 0, 0};
				notesHitString = (line.get(4)).split( "\\." );
				for (int i=0; i<5; i++) {
					notesHitList[i] = Integer.parseInt(notesHitString[i]); 
				}
				notesList.add(notesHitList);
			}
		}
		if (notesList.size() == 0) {
			int[] notesHitList = {0, 0, 0, 0, 0};
			notesList.add(notesHitList);									
		}
		return notesList;
	}

	//######################################################################
	public int[] getNotesHit ( String songList, long startTime, long endTime) {
		List<String> list = new ArrayList<String>();
		list.add(songList);
		return getNotesHit(list, startTime, endTime);
	}

	//######################################################################
	public int[] getNotesHit ( List<String> songList, long startTime, long endTime) {
		if (startTime == endTime) {startTime -= 24*60*60*1000+1;}
		int[] notes = {0, 0, 0, 0, 0};
		String[] notesHitString;																				
		for (List<String> line : file.dataLines) {
			if ( (songList.indexOf(line.get(0)) >= 0) && (milliToOrdinal(startTime) < Integer.parseInt(line.get(1))) && (Integer.parseInt(line.get(1)) <= milliToOrdinal(endTime)) ) {
				notesHitString = (line.get(3)).split( "\\." );
				for (int i=0; i<5; i++) {
					notes[i] += Integer.parseInt(notesHitString[i]); 
				}
			}
		}
		return notes;
	}

	//######################################################################
	public int[] getNotesPossible ( String songList, long startTime, long endTime) {
		List<String> list = new ArrayList<String>();
		list.add(songList);
		return getNotesPossible(list, startTime, endTime);
	}

	//######################################################################
	public int[] getNotesPossible (List<String> songList, long startTime, long endTime) {
		if (startTime == endTime) {startTime -= 24*60*60*1000+1;}
		int[] notes = {0, 0, 0, 0, 0};
		String[] notesAttemptedString;																					
		for (List<String> line : file.dataLines) {
			if ( (songList.indexOf(line.get(0)) >= 0) && (milliToOrdinal(startTime) < Integer.parseInt(line.get(1))) && (Integer.parseInt(line.get(1)) <= milliToOrdinal(endTime)) ) {
				notesAttemptedString = (line.get(4)).split( "\\." );
				for (int i=0; i<5; i++) {
					notes[i] += Integer.parseInt(notesAttemptedString[i]);  
				}
			}
		}
		return notes;
	}

	//######################################################################
	public float[] getPercentNotesHit ( String songList, long startTime, long endTime ) {
		List<String> list = new ArrayList<String>();
		list.add(songList);
		return getPercentNotesHit(list, startTime, endTime);
	}

	//######################################################################
	public float[] getPercentNotesHit ( List<String> songList, long startTime, long endTime) {
		if (startTime == endTime) {startTime -= 24*60*60*1000+1;}
		int[] notesHit = {0, 0, 0, 0, 0};
		int[] notesPossible = {0, 0, 0, 0, 0};
		float[] percent = {0, 0, 0, 0, 0};
		String[] notesHitString;
		String[] notesAttemptedString;
		for (List<String> line : file.dataLines) {
			if ( (songList.indexOf(line.get(0)) >= 0) && (milliToOrdinal(startTime) < Integer.parseInt(line.get(1))) && (Integer.parseInt(line.get(1)) <= milliToOrdinal(endTime)) ) {
				notesHitString = (line.get(3)).split( "\\." );
				notesAttemptedString = (line.get(4)).split( "\\." );
				for (int i=0; i<5; i++) {
					notesHit[i] += Integer.parseInt(notesHitString[i]);
					notesPossible[i] += Integer.parseInt(notesAttemptedString[i]); 
				}
			}
		}
		for (int i = 0; i < notesHit.length; i++) {
			if ( notesPossible[i] != 0 ) {
				percent[i] = (float)(notesHit[i]) / (float)(notesPossible[i]);
			}
			else {
				percent[i] = 0;
			}
		}
		return percent;
	}

	//######################################################################
	public int getTimePlayed ( String songList, long startTime, long endTime) {
		List<String> list = new ArrayList<String>();
		list.add(songList);
		return getTimePlayed(list, startTime, endTime);
	}

	//######################################################################
	public int getTimePlayed (List<String> songList, long startTime, long endTime ) {
		if (startTime == endTime) {startTime -= 24*60*60*1000+1;}
		int time = 0;
		for (List<String> line : file.dataLines) {
			if ( (songList.indexOf(line.get(0)) >= 0) && (milliToOrdinal(startTime) < Integer.parseInt(line.get(1))) && (Integer.parseInt(line.get(1)) <= milliToOrdinal(endTime)) ) {
				time += Integer.parseInt(line.get(5));
			}
		}
		return time;
	}

	//######################################################################
	public List<List<int[]>> getLastNotes ( int num ) {
 		List<List<int[]>> returnList = new ArrayList<List<int[]>>(2);
		List<int[]> notesHitList = new ArrayList<int[]>();
		List<int[]> notesPossibleList = new ArrayList<int[]>();
		String[] notesHitString;
		String[] notesAttemptedString;
		for (int j=file.dataLines.size()-1; j >= (((file.dataLines.size()-num)>=0)?(file.dataLines.size()-num):(0)); j--) {
			List<String> line = file.dataLines.get(j);
			int[] notesHit = {0, 0, 0, 0, 0};
			int[] notesAttempted = {0, 0, 0, 0, 0};
			notesHitString = (line.get(3)).split( "\\." );
			notesAttemptedString = (line.get(4)).split( "\\." );
			for (int i=0; i<5; i++) {
				notesHit[i] = Integer.parseInt(notesHitString[i]);  
				notesAttempted[i] = Integer.parseInt(notesAttemptedString[i]);
			}
			notesHitList.add( notesHit );
			notesPossibleList.add( notesAttempted );
		}
		if (notesHitList.size() < 0) {
			int[] a = {0, 0, 0, 0, 0};
			notesHitList.add(a);
			notesPossibleList.add(a);
		}
		returnList.add(notesHitList);
		returnList.add(notesPossibleList);
		return returnList;
	}

	//######################################################################
	public Boolean hasBeenPlayed ( String song, long startTime, long endTime ) {
		if (startTime == endTime) {startTime -= 24*60*60*1000+1;}
		int songCount = 0;
		for (List<String> line : file.dataLines) {
			if ( (line.get(0).equals(song)) && (milliToOrdinal(startTime) < Integer.parseInt(line.get(1))) && (Integer.parseInt(line.get(1)) <= milliToOrdinal(endTime)) ) {
				songCount += 1;
			}
		}
		return songCount > 1;
	}

	//######################################################################
	public int getAllNotesHit (  ) {
		int notes = 0;
		String[] notesHitString; 
		for (List<String> line : file.dataLines) {
			notesHitString = (line.get(3)).split( "\\." );
			for (int i=0; i<5; i++) {
				notes += Integer.parseInt(notesHitString[i]);
			}
		}
		return notes;
	}

	//######################################################################
	public float getAllPercentHit (  ) {
		int notesHit = 0;
		int notesPossible = 0;
		String[] notesHitString;
		String[] notesPossibleString;
		for (List<String> line : file.dataLines) {
			notesHitString = (line.get(3)).split( "\\." );
			notesPossibleString = (line.get(4)).split( "\\." );
			for (int i=0; i<5; i++) {
				notesHit += Integer.parseInt(notesHitString[i]);
				notesPossible += Integer.parseInt(notesPossibleString[i]);
			}
		}
		if ( notesPossible != 0 ) {
			return (float)(notesHit)/(float)(notesPossible);
		} else {
			return 0;
		}

	}

	//######################################################################
	public int getAllTimePlayed (  ) {
		int time = 0;
		for (List<String> line : file.dataLines) {
			time += Integer.parseInt(line.get(5));
		}
		return time;
	}
	

} // END



