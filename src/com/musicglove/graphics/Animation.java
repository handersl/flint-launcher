package com.musicglove.graphics;

import java.util.ArrayList;

public class Animation extends Image {  
	
	private AnimatedSprite spriteRef;
	public int frame;
	private long tickCounter;
	private float msPerFrame;
	private int frames;
	private int	loop;
	public Boolean done;
	private ArrayList<AnimationCue> cueQueue;
	private AnimationCue firstCue;
	public final static int NEVER = 0;                                                           
	public final static int ON_TIME = 2;   
	
	//######################################################################
	public Animation ( AnimatedSprite spriteReference, float xPos, float yPos, int inloop, int inframes, int frameRate, float rotation, float scaleX, float scaleY ) {        
		super(spriteReference, xPos, yPos, false, 0, rotation, scaleX, scaleY);                                   
		spriteRef = spriteReference;
		frame = 0;                                                                                                            
		tickCounter = 0;                                                                                          
		msPerFrame = 1000f / (float) frameRate;                                                                 
		if ( inframes != 0 ) {                                                                                         
			frames = inframes;                                                                                    
		}
		else {                                                                                                         
			frames = spriteReference.spriteList.size();                                                             
		}
		loop = inloop;                                                                                             
		done = false;                                                                                           
		cueQueue = new ArrayList<AnimationCue>();
		firstCue = new AnimationCue(0, frames-1, 0, loop, AnimationCue.ON_LOOP, 1);                            
		cueQueue.add(firstCue);                                                                   
	}

	//######################################################################
	public Animation (AnimatedSprite spriteReference, float xPos, float yPos, float xScale, float yScale) {
		this(spriteReference, xPos, yPos, 0, 0, 60, 0.0f, xScale, yScale);
	}
	
	//######################################################################
	public Animation (AnimatedSprite spriteReference, float xPos, float yPos) {
		this(spriteReference, xPos, yPos, 0, 0, 60, 0.0f, 1.0f, 1.0f);
	}
	
	//######################################################################
	public Animation (AnimatedSprite spriteReference, float xPos, float yPos, int framerate) {
		this(spriteReference, xPos, yPos, 0, 0, framerate, 0.0f, 1.0f, 1.0f);
	}

	//######################################################################
	public void clear() {
		super.clear();
		if(spriteRef!=null){spriteRef.clear();}
		if(cueQueue!=null){cueQueue.clear();}
		spriteRef=null;
		cueQueue=null;
	}
	
	//######################################################################
	@Override
	public void tick ( long tickTime ) {                                                                   
		super.tick(tickTime);
		tickCounter += tickTime;       
		if ( cueQueue.size() > 0) {
			try {
				if (tickCounter >= msPerFrame) {                                                    
					tickCounter -= msPerFrame;                                                          
					frame += cueQueue.get(0).step;                                                              
					if ( (frame > cueQueue.get(0).endFrame) || 
							((cueQueue.get(0).step < 0) && (frame < cueQueue.get(0).endFrame)) ) {                                                
						if ( cueQueue.get(0).loop != 0 ) {                                                          
							frame = cueQueue.get(0).startFrame;                                            
						} else {                                                                                  
							frame = cueQueue.get(0).endFrame;                                                
						}
						if ( cueQueue.get(0).loop > 0 ) {                                                                
							cueQueue.get(0).loop-=1;                                                                   
						} else if ( cueQueue.get(0).loop == 0 ) {                                                           
							if ( cueQueue.get(0).proceed == AnimationCue.ON_LOOP ) {                                             
								nextCue();                                                                        
							}
						}
					} 
//					else if ( (cueQueue.get(0).step < 0) && (frame < cueQueue.get(0).startFrame) ) {                                                 
//						if ( cueQueue.get(0).loop != 0 ) {                                                                  
//							frame = cueQueue.get(0).endFrame;                                                              
//						} else {                                                                                         
//							frame = cueQueue.get(0).startFrame;                                                            
//						}
//						if ( cueQueue.get(0).loop > 0 ) {                                                                    
//							cueQueue.get(0).loop-=1;                                                                    
//						} else if ( cueQueue.get(0).loop == 0 ) {                                                             
//							if ( cueQueue.get(0).proceed == AnimationCue.ON_LOOP ) {                                         
//								nextCue();                                                                         
//							}
//						}
//					}
					spriteRef.setFrame(frame);	
				}
				if ( cueQueue.get(0).timer > 0 ) {                                                                           
					cueQueue.get(0).timer -= tickTime;                                                                
				} else if ( cueQueue.get(0).proceed == AnimationCue.ON_TIME ) {                                             
					nextCue();                                                                                    
				}
			} catch (Exception e) {
				// might need to do something to handle the event when the cueQueue is cleared while the above code was trying to run
			}
			
		} else {
			
		}
	}

	//######################################################################
	public void nextCue (  ) {                                                                                     
		if ( cueQueue.size() > 1 ) {                                                                            
			cueQueue.remove(0);                                                                             
			tickCounter = 0;                                                                                
			frame = cueQueue.get(0).startFrame;  
			spriteRef.setFrame(frame);
		} else {                                                                                                 
			done = true;                                                                       
		}
	}

	//######################################################################
	public void addCue ( int startFrame, int endFrame, int time, int loop, int proceed, int step ) {                 
		cueQueue.add( new AnimationCue(startFrame, endFrame, time, loop, proceed, step) );   
	}
	
	//######################################################################
	public void addCue (int startFrame, int endFrame, int time, int proceed) {
		cueQueue.add( new AnimationCue(startFrame, endFrame, time, 0 , proceed, 1) );
	}
	
	//######################################################################
	public void addCue (int startFrame, int endFrame) {
		cueQueue.add( new AnimationCue(startFrame, endFrame, 0, 0 , AnimationCue.ON_LOOP, 1) );
	}

	//######################################################################
	public void setCue (  int startFrame, int endFrame, int time, int loop, int proceed, int step ) {             
		cueQueue.clear();                                                  
		tickCounter = 0;                                                                      
		frame = startFrame;    
		spriteRef.setFrame(frame);
		cueQueue.add( new AnimationCue(startFrame, endFrame, time, loop, proceed, step) );  
	}
	
	//######################################################################
	public void setCue (int startFrame, int endFrame, int proceed, int step) {
		setCue(startFrame, endFrame, 0, 0, proceed, step);
	}
	
	
	//######################################################################
	public void setCue (int startFrame, int endFrame, int proceed) {
		setCue(startFrame, endFrame, 0, 0, proceed, 1);
	}
	
	//######################################################################
	public void setCue (int startFrame, int endFrame) {
		setCue(startFrame, endFrame, 0, 0, AnimationCue.ON_LOOP, 1);
	}
	
	//######################################################################
	@Override
	public Boolean isDone () {
		return done;
	}

	
} // END


