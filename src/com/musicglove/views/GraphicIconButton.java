package com.musicglove.views;

import android.content.Context;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;

// merges two graphics side by side into a button.
// old code base - requires explicitly setting the "clicked", "active" and "inactive" images.
public class GraphicIconButton extends ImageButton {

	private Graphics graphics;
	private Sprite iconImage;
	private Sprite textImage;
	private Sprite image;
	public Sprite inactiveImage;
	public Sprite activeImage;
	public Sprite clickedImage;
	
	//######################################################################
	public GraphicIconButton (Context contxt) {
		super(contxt);
	}
	
	//######################################################################
	public GraphicIconButton(Context context, Graphics g, Sprite inIconImage, Sprite inTextImage, float relX, float relY, Boolean scaleFlag) {
		super(context);
		graphics = g;
		iconImage = inIconImage;
		changeText(inTextImage, relX, relY, scaleFlag);
		inactiveImage = image;
		activeImage = image;
		clickedImage = image;
	}
	
	//######################################################################
	public void clear() {
		if(iconImage!=null){iconImage.clear();}
		if(textImage!=null){textImage.clear();}
		if(image!=null){image.clear();}
		if(inactiveImage!=null){inactiveImage.clear();}
		if(activeImage!=null){activeImage.clear();}
		if(clickedImage!=null){clickedImage.clear();}
		iconImage=null;
		textImage=null;
		image=null;
		inactiveImage=null;
		activeImage=null;
		clickedImage=null;
	}
	
	//######################################################################
	public void changeText (Sprite inTextImage, float relX, float relY, Boolean scaleFlag) {
		textImage = inTextImage;
		if (scaleFlag == true) {
			float scale = (float) iconImage.h/textImage.h;
			textImage.reScale(scale, scale);
		}
		image = textImage.addSprites(iconImage,textImage);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(image.w, image.h);
		this.setPadding(0, 0, 0, 0);
		this.setBackground(null);
		this.setImageBitmap(image.bitmap);
		lp.leftMargin = graphics.absX(relX) - iconImage.w/2;
		lp.topMargin = graphics.absY(relY) - iconImage.h/2;	
		this.setLayoutParams(lp);
	}

	//######################################################################
	public void setInactive() {
		image = inactiveImage;
		this.setImageBitmap(image.bitmap);
	}
	
	//######################################################################
	public void setActive() {
		image = activeImage;
		this.setImageBitmap(image.bitmap);
	}

	//######################################################################
	public void setClicked() {
		image = clickedImage;
		this.setImageBitmap(image.bitmap);
	}
	
} // END
