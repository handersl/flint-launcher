package com.musicglove.graphics;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.opengl.GLUtils;


public class AnimatedSprite extends Sprite {      
	protected List<Bitmap> spriteList;
	protected int frameRate;
	protected int currentFrame;
	
	protected int[] TextureId;
	
	//######################################################################
	public AnimatedSprite ( Graphics g, String name, int frameRate ) {      
		super(g, "animated_"+name+"_"+name+"0");
		this.frameRate = frameRate;
		spriteList = new ArrayList<Bitmap>();                  
		int index = 0;
		int identifier = graphics.r.getIdentifier("animated_"+name+"_"+name+Integer.toString(index), "drawable", graphics.context.getPackageName());
		while (identifier != 0) {
			spriteList.add((new Sprite(g, "animated_"+name+"_"+name+Integer.toString(index))).bitmap);
			index++;
			identifier = graphics.r.getIdentifier("animated_"+name+"_"+name+Integer.toString(index), "drawable", graphics.context.getPackageName());
		};
		TextureId = new int[spriteList.size()];
		currentFrame = 0;
	}
	
	//######################################################################
	public AnimatedSprite (Graphics g, String name) {
		this(g, name, 60);
	}
	
	//######################################################################
	public void clear() {
		super.clear();
		if(spriteList!=null) {
			for (int i=0;i<spriteList.size();i++) {
				if (spriteList.get(i)!=null) {spriteList.get(i).recycle();}
			}
			spriteList.clear();
		}
		spriteList = null;
	}
	
	//######################################################################
	public void setFrame ( int index ) {                   
		if (index >= spriteList.size()) {
			index = spriteList.size()-1;
		}
		if (index < 0) {
			index = 0;
		}
		currentFrame = index;
		bitmap = spriteList.get(index);   
	}
	
	@Override
	protected void loadGLTexture(GL10 gl) { 
		int[] textures = new int[spriteList.size()];
		gl.glGenTextures(spriteList.size(), textures, 0);
		for (int i=0; i<spriteList.size(); i++) {		
			TextureId[i] = textures[i];
			gl.glBindTexture(GL10.GL_TEXTURE_2D, TextureId[i]);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, spriteList.get(i), 0);
		}
	}
	
	@Override
	public void renderGL ( GL10 gl, float x, float y, float rotation, float xScale, float yScale ) {     
		gl.glPushMatrix();
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, myVerticesBuffer);
		if (textureLoaded == false) {
			loadGLTexture(gl);
			textureLoaded = true;
		}

		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, myTextureBuffer);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, TextureId[currentFrame]);
		gl.glTranslatef(x+w/2, (graphics.height-y)-h/2, 0); 
		gl.glRotatef(rotation, 0, 0, 1);
		gl.glScalef(xScale, yScale, 0);
		
		gl.glDrawElements(GL10.GL_TRIANGLES, 8, GL10.GL_UNSIGNED_SHORT, myIndicesBuffer);
		
		gl.glPopMatrix();
	}
	

	
} // END


