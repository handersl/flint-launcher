package com.musicglove.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.Display;

// This is our container class.  It knows all about the real screen, the activity, canvas, paint, all resourcers, etc
// Through this class, the other classes can access vital things
public class Graphics {
		
	// hardcoded target resolution
	// the view window will be scaled to fit this ratio, but scaled to maximize screen use
	public int XRES = 1920;
	public int YRES = 1080;
	public float scaleX = 1;
	public float scaleY = 1;
	public float xCenter = 0.5f;
	public float yCenter = 0.5f;
	public float xStretch = 1;
	public float yStretch = 1;
	
	public Context context;
	public Paint paint;
	public BitmapFactory.Options opts;
	public Resources r;
	private Display display;
	private Point size;
	// the real screen size, no one knows about ...
	private int fullWidth;
	private int fullHeight;
	private float ratio;
	// the drawing window size, all that matters ...
	public int width;
	public int height;
	public int spacer;
	public String orientation;

	//######################################################################
	public Graphics(Context contxt, Display indisplay, Paint inpaint, BitmapFactory.Options inopts, Resources inr, float inScale, boolean forceHD) {
		this(contxt, indisplay, inpaint, inopts, inr, inScale, forceHD, 0.5f, 0.5f);
	}
		
	//######################################################################
	public Graphics(Context contxt, Display indisplay, Paint inpaint, BitmapFactory.Options inopts, Resources inr, float inScale) {
		this(contxt, indisplay, inpaint, inopts, inr, inScale, true);
	}
	
	//######################################################################
	public Graphics(Context contxt, Display indisplay, Paint inpaint, BitmapFactory.Options inopts, Resources inr, float inScale, boolean forceHD, float xC, float yC) {
		scaleX = inScale;
		scaleY = inScale;
		xCenter = xC;
		yCenter = yC;
		context = contxt;
		display = indisplay;
		paint = inpaint;
		opts = inopts;
		r=inr;
		size = new Point();
		display.getSize(size);
		fullWidth = size.x;
		fullHeight = size.y;
		ratio = (float) fullWidth / (float) fullHeight; 
		if (forceHD) { // make "forceHD" actually refer to "bottom bar exists"
//			if ( ratio < (float) (XRES/scaleX) / (float) (YRES/scaleY)) {
//				orientation = "portrait";
//				spacer = (int) (((float)fullHeight-(float)fullWidth*(float)(YRES/scaleY)/(float)(XRES/scaleX))/2f);
//				height = (int) ((float)fullWidth*(float)(YRES/scaleY)/(float)(XRES/scaleX));
//				width = fullWidth;
//			} else if ( ratio > (float) (XRES/scaleX) / (float) (YRES/scaleY)) {
//				orientation = "landscape";
//				spacer = (int) (((float)fullWidth-(float)fullHeight*(float)(XRES/scaleX)/(float)(YRES/scaleY))/2f);
//				width = (int) ((float)fullHeight*(float)(XRES/scaleX)/(float)(YRES/scaleY));
//				height = fullHeight;
//			} else {
//				orientation = "HD";
//				width = fullWidth;
//				height = fullHeight;
//			}
			// just force this for now, I know what my hardware is
			orientation = "HD";
			width = 1920;
			height = 1079; // this will fill the whole screen, but I'm going to move things around differently since the bottom bar will be shown
			XRES = (int) ((float)XRES/scaleX);
			YRES = (int) ((float)YRES/scaleY);
		} else {
			orientation = "native";
			//width = fullWidth;
			//height = fullHeight;
			width = 1024;
			height = 600;
			//if (ratio < (float) XRES / (float) YRES) {
			//	yStretch = (float)fullHeight/((float)fullWidth*(float)YRES/(float)XRES);
			//	scaleX = scaleX * yStretch;
			//} else if ( ratio > (float) XRES / (float) YRES) {
			//	xStretch = (float)fullWidth/((float)fullHeight*(float)XRES/(float)YRES);
			//	scaleY = scaleY * xStretch;
			//} 
			XRES = (int) ((float)XRES/scaleX);
			YRES = (int) ((float)YRES/scaleY);
		}
	}

	//######################################################################
	public int absX(float relX) {
		return (int) (((relX-xCenter)*scaleX+xCenter) * width);
		//return (int) (((relX-0.5f)*scaleX+xCenter) * width);
	}
	
	//######################################################################
	public int absY(float relY) {
		return (int) ((1-((relY-yCenter)*scaleY+yCenter)) * height);
		//return (int) ((1f-((relY-0.5f)*scaleY+yCenter)) * (float)height);
	}
	
	//######################################################################
	public int convX(int fullX) {
		return (int) ((float)fullX/(float)XRES*(float)width);
	}
	
	//######################################################################
	public int convY(int fullY) {
		return (int) ((float)fullY/(float)YRES*(float)height);
	}
	
	//######################################################################
	public int convX(float fullX) {
		return (int) ((float)fullX/(float)XRES*(float)width);
	}
	
	//######################################################################
	public int convY(float fullY) {
		return (int) ((float)fullY/(float)YRES*(float)height);
	}

} // END