package com.musicglove.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.musicglove.data.DataFile;

public class UserSav extends DataFile {
	private static final String TAG = "MG_UserSav";
	
	//######################################################################
	public UserSav (Context context, String userName) {
		super(context, userName+".sav", null, ":");
	}
	
	//######################################################################
	@Override
	public void loadData() {
		File sdCard = Environment.getExternalStorageDirectory();
		File directory = new File (sdCard.getAbsolutePath()+"/MusicGlove/saves");
		try {
			File file = new File(directory, fileName);
			FileInputStream fIn = new FileInputStream(file);
			if (fIn != null ) {
				InputStreamReader inputStreamReader = new InputStreamReader(fIn);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            while ( (receiveString = bufferedReader.readLine()) != null ) {
        			String[] parts = receiveString.split(separator);
        			ArrayList<String> arrayPart = new ArrayList<String>();
        			for (String part : parts) {
        				arrayPart.add(part);
        			}
        			dataLines.add(arrayPart);
	            }
	            inputStreamReader.close();
			} 
		} catch (FileNotFoundException e) {
	        Log.e(TAG, "File not found: " + e.toString());
	        loadDefault();
	    } catch (IOException e) {
	        Log.e(TAG, "Can not read file: " + e.toString());
	        loadDefault();
	    }
		
	}
	
	//######################################################################
	@Override
	public void saveData() {
		File sdCard = Environment.getExternalStorageDirectory();
		File directory = new File (sdCard.getAbsolutePath()+"/MusicGlove/saves");
		directory.mkdirs();
		try {
			File file = new File(directory, fileName);
			FileOutputStream fOut = new FileOutputStream(file);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);
			outputStreamWriter.write(processOutData());
			outputStreamWriter.flush();
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e(TAG, "File write failed: " + e.toString());
		}
	}
	
} // END
