package com.musicglove.views;

import android.content.Context;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;

// extensions to make a custom graphic button with multiple states.
public class GraphicButton extends ImageButton {
	
	public Sprite image;
	public Sprite[] images;
	public int state;
	private Graphics graphics;
	public float relX;
	public float relY;

	//######################################################################
	public GraphicButton(Context context) {
		super(context);
	}
	
	//######################################################################
	public GraphicButton(Context context, Graphics g, float relX, float relY, String... args) {
		super(context);
		graphics = g;
		images = new Sprite[args.length];
		for (int i=0; i<args.length; i++) {
			images[i] = new Sprite (graphics, args[i]);
		}
		state = 0;
		image = images[state];
		this.setPadding(0, 0, 0, 0);
		this.setBackground(null);
		this.setImageBitmap(image.bitmap);
		setXY(relX, relY);
	}
	
	//######################################################################
	public GraphicButton (Context context, Graphics g, String... args) {
		this(context, g, 0, 0, args);
	}
	
	//######################################################################
	public void clear() {
		if(image!=null){image.clear();}
		if(images!=null){
			for (int i=0; i<images.length; i++) {
				if(images[i]!=null){images[i].clear();}
				images[i]=null;
			}
		}
		image = null;
		images =null;
	}
	
	//######################################################################
	public void setXY(float x, float y) {
		relX = x;
		relY = y;
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(images[0].w, images[0].h);
		lp.leftMargin = graphics.absX(x) - (int) (images[0].w/2);
		lp.topMargin = graphics.absY(y) - (int) (images[0].h/2);
		this.setLayoutParams(lp);
	}
	
	//######################################################################
	public void setState (int newState) {
		if (newState >= images.length) {
			state = 0; 
		} else if (newState < 0) {
			state = 0;
		} else {
			state = newState;
		}
		image = images[state];
		this.setImageBitmap(image.bitmap);
	}

} // END
