package com.musicglove.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;

public class GamePauseDialog extends Dialog {
	
	protected Context context;
	private Graphics graphics;
	private RelativeLayout dialogLayout;
	private LinearLayout buttonLayout;
	private ImageView overlay;
	private GraphicButton resumeSongButton; // response code 0
	private GraphicButton restartSongButton; // response code 1
	private GraphicButton endSongButton; // response code 2
	
	public boolean newResponse = false;
	public int response = -1;
	
	private int w;
	private int h;

	
	//######################################################################
	public GamePauseDialog(Context contxt, Graphics g) {
		super(contxt);
		context = contxt;
		graphics = g;
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		Bitmap b = new Sprite(graphics, "pause_bg").bitmap;
		w=b.getWidth();
		h=b.getHeight();
		
		this.getWindow().setBackgroundDrawable(new BitmapDrawable(context.getResources(), b));
		
		dialogLayout = new RelativeLayout(context);
		RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);//b.getWidth(), b.getHeight());
		dialogLayout.setLayoutParams(rlp);
		
		buttonLayout = new LinearLayout(context);
		buttonLayout.setOrientation(LinearLayout.VERTICAL);
		buttonLayout.setPadding(graphics.convX(45), graphics.convY(83), 0,0);
		
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		resumeSongButton = new GraphicButton(context, graphics, "animated_pauseresume_pauseresume0");
		resumeSongButton.setOnClickListener( new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				response = 0;
				newResponse = true;
				dismiss();
			}
		});
		llp.setMargins(0, 2, 0, 2);
		resumeSongButton.setLayoutParams(llp);
		buttonLayout.addView(resumeSongButton);
		
		restartSongButton = new GraphicButton(context, graphics, "animated_pauserestart_pauserestart0");
		restartSongButton.setOnClickListener( new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				response = 1;
				newResponse = true;
				dismiss();
			}
		});
		llp.setMargins(0, 2, 0, 2);
		restartSongButton.setLayoutParams(llp);
		buttonLayout.addView(restartSongButton);
		
		endSongButton = new GraphicButton(context, graphics, "animated_pauseend_pauseend0");
		endSongButton.setOnClickListener( new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				response = 2;
				newResponse = true;
				dismiss();
			}
		});
		llp.setMargins(0, 2, 0, 2);
		endSongButton.setLayoutParams(llp);
		buttonLayout.addView(endSongButton);
		
		dialogLayout.addView(buttonLayout);
		
		overlay = new ImageView(context);
		overlay.setImageBitmap(new Sprite(graphics, "pause_fg").bitmap);
		rlp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		rlp.setMargins(graphics.convX(4), graphics.convX(84), 0, 0);
		dialogLayout.addView(overlay, rlp);
				
		this.setContentView(dialogLayout);
		
		this.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				response = 0;
				newResponse = true;
			}
		});
	}

	//######################################################################
	@Override
	public void show() {
		super.show();
		this.getWindow().setLayout(w, h);
	}
	
	//######################################################################
	public void clear () {
	}
	
	//######################################################################
	public boolean getNewResponse () {
		if (newResponse == true) {
			newResponse = false;
			return true;
		}
		return false;
	}
	
} // END