package com.musicglove.views;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;

import com.musicglove.graphics.AnimatedSprite;
import com.musicglove.graphics.Animation;
import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Image;
import com.musicglove.graphics.Sprite;

// The customized graphics view compatible with the image and motion classes.
public class GraphicsView extends View {
	
	protected Graphics graphics;
	protected int width;
	protected int height;
	protected Bitmap background;
	public ArrayList<Image> imageList;
	
	public volatile Boolean updated = false;
	public volatile Boolean changing = false;
	
	protected int i;

	//######################################################################
	public GraphicsView (Context contxt) {
		super(contxt);
	}
	
	//######################################################################
	public GraphicsView (Context context, Graphics g, int w, int h) {
		super(context);
		graphics = g;
		imageList  = new ArrayList<Image>();
		width = w;
		height = h;
	}
	
	//######################################################################
	public void clear () {
		if (background!=null){background.recycle();}
		if (imageList!=null){
			for (int i=0; i<imageList.size(); i++) {
				if(imageList.get(i)!=null){imageList.get(i).clear();}
			}
			imageList.clear();
		}
		background=null;
		imageList=null;
	}
	
	//######################################################################
	@Override
	protected void onDraw (Canvas canvas) {
		if (changing == false) {
			updated = false;
			render(canvas);
			super.onDraw(canvas);
			updated = true;
		}
	}

	//######################################################################
	public void update (long ticktime) {
		tick(ticktime);
		changing = false;
		updated = false;
	}

	//######################################################################
	public int addImage (Bitmap inimage, float x, float y) {
		imageList.add(new Image(new Sprite(graphics, inimage), x, y));
		return imageList.size()-1;
	}
	
	//######################################################################
	public int addImage (String fileName, float x, float y) {
		imageList.add(new Image(new Sprite(graphics, fileName), x, y));
		return imageList.size()-1;
	}
	
	//######################################################################
	public int addImage (String text, String font, int size, int color, float x, float y) {
		imageList.add(new Image(new Sprite(graphics, text, font, size, color), x, y));
		return imageList.size()-1;
	}
	
	//######################################################################
	public int addImage (String text, String font, int size, int color, float x, float y, boolean fixedSize) {
		imageList.add(new Image(new Sprite(graphics, text, font, size, color, fixedSize), x, y));
		return imageList.size()-1;
	}

	//######################################################################
	public int addAnimation(String fileName, float x, float y) {
		imageList.add(new Animation(new AnimatedSprite(graphics, fileName), x, y));
		return imageList.size()-1;
	}

	//######################################################################
	public int addAnimation(String fileName, float x, float y, int fr) {
		imageList.add(new Animation(new AnimatedSprite(graphics, fileName), x, y, fr));
		return imageList.size()-1;
	}
	
	//######################################################################
	public int addAnimation(String fileName, float x, float y, float xScale, float yScale) {
		imageList.add(new Animation(new AnimatedSprite(graphics, fileName), x, y, xScale, yScale));
		return imageList.size()-1;
	}

	//######################################################################
	public void tick ( long tickTime ) { 
		for (int index = imageList.size()-1; index >= 0; index--) {
			if (imageList.get(index).isDone()) {
				imageList.get(index).hidden=true;
			} else {
				if (imageList.get(index).spriteRef.bitmap != null) {
					imageList.get(index).tick(tickTime);
				}
			}
		}
	}

	//######################################################################
	public void render (Canvas canvas) {  
		for (i=0;i<imageList.size();i++) {
			if ( imageList.get(i) != null && imageList.get(i).hidden == false ) {
				imageList.get(i).spriteRef.render( canvas, ((imageList.get(i).position[0]-graphics.xCenter)*graphics.scaleX+graphics.xCenter)*(float)width-(float)imageList.get(i).spriteRef.w/2f,  (1-((imageList.get(i).position[1]-graphics.yCenter)*graphics.scaleY+graphics.yCenter)) * (float)height-(float)imageList.get(i).spriteRef.h/2f, imageList.get(i).position[2], imageList.get(i).scaleX, imageList.get(i).scaleY, graphics.paint );
			}
		}
	}
	
} // END
