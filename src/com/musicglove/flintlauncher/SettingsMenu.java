package com.musicglove.flintlauncher;

import java.io.File;
import java.util.List;

import com.flint.flintlauncher.R;
import com.musicglove.helper.PassedValues;
import com.musicglove.scenes.ActivityMainMenuScene;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SettingsMenu extends Activity 
{
	private SDCard sdcard = new SDCard();
	private Handler sdcardRefreshHandler;
	
	public void onCreate(Bundle savedInstanceState) 
	 {
		//Set up the window.
		super.onCreate(savedInstanceState);
		super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		sdcardRefreshHandler = new Handler();
		sdcardRefreshHandler.post(sdcardRefreshRunnable);
		loadAct();
	 }
	
	private void loadAct()
	{	
		final PassedValues values = new PassedValues();
		values.receiveValues(getIntent().getExtras());
		
		//Grab the display size.
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int screenWidth = size.x;
		int screenHeight = size.y;
		
		//The main layout to hang the lists on.
		final RelativeLayout layout = new RelativeLayout(this);
		setContentView(layout);
		
		Button setTimeButton = new Button(this);
		setTimeButton.setBackgroundResource(R.drawable.settimebutton);
		setTimeButton.setX(screenWidth*.75f);
		setTimeButton.setY(screenHeight*.25f);
		setTimeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
				finish();
			}
		});
		layout.addView(setTimeButton);
		
		Button setWifiButton = new Button(this);
		setWifiButton.setBackgroundResource(R.drawable.setwifibutton);
		setWifiButton.setX(screenWidth*.75f);
		setWifiButton.setY(screenHeight*.50f);
		setWifiButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
				finish();
			}
		});
		layout.addView(setWifiButton);
		
		Button exportDataButton = new Button(this);
		exportDataButton.setBackgroundResource(R.drawable.exportdatabutton);
		exportDataButton.setX(screenWidth*.75f);
		exportDataButton.setY(screenHeight*.75f);
		exportDataButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//TODO
			}
		});
		layout.addView(exportDataButton);
		
		Button backButton = new Button(this);
		backButton.setBackgroundResource(R.drawable.backbutton);
		backButton.setX(1720);
		backButton.setY(1006);
		backButton.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				Intent intent = new Intent(SettingsMenu.this, ActivityMainMenuScene.class);
				intent = values.sendValues(intent);
				startActivity(intent);
				finish();
			}
		});
		layout.addView(backButton);
		
		final PackageManager pm = getPackageManager();
		//get a list of installed apps.
		List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
		
		//Create a scroll plane to list all the apps available on the sdcard.
		ScrollView scrollSDCard = new ScrollView(this);
		final LinearLayout layoutSDCard = new LinearLayout(this);
		layoutSDCard.setLayoutParams(new FrameLayout.LayoutParams((int) (screenWidth*.5), screenHeight));
		layoutSDCard.setVerticalScrollBarEnabled(true);
		layoutSDCard.setOrientation(LinearLayout.VERTICAL);
		layoutSDCard.setX(0);
		scrollSDCard.addView(layoutSDCard);
		layout.addView(scrollSDCard);
		
		TextView textSDCard = new TextView(this);
		textSDCard.setTextSize(30);
		textSDCard.setText("Apps to Install.");
		textSDCard.setHeight(64);
		layoutSDCard.addView(textSDCard);
		
		File[] listOfSDCardFiles = sdcard.getListOfFiles();
		
		for (final File file : listOfSDCardFiles) 
		{
			if(file.getName().contains(".apk"))
			{
				//Check to make sure it is not already installed.
				PackageManager pakman = getPackageManager();
				PackageInfo pakInfo = pakman.getPackageArchiveInfo(sdcard.getDir()+"/"+file.getName(), PackageManager.GET_ACTIVITIES);
				
				boolean installed = false;
				for (final ApplicationInfo packageInfo : packages) 
				{
					if(packageInfo.packageName.equals(pakInfo.packageName))
					{
						installed = true;
					}
				}
				
				if(pakInfo != null && pakInfo.packageName.contains("musicglove") && !installed)
				{
					Button appButton = new Button(this);
					appButton.setText(file.getName());
					appButton.setOnClickListener(new View.OnClickListener() 
					{	
						@Override
						public void onClick(View v) 
						{
							Intent intent = new Intent(Intent.ACTION_VIEW);
						    intent.setDataAndType(
						            Uri.parse("file:///"+sdcard.getDir()+"/"+file.getName()),
						            "application/vnd.android.package-archive");
						    startActivity(intent);
						}
					});
					layoutSDCard.addView(appButton);
				}
			}
		}
	}
	
	
	final Runnable sdcardRefreshRunnable = new Runnable()
	{
	    public void run() 
	    {
	    	if(sdcard.sdCardPresent && !sdcard.isSDCardLoaded())
	    	{
	    		loadAct();
	    		sdcardRefreshHandler.postDelayed(sdcardRefreshRunnable, 300);
	    	}
	    }
	};
	
	private void cleanup()
	{
		if(sdcard != null)
		{
			sdcard = null;
		}
		if(sdcardRefreshHandler != null)
		{
			sdcardRefreshHandler.removeCallbacksAndMessages(null);
			sdcardRefreshHandler = null;
		}
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    loadAct();
	}
	
	@Override
	public void onRestart() {
	    super.onRestart();
	    loadAct();
	}
	
	@Override
	public void onDestroy() {
		cleanup();
	    super.onDestroy();
	}
}