package com.musicglove.views;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class MyGLSurfaceView extends GLSurfaceView {

	boolean touched = false;
	
	public MyGLSurfaceView(Context context) {
		super(context);
		setEGLConfigChooser(8, 8, 8, 8, 0, 0);
		getHolder().setFormat(PixelFormat.RGBA_8888);
		setPreserveEGLContextOnPause(true);
	}
	
	public boolean hasBeenTouched() {
		if (touched == true) {
			touched = false;
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		super.onTouchEvent(e);
		performClick();
		touched = true;
		return true;
	}
	
	@Override
	public boolean performClick() {
		super.performClick();
		return true;
	}
	
	
}
