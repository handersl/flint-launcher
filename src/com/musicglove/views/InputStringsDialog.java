package com.musicglove.views;

import java.util.ArrayList;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;

// Generic input string dialog.
// set the title on top, then add variable number of text and edit boxes.
// two customized buttons added in a row on the bottom.
public class InputStringsDialog extends CustomDialog {

	private ArrayList<String> response;
	public ArrayList<TextView> label;
	private ArrayList<EditText> input;
	private LinearLayout dialogLayout;
	// there should be a new response flag and a response value, this was early code.
	public int newResponse;
	private Context context;
	public boolean showKeyboard = true;
	
	//######################################################################
	public InputStringsDialog(Context context, Graphics g, Sprite image, int leftpad, int toppad, int rightpad, int bottompad) {
		super(context, g, image, leftpad, toppad, rightpad, bottompad);
		this.context = context;
		newResponse = -1;
		response = new ArrayList<String>();
		label = new ArrayList<TextView>();
		input = new ArrayList<EditText>();
		dialogLayout = new LinearLayout(context);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		dialogLayout.setOrientation(LinearLayout.VERTICAL);
		dialogLayout.setLayoutParams(params);
		dialogLayout.setGravity(Gravity.CLIP_VERTICAL);
		dialogLayout.setPadding(30, 15, 30, 15);
		this.setContent(dialogLayout);
	
		this.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				newResponse = 0;
			}
		});
		
		
	}

	//######################################################################
	public void clear() {
		if(response!=null){response.clear();}
		if(label!=null){label.clear();}
		if(input!=null){input.clear();}
		if(dialogLayout!=null){dialogLayout.removeAllViews();}
		response=null;
		label=null;
		input=null;
		dialogLayout=null;
	}
	
	//######################################################################
	@Override
	public void show() {
		// Set the dialog to not focusable. This prevents the nav bar from appearing
//	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
//	            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
//
//	    try {
//	    	this.getWindow().getDecorView().setSystemUiVisibility(
//	    		getOwnerActivity().getWindow().getDecorView().getSystemUiVisibility()); // might not work, some prereqs need to be met for getOwnerActivity to not return null!
//	    } catch(Exception e) {
//	    	// probably could not getOwnerActivity(); I THINK I AM LANDING HERE, DID NOT WORK!
//	    }
		super.show();
		if (showKeyboard) {
			this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
	                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM); 
			this.getWindow().setSoftInputMode(
	                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		}
	}
	
	//######################################################################
	public void addText(String text, String fontName, int fontSize, int fontColor) {
		TextView newlabel = new TextView(context);
		Typeface font = Typeface.createFromAsset(graphics.context.getAssets(),fontName);
		newlabel.setTypeface(font);
		newlabel.setTextSize(fontSize);
		newlabel.setTextColor(fontColor);
		newlabel.setText(text);
		newlabel.setPadding(0,10,0,10);
		newlabel.setGravity(Gravity.CENTER_HORIZONTAL);
		label.add(newlabel);
		dialogLayout.addView(label.get(label.size()-1));			
	}
	
	//######################################################################
	public void addEdit (int textSize, int fieldEms, boolean mask) {
		EditText newinput = new EditText(context);
		newinput.setSingleLine();
		newinput.setEms(fieldEms);
		newinput.setTextSize(textSize);
		newinput.setPadding(5,0,0,5);
		if (mask) {
			newinput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		}
		input.add(newinput);
		response.add("");
		dialogLayout.addView(input.get(input.size()-1));
	}
	
	//######################################################################
	public void addEdit (int textSize, int fieldEms) {
		addEdit(textSize, fieldEms, false);
	}
	
	//######################################################################
	public void addButtons( GraphicButton okButton, GraphicButton cancelButton) {
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				for (int i=0; i<input.size(); i++) {
					response.set(i,input.get(i).getText().toString());
				}
				dismiss();
				if (input.size() > 0 ) input.get(0).requestFocus();
				newResponse=1;
			}
		});
		this.addButton(okButton);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if (input.size() > 0 ) input.get(0).requestFocus();
				newResponse=0;
			}
		});
		this.addButton(cancelButton);
	}
	
	//######################################################################
	public String[] getAllResponses() {
		String[] responses = response.toArray(new String[response.size()]);
		clearFields();
		return responses;
	}
	
	//######################################################################
	public int getButtonResponse() {
		if (newResponse == 1) {
			newResponse = -1;
			return 1;
		} else if (newResponse == 0){
			newResponse = -1;
			return 0;
		} else {
			newResponse = -1;
			return -1;
		}
	}
	
	//######################################################################
	public String getResponse() {
		String resp = response.get(0);
		clearFields();
		return resp;
	}
	
	//######################################################################
	public void clearFields() {
		for (int i=0; i<input.size(); i++) {
			response.set(i,null);
			input.get(i).setText("");
		}
		input.get(0).requestFocus();
	}
		
} // END
