package com.musicglove.views;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;

// A generic custom dialog that permits inserting a custom layout.
// set the title, set the content (or give it a string), and add however many buttons you want.
public class CustomDialog extends Dialog {
	
	protected Context context;
	private LinearLayout dialogLayout;
	private LinearLayout titleContainer;
	private RelativeLayout contentContainer;
	private LinearLayout buttonContainer;
	private TextView title;
	private TextView content;
	protected Graphics graphics;
	
	private int w;
	private int h;
	
	//######################################################################
	public CustomDialog(Context contxt, Graphics g, Sprite image, int leftpad, int toppad, int rightpad, int bottompad) {
		super(contxt);
		context = contxt;
		graphics = g;
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		this.getWindow().setBackgroundDrawable(new BitmapDrawable(context.getResources(), image.bitmap));
		w=image.w;
		h=image.h;
		
		dialogLayout = new LinearLayout(context);
		dialogLayout.setOrientation(LinearLayout.VERTICAL);
		dialogLayout.setPadding(graphics.convX(leftpad), graphics.convY(toppad), graphics.convX(rightpad), graphics.convY(bottompad));
		
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		titleContainer = new LinearLayout(context);
		titleContainer.setOrientation(LinearLayout.VERTICAL);
		titleContainer.setGravity(Gravity.CENTER_HORIZONTAL);
		titleContainer.setLayoutParams(llp);
		dialogLayout.addView(titleContainer);
		
		contentContainer = new RelativeLayout(context);
		contentContainer.setLayoutParams(llp);
		dialogLayout.addView(contentContainer);
		
		buttonContainer = new LinearLayout(context);
		buttonContainer.setOrientation(LinearLayout.HORIZONTAL);
		buttonContainer.setGravity(Gravity.CENTER_HORIZONTAL);
		buttonContainer.setPadding(15, 8, 15, 15);
		buttonContainer.setLayoutParams(llp);
		dialogLayout.addView(buttonContainer);

		this.setContentView(dialogLayout);
	}
	
	//######################################################################
	@Override
	public void show() {
		super.show();
		this.getWindow().setLayout(w, h);
	}

	//######################################################################
	public void clear () {
		if(dialogLayout!=null){dialogLayout.removeAllViews();}
		if(titleContainer!=null){titleContainer.removeAllViews();}
		if(contentContainer!=null){contentContainer.removeAllViews();}
		if(buttonContainer!=null){buttonContainer.removeAllViews();}
		dialogLayout=null;
		titleContainer=null;
		contentContainer=null;
		buttonContainer=null;
	}
	
	//######################################################################
	public void setTitle(String text, String fontName, int fontSize, int fontColor, Sprite image) {
		title = new TextView(context);
		if (fontName != null) {
			Typeface font = Typeface.createFromAsset(graphics.context.getAssets(),fontName);
			title.setTypeface(font);
		}
		title.setTextSize(fontSize);
		title.setTextColor(fontColor);
		title.setText(text);
		title.setGravity(Gravity.CENTER);
		title.setBackground(new BitmapDrawable(context.getResources(), image.bitmap));
		title.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		titleContainer.addView(title);
	}
	
	//######################################################################
	public void setContent(String text, String fontName, int fontSize, int fontColor, Boolean center) {
		content = new TextView(context);
		if (fontName != null) {
			Typeface font = Typeface.createFromAsset(graphics.context.getAssets(),fontName);
			content.setTypeface(font);
		}
		content.setPadding(15,15,15,25);
		content.setTextSize(fontSize);
		content.setTextColor(fontColor);
		content.setText(text);
		content.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		if (center) {
			content.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			content.setGravity(Gravity.CENTER);
		}
		contentContainer.addView(content);
	}
	
	//######################################################################
	public void setContent(LinearLayout newContent) {
		contentContainer.addView(newContent);  
	}
	
	//######################################################################
	public void addButton(GraphicTextButton button) {
		buttonContainer.addView(button);
	}
	
	//######################################################################
	public void addButton(GraphicButton button) {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.weight = 1;
		button.setLayoutParams(params);
		buttonContainer.addView(button);
	}
	
	
} // END
