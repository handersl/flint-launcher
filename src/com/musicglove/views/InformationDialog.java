package com.musicglove.views;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;

// generic pop showing information and an ok button
public class InformationDialog extends CustomDialog {

	//######################################################################
	public InformationDialog(Context context, Graphics g, String title, String info, float sX, float sY, int fs, int fst) {
		super(context, g, new Sprite(g, "popup_0_box",sX,sY), (int) (24*sX), (int) (25*sY), (int) (24*sX), (int) (24*sY));
		setTitle(title, "coolvetica_rg.ttf", fst, Color.RED, new Sprite(graphics, "popup_1_titlebar",sX,1.0f));
		setContent(info, "coolvetica_rg.ttf", fs, Color.BLACK, true);
		addButtonGraphic("animated_popupok_popupok1");
	}
	
	//######################################################################
	public InformationDialog(Context context, Graphics g, Sprite image, int leftpad, int toppad, int rightpad, int bottompad) {
		super(context, g, image, leftpad, toppad, rightpad, bottompad);
	}
	
	//######################################################################
	public void addButtonGraphic (String image) {
		GraphicButton okButton = new GraphicButton(context, graphics, image);
		okButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismiss();
			}
		});
		this.addButton(okButton);
	}
	
} // END
