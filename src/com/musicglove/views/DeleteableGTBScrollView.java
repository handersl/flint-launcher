package com.musicglove.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.musicglove.graphics.Sprite;

// modification to the GraphicTextButtonScrollView class to permit
// deleting of items via a long click.
public class DeleteableGTBScrollView extends GraphicTextButtonScrollView {

	public boolean deleteSelection;	
	private List<Boolean> isProtected;

	//######################################################################
	public DeleteableGTBScrollView(Context context) {
		super(context);
	}
	
	//######################################################################
	public DeleteableGTBScrollView(Context context, Sprite inImage, int padleft, int padtop, int padright, int padbottom) {
		super(context, inImage, padleft, padtop, padright, padbottom);
		isProtected = new ArrayList<Boolean>();
		deleteSelection = false;
	}

	//######################################################################
	public void clear() {
		super.clear();
		if(isProtected!=null){isProtected.clear();}
		isProtected=null;
	}
	
	//######################################################################
	@Override
	public void addListItem(GraphicTextButton item, String description, Boolean protect) {
		super.addListItem(item, description, true);
		isProtected.add(protect);
		if (!protect) {
			item.setOnLongClickListener(new View.OnLongClickListener() {
				public boolean onLongClick(View v) {
					longClicked(v);
					return true;
				}
			});
		}
	}
	
	//######################################################################
	@Override
	public void addListItem(GraphicTextButton item, String description) {
		addListItem(item, description, false);
	}
		
	//######################################################################	
	public void removeListItem() { 
		if (activeIndex != -1) {
			if (isProtected.get(activeIndex)) {	
				return;
			}
			itemContainer.removeView(itemList.get(activeIndex));
			itemList.remove(activeIndex);
			descriptionList.remove(activeIndex);
			isProtected.remove(activeIndex);
			stateList.remove(activeIndex);
			activeIndex = -1;
			refresh();
		}
	}

	//######################################################################
	public void longClicked(View v) {
		int i;
		for (i=0;i<itemList.size();i++) {
			if (v.getId() == itemList.get(i).getId()) {
				break;
			}
		}
		if (activeIndex != -1) {
			itemList.get(activeIndex).setState(0);
			itemList.get(activeIndex).setTextColor(Color.BLACK);
		}
		activeIndex = i;
		activeIndex = i;
		itemList.get(activeIndex).setState(1);
		itemList.get(activeIndex).setTextColor(Color.YELLOW);
		//changedSelection = true;
		deleteSelection = true;
	}
	
	//######################################################################	
	public boolean deleteSelection() {
		if (deleteSelection == true) {
			deleteSelection = false;
			return true;
		}
		return false;
	}
	
} // END
