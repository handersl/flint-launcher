package com.musicglove.views;

import com.flint.flintlauncher.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class SplashScreen extends RelativeLayout{

	private Context context;
	private ImageView view;
	private AnimationDrawable loading;
	
	//######################################################################
	public SplashScreen (Context contxt) {
		super(contxt);
		context = contxt;
	}
	
	//######################################################################
	public void setup() {
		view = new ImageView(context);
		loading = new AnimationDrawable();
		loading.addFrame(getResources().getDrawable(R.drawable.animated_loadingtemp_loadingtemp0), 250);
		loading.addFrame(getResources().getDrawable(R.drawable.animated_loadingtemp_loadingtemp1), 250);
		loading.addFrame(getResources().getDrawable(R.drawable.animated_loadingtemp_loadingtemp2), 250);
		loading.addFrame(getResources().getDrawable(R.drawable.animated_loadingtemp_loadingtemp3), 250);
		loading.setOneShot(false);
		view.setBackgroundColor(Color.WHITE);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.CENTER_IN_PARENT);
		view.setImageDrawable(loading);
		this.addView(view, lp);
		this.setBackgroundColor(Color.WHITE);
		loading.start();
	}
	
	//######################################################################
	public void clear() {
		if (loading!=null) {loading.stop();}
		loading = null;
		view = null;
	}
	
} // END
