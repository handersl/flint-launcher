package com.musicglove.audio;

import com.flint.flintlauncher.R;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.IBinder;

public class MediaPlayerService extends Service {
	
	public static final String TAG = "MG_MediaPlayerServce";
	MediaPlayer mediaPlayer;
	boolean playedOnce = false;
	boolean active = false;
	
	private final int UID = (int)(Math.random()*1000);
	
	//######################################################################	
	@Override
	public void onCreate() {
		super.onCreate();
		mediaPlayer = MediaPlayer.create(this, R.raw.ambientmusic_mg_music_mp3);
		mediaPlayer.setLooping(false);
		mediaPlayer.setVolume(0.25f, 0.25f);
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				playedOnce = true;
			}
		});
	}
	
	//######################################################################	
	public IBinder onBind (Intent intent) {
		return null;
	}
	
	//######################################################################	
	public IBinder onUnBind(Intent intent) {
		return null;
	}
	
	//######################################################################	
	@Override
	public void onDestroy() {
		if (mediaPlayer!=null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
			mediaPlayer.release();	
			mediaPlayer = null;
		}
	}
	
	//######################################################################	
	@Override
	public int onStartCommand(Intent intent, int flags, int startid) {
		Bundle values = intent.getExtras();
		if (values != null) {
			active = true;
			if (values.getBoolean("playagain")) {
				playedOnce = true;
			}
			if (values.getBoolean("restart")) {
				if (!mediaPlayer.isPlaying()) {
					playedOnce = false;
					mediaPlayer.seekTo(0);
					mediaPlayer.start();
				}
			}
			if (values.getBoolean("pause")) {
				if (mediaPlayer.isPlaying()) {
					mediaPlayer.pause();
				}
			}
			if (values.getBoolean("unpause")) {
				if (!playedOnce && !mediaPlayer.isPlaying()) {
					mediaPlayer.start();
				}
			}
		} else if (active == false) {
			mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					if (!playedOnce) {
						mediaPlayer.start();
						active = true;
					}
				}
			});
		} else {
			if (!mediaPlayer.isPlaying()) {
				playedOnce = false;
				mediaPlayer.seekTo(0);
				mediaPlayer.start();
			}
		}
		return Service.START_NOT_STICKY;
	}
	
	//######################################################################	
	@Override
	public void onLowMemory() {
		// do nothing
	}
	
} // END
