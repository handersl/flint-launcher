package com.musicglove.scenes;

import java.io.File;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Random;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.flint.flintlauncher.R;
import com.musicglove.audio.MediaPlayerService;
import com.musicglove.data.ConfigIni;
import com.musicglove.data.DataFile;
import com.musicglove.data.User;
import com.musicglove.data.UserDatabase;
import com.musicglove.data.InfoDat;
import com.musicglove.flintlauncher.MainActivity;
import com.musicglove.flintlauncher.SettingsMenu;
import com.musicglove.graphics.Animation;
import com.musicglove.graphics.Graphics;
import com.musicglove.graphics.Sprite;
import com.musicglove.helper.MyAdminReceiver;
import com.musicglove.helper.PassedValues;
import com.musicglove.views.DeleteableGTBScrollView;
import com.musicglove.views.GraphicButton;
import com.musicglove.views.GraphicIconButton;
import com.musicglove.views.GraphicTextButton;
import com.musicglove.views.GraphicsView;
import com.musicglove.views.InformationDialog;
import com.musicglove.views.InputStringsDialog;
import com.musicglove.views.SplashScreen;

public class ActivityMainMenuScene extends Scene {
	//private final String TAG = "MG_MainMenuSceneActivity";
	private static final boolean DEBUG = false;
	private static final boolean TRBUTTON = false;
	private static final boolean BRBUTTON = true;
	private static final boolean BLBUTTON = false;
	
	private static String VersionText = "v1.0.7";

	//*********************************************************************************************
	//************************** Activity Constants ***********************************************
	//*********************************************************************************************
	
	private final int B_OK = 1; // button OK
	
	//*********************************************************************************************
	//************************** Activity Variables ***********************************************
	//*********************************************************************************************
	
	// current state
	private int status = PassedValues.CODE_LOADING;
	private boolean loaded = false; // flag for when the activity is full loaded.
	private PassedValues values;
	private int nextScene = PassedValues.CODE_BUSY;	
	private int partsLoaded = 0;
	private boolean pauseLoading = false;
	private boolean cleanedUp = true;
	private boolean deletingUser = false;
	
	private DataFile usersfile;
	//private DataFile datefile;
	
	private Thread mainThread;
	private Thread loadBackgroundThread;

	private SoundPool soundPool;
	private int soundLowBoom;
	private int soundClick;
	private int soundBubbleBoing;
	
	private int[] userNotes;
	private float[] userPercent;
	private int[] userTime;
	private String[] userName;
	
	private boolean wakingup = false;
	
	//*********************************************************************************************
	//************************** Activity Drawing Elements ****************************************
	//*********************************************************************************************	
	
	private Toast volToast = null;
	
	private int userIconImageIndex;
	private int userDataImageIndex;
	private int totalNotesImageIndex;
	private int totalPercentImageIndex;
	private int totalTimeImageIndex;
	private int totalNotesShadowImageIndex;
	private int totalPercentShadowImageIndex;
	private int totalTimeShadowImageIndex;
	
	private GraphicButton logoButton;
	private GraphicButton selectUserButton;
	private GraphicTextButton createNewUserButton;
	private GraphicButton passwordButton;
	private GraphicButton playButton;
	private GraphicButton analyticsButton;
	private GraphicButton logoutButton;
	private GraphicButton settingsButton;
	private GraphicIconButton userPasswordButton;
	
	private Button topRightButton;
	private Button bottomRightButton;
	private Button bottomLeftButton;
	private long trTime = 0;
	private long brTime = 0;
	private long blTime = 0;
	private int trCount = 0;
	private int brCount = 0;
	private int blCount = 0;
	private Toast toastHandle;
	
	private GraphicsView graphicsView;
	private DeleteableGTBScrollView selectUserScroll;
	
	private InputStringsDialog createNewUserDialog; // LS Size Good (5-19-2014)
	private InputStringsDialog passwordDialog; // LS Size Good (5-19-2014)
	private InformationDialog logoButtonDialog; 
	private InputStringsDialog deleteUserDialog; // LS Size Good (5-19-2014)
	private InformationDialog selectUserInfoDialog;
	private InformationDialog unmatchingOldPasswordDialog; // LS Size Good (5-19-2014)
	private InformationDialog unmatchingNewPasswordDialog; // LS Size Good (5-19-2014)
	private InformationDialog incorrectPasswordDialog; // LS Size Good (5-19-2014)
	private InputStringsDialog checkPasswordDialog; // LS Size Good (5-19-2014)
	private InformationDialog userAlreadyExistsDialog; // LS Size Good (5-19-2014)
	
	private InputStringsDialog needToSetSytemTimeDialog;
		
	private SplashScreen splashScreen;

	private DevicePolicyManager mDevicePolicyManager; 
	private ComponentName mComponentName; 
	
	//######################################################################	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler(this, ActivityMainMenuScene.class));
		
		Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mComponentName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,"Administrative access is necessary to lock the screen when the user logs off.");
        startActivityForResult(intent, 0);
  
		boolean proceed = true;
		
		values = new PassedValues();

		
		if (savedInstanceState != null) {
			proceed = savedInstanceState.getBoolean("screenlock");
		}
		if (proceed) {
			values.receiveValues(getIntent().getExtras());
			
			ConfigIni con = new ConfigIni(this);
			con.loadData();
			values.version = con.getVersion();
			values.subversion = con.getSubversion();
			
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				//Instead of loading the user name screen just move to the launcher screen.
				Intent intentLauncher = new Intent(ActivityMainMenuScene.this, MainActivity.class);
				intentLauncher = values.sendValues(intentLauncher);
				startActivity(intentLauncher);
				cleanUp();
				finish();
				
				//setupGraphics(new Graphics(this, getWindowManager().getDefaultDisplay(), paint, opts, r, 1.161f, false, 0.5f, 0.66f));
				//this.getWindow().getDecorView().setSystemUiVisibility(
				//		View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
				//} else if (values.subversion.equals("10inchHD")) {
				//	setupGraphics(new Graphics(this, getWindowManager().getDefaultDisplay(), paint, opts, r, 1.114f, true));
				//} else if (values.subversion.equals("fullscreen")) {
				//	setupGraphics(new Graphics(this, getWindowManager().getDefaultDisplay(), paint, opts, r, 1.0f, false));
			} else {//"highdef" slate+
				setupGraphics(new Graphics(this, getWindowManager().getDefaultDisplay(), paint, opts, r, 1.0f, true));
			}
			
			splashScreen = new SplashScreen(this);
			splashScreen.setup();
			
			mDevicePolicyManager = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);  
			mComponentName = new ComponentName(this, MyAdminReceiver.class);  
			
			this.setContentView(splashScreen);
			
			//loadBackgroundThread();
			//loadBackgroundThread.start();

		} else {		
			values.receiveValues(getIntent().getExtras());
			ConfigIni con = new ConfigIni(this);
			con.loadData();
			values.version = con.getVersion();
			values.subversion = con.getSubversion();
			values.status = PassedValues.CODE_MAINMENU;
			values.userName = null;
			
			//TODO CHANGE TO SOMETHING HERE!!!
			/*Intent intent2 = new Intent(ActivityMainMenuScene.this, ActivityMain.class);
			intent2 = values.sendValues(intent2);
			//setResult(PassedValues.CODE_MAINMENU, intent2);
			startActivity(intent2);
			finish();*/
		}      
	}

	//######################################################################	
	private void loadBackgroundThread () {
		loadBackgroundThread = new Thread() {        	
	        @Override
	        public void run() {
	        	
	        	if ((!pauseLoading) && (partsLoaded < 1)) {
	    		// load users file
		    		usersfile = new DataFile(ActivityMainMenuScene.this, "users.dat", "file_users_dat", ":");
		    		usersfile.loadData(); 
		    		if (usersfile.getListSize() > 0) {
			    		userNotes = new int[usersfile.getListSize()];
		    			userPercent = new float[usersfile.getListSize()];;
		    			userTime = new int[usersfile.getListSize()];
		    			userName = new String[usersfile.getListSize()];
		    		} else {
		    			userNotes = new int[1];
		    			userPercent = new float[1];;
		    			userTime = new int[1];
		    			userName = new String[1];
		    		}
	    			partsLoaded++;
	        	}
	        	
	        	if ((!pauseLoading) && (partsLoaded < 2)) {
	    			PassedValues temp = new PassedValues();
	    			if (usersfile.getListSize() > 0) {
			    		for (int i=0; i<usersfile.getListSize(); i++) {
			    			temp.userName = usersfile.getCurrent(0);
			    			UserDatabase udb = new UserDatabase(ActivityMainMenuScene.this);
			    			udb.setUser(new User(ActivityMainMenuScene.this, temp)); 
			    			userName[i] = usersfile.getCurrent(0);
			    			userNotes[i] = udb.getAllNotesHit();
			    			userPercent[i] = udb.getAllPercentHit()*100;
			    			userTime[i] = udb.getAllTimePlayed();
			    			usersfile.incrIndex();
			    		}
	    			} else {
	    				temp.userName = "Guest";
		    			UserDatabase udb = new UserDatabase(ActivityMainMenuScene.this);
		    			udb.setUser(new User(ActivityMainMenuScene.this, temp)); 
		    			userName[0] = "Guest";
		    			userNotes[0] = udb.getAllNotesHit();
		    			userPercent[0] = udb.getAllPercentHit()*100;
		    			userTime[0] = udb.getAllTimePlayed();
		    			//usersfile.incrIndex();
	    			}
		    		partsLoaded++;
	        	}
	        	
	        	if ((!pauseLoading) && (partsLoaded < 3)) {
		    		// setup music
		    		soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		    		setupSound();
		    		partsLoaded++;
	        	}	
	        	
	        	if ((!pauseLoading) && (partsLoaded < 4)) {
		    		// setup animated background
		    		graphicsView = new GraphicsView(ActivityMainMenuScene.this, graphics, graphics.width, graphics.height);
		    		partsLoaded++;
	        	}	
	        	
	        	setupAnimatedBackground();
	        	
	        	if ((!pauseLoading) && (partsLoaded < 23)) {
		    		//setup the buttons
		    		setupButtons();
		    		partsLoaded++;
	        	}	
	        	
	        	if ((!pauseLoading) && (partsLoaded < 24)) {
			    	// setup Dialog popups
	        		setupDialogs();
	        		partsLoaded++;
	        	}
	        	
	        	if ((!pauseLoading) && (partsLoaded < 25)) {
				    // setup event handlers
	        		setEventHandlers();
	        		partsLoaded++;
	        	}

	        	
	    		// if someone is logged in, make sure it is a valid username
	        	if ((!pauseLoading) && (partsLoaded < 26)) {
					//if (values.userName != null) {
		    		//	selectUserScroll.setSelected(values.userName);
		    		//	selectUserScroll.changedSelection = false; // to avoid the click noise
		    		//	if (selectUserScroll.getSelected() == null) { // the user does not exist, log out
			    	//		values.userName = null;	
		    		//	}
		    		//} 

					// if home version, log in the Guest
		    		if (values.version.equals("home")) {
		    			//logger.warn("setting username to Guest");
		    			values.userName = "Guest";
		    			selectUserScroll.setSelected(values.userName);
		    			selectUserScroll.changedSelection = false; // to avoid the click noise
		    		} else if (values.userName != null) {// if someone is logged in, make sure it is a valid username
		    			selectUserScroll.setSelected(values.userName);
		    			selectUserScroll.changedSelection = false; // to avoid the click noise
		    			if (selectUserScroll.getSelected() == null) { // the user does not exist, log out
			    			values.userName = null;	
		    			}
		    		} 
		    		
		    		//logger.warn("username = " + values.userName);
	    		
		    		if (values.userName != null) {
			    		passwordButton.setVisibility(View.INVISIBLE);
			    		//if(userPasswordButton!=null){
	//
			    		//}
			    		userPasswordButton = new GraphicIconButton(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "usericon"), new Sprite(graphics, " "+values.userName, "coolvetica_rg.ttf", 50, Color.argb(255,60,60,60)), 0.019f, 0.035f, true);
						
		    			//userPasswordButton.changeText(new Sprite (graphics, "  "+selectUserScroll.getSelected(), "coolvetica_rg.ttf", 50, Color.BLACK), 0.019f, 0.035f, true);
		    			//userPasswordButton.setVisibility(View.VISIBLE);
		    			selectUserScroll.setVisibility(View.INVISIBLE);
		    			playButton.setVisibility(View.VISIBLE);
		    			settingsButton.setVisibility(View.VISIBLE);
		    			analyticsButton.setVisibility(View.VISIBLE);
		    			logoutButton.setVisibility(View.VISIBLE);
						graphicsView.imageList.get(userDataImageIndex).hidden=false;
						graphicsView.imageList.get(userIconImageIndex).hidden=true;
						int index = -1;
						for (int i=0; i<userName.length; i++) {
							if (userName[i].equals(selectUserScroll.getSelected())) {
								index = i;
								break;
							}
						}
						if (index != -1) {
							graphicsView.imageList.get(totalNotesImageIndex).spriteRef.setText(Integer.toString(userNotes[index]));
							graphicsView.imageList.get(totalTimeImageIndex).spriteRef.setText(getFormattedTime(userTime[index]));
							if (Float.toString(userPercent[index]).length() > 4) {
								graphicsView.imageList.get(totalPercentImageIndex).spriteRef.setText((Float.toString(userPercent[index])).substring(0,4)+"%");
								graphicsView.imageList.get(totalPercentShadowImageIndex).spriteRef.setText((Float.toString(userPercent[index])).substring(0,4)+"%");
							} else {
								graphicsView.imageList.get(totalPercentImageIndex).spriteRef.setText(Float.toString(userPercent[index])+"%");
								graphicsView.imageList.get(totalPercentShadowImageIndex).spriteRef.setText(Float.toString(userPercent[index])+"%");
							}
							graphicsView.imageList.get(totalNotesShadowImageIndex).spriteRef.setText(Integer.toString(userNotes[index]));
							graphicsView.imageList.get(totalTimeShadowImageIndex).spriteRef.setText(getFormattedTime(userTime[index]));
						} else {
							graphicsView.imageList.get(totalNotesImageIndex).spriteRef.setText("0");
							graphicsView.imageList.get(totalPercentImageIndex).spriteRef.setText("0%");
							graphicsView.imageList.get(totalTimeImageIndex).spriteRef.setText("0:00");
							graphicsView.imageList.get(totalNotesShadowImageIndex).spriteRef.setText("0");
							graphicsView.imageList.get(totalPercentShadowImageIndex).spriteRef.setText("0%");
							graphicsView.imageList.get(totalTimeShadowImageIndex).spriteRef.setText("0:00");
						}
						graphicsView.imageList.get(totalNotesImageIndex).hidden=false;
						graphicsView.imageList.get(totalPercentImageIndex).hidden=false;
						graphicsView.imageList.get(totalTimeImageIndex).hidden=false;
						graphicsView.imageList.get(totalNotesShadowImageIndex).hidden=false;
						graphicsView.imageList.get(totalPercentShadowImageIndex).hidden=false;
						graphicsView.imageList.get(totalTimeShadowImageIndex).hidden=false;
		    		} else {
		    			passwordButton.setVisibility(View.VISIBLE);
		    			if(userPasswordButton!=null){userPasswordButton.clear();}
		    			//userPasswordButton.setVisibility(View.INVISIBLE);
		    			selectUserScroll.setVisibility(View.VISIBLE);
		    			playButton.setVisibility(View.INVISIBLE);
		    			analyticsButton.setVisibility(View.INVISIBLE);
		    			logoutButton.setVisibility(View.INVISIBLE);
		    			graphicsView.imageList.get(userDataImageIndex).hidden=true;
						graphicsView.imageList.get(userIconImageIndex).hidden=false;
						graphicsView.imageList.get(totalNotesImageIndex).hidden=true;
						graphicsView.imageList.get(totalPercentImageIndex).hidden=true;
						graphicsView.imageList.get(totalTimeImageIndex).hidden=true;
						graphicsView.imageList.get(totalNotesShadowImageIndex).hidden=true;
						graphicsView.imageList.get(totalPercentShadowImageIndex).hidden=true;
						graphicsView.imageList.get(totalTimeShadowImageIndex).hidden=true;
		    		}
		    		partsLoaded++;
	        	}
	        	
	        	if ((!pauseLoading) && (partsLoaded < 27)) {
	        		layout.removeAllViews();
	        		layout.addView(graphicsView);
	        		if (values.subversion.equals("slate")) {
	        			layout.addView(logoButton);
	        			layout.addView(passwordButton);
	        		}
        			//layout.addView(userPasswordButton);
        			RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(selectUserScroll.image.w, selectUserScroll.image.h);
        			rl.setMargins(graphics.absX(0.686f) - selectUserScroll.image.w/2, graphics.absY(0.468f) - selectUserScroll.image.h/2, 0 ,0);
        			layout.addView(selectUserScroll, rl);
        			layout.addView(playButton);
        			layout.addView(settingsButton);
        			layout.addView(analyticsButton);
        			layout.addView(logoutButton);
        			if (TRBUTTON) {layout.addView(topRightButton);}
        			if (BRBUTTON) {
        				if (values.subversion.equals("slate")) {layout.addView(bottomRightButton);}
        			}
        			if (BLBUTTON) {layout.addView(bottomLeftButton);}
        			if (values.version.equals("research") || values.version.equals("clinic")) {
	        			if ((values.userName != null) && (userPasswordButton != null)) {
	        				layout.addView(userPasswordButton);
	        			}
        			}
	        		
	        		partsLoaded++;
	        	}	
	        	
	        	if ((!pauseLoading) && (partsLoaded < 28)) {
	        		// start
	    			changeContentView();
		    		partsLoaded++;
	        	}
	        	
	        	// unpause the music
    			Intent mediaIntent = new Intent(ActivityMainMenuScene.this, MediaPlayerService.class);
    			mediaIntent.putExtra("restart", true);
    			startService(mediaIntent);
	        	
	        	cleanedUp = false;
	        }
      	};
		loadBackgroundThread.start();
	}	

	//######################################################################	
	private void changeContentView() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				ActivityMainMenuScene.this.setContentView(rootLayout);
				splashScreen.clear();
				mainThread();
				/*Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
		        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mComponentName);
		        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,"Administrative access is necessary to lock the screen when the user logs off.");
		        startActivityForResult(intent, 0);*/
			}
		});
	}
	
	//######################################################################	
	private void mainThread () {
		mainThread = new Thread() {        	
	        @Override
	        public void run() {
	        	loaded = true;
	        	status = PassedValues.CODE_BUSY;
	        	
	        	checkSystemTime();
	        	
            	long startTime;
            	long tickTime = 0;
            	long lastTickTime = System.currentTimeMillis();

            	while (status == PassedValues.CODE_BUSY) {
            		if (graphicsView != null && graphicsView.updated == true) { // to avoid concurrent modification of arrays
            			graphicsView.changing = true;
	            		startTime = System.currentTimeMillis();
	            		tickTime = startTime-lastTickTime;
	            		lastTickTime = startTime;
          		
	            		// system time
            			if (needToSetSytemTimeDialog.getButtonResponse() == B_OK) {
            				forceSetSytemTime();
            			} 
	            		
	            		// add a user
	            		if (createNewUserDialog.getButtonResponse() == B_OK) {
	            			addUser();
	            		}
	            		
	            		// select a user
	            		if (selectUserScroll.changedSelection() == true) {
	            			changedUser();
	            		}
	            		
	            		// delete a user? confirm
	            		if (selectUserScroll.deleteSelection() == true) {
	            			confirmDeleteUser();
	            		}
	            		
	            		// delete a user
	            		if (deleteUserDialog.getButtonResponse() == B_OK) {
	            			permissionDeleteUser();
	            		}
	        
	            		// change password
	            		if (passwordDialog.getButtonResponse() == B_OK) {
	            			changePassword();
	            		}
	            		
	            		// selected user, check password
	            		if (checkPasswordDialog.newResponse != -1) {
	            			if (!deletingUser) {
	            				if (checkPasswordDialog.getButtonResponse() == B_OK) {
	            					selectUser();
	            				}
	            			} else {
	            				deletingUser = false;
	            				if (checkPasswordDialog.getButtonResponse() == B_OK) {
	            					deleteUser();
	            				}
	            			}
	            		}
	            			
	            		
	            		// choice made, go to next scene
	            		if (nextScene != PassedValues.CODE_BUSY) {
	            			if (nextScene == PassedValues.CODE_QUIT) {
	            				try { sleep(500); } catch (Exception e) {} // give time to see button press graphic
	            				//status = PassedValues.CODE_QUIT;
	            				nextScene = PassedValues.CODE_BUSY;
	            				logoutCB();
	            			} else if (nextScene == PassedValues.CODE_PLAY) {
	            				try { sleep(500); } catch (Exception e) {}
	            				status = PassedValues.CODE_PLAY;
	            				playCB();
	            			} else if (nextScene == PassedValues.CODE_ANALYTICS) {
	            				try { sleep(500); } catch (Exception e) {}
	            				status = PassedValues.CODE_ANALYTICS;
	            				analyticsCB();
	            			} else if (nextScene == PassedValues.CODE_OPTIONS) {
	            				try { sleep(500); } catch (Exception e) {}
	            				status = PassedValues.CODE_OPTIONS;
	            			}
	            		}
	            		
	            		try {
	            			graphicsView.update(tickTime); // GOT A NULL POINTER EXCEPTION HERE, SOME BAD RACE CONDITIONS Maybe
		            		graphicsView.postInvalidate();
	            		} catch (Exception e) {
	            			e.printStackTrace();
	            		}
	            		
           		
            		}
            	}
	        }
	    };
	    mainThread.start();
	}
	
	//######################################################################
	private void setupSound() {
		if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
			soundLowBoom = soundPool.load(this, R.raw.sounds_switch, 1);
		} else { // slate
			soundLowBoom = soundPool.load(this, R.raw.sounds_lowboom, 1);
		}
		soundClick = soundPool.load(this, R.raw.sounds_clicksound, 1);
		soundBubbleBoing = soundPool.load(this, R.raw.sounds_bubbleboing, 1);
	}
	
	//######################################################################	
	private void setupAnimatedBackground() {
		int imageLayer;
		
		if ((!pauseLoading) && (partsLoaded < 5)) {
			graphicsView.addImage( "mainmenu_0_bg0", 0.5f, 0.5f );
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 6)) {	
			imageLayer = graphicsView.addImage( "mainmenu_1_wave0", 0.5f, 0.234f );
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.imageList.get(imageLayer).addMotionSway(0f, 0.007f, 0f, 12000, 2); // these are the original python settings
			} else { // slate
				graphicsView.imageList.get(imageLayer).addMotionSway(0f, 0.05f, 0.2f, 12000, 1); // but I found these look better on the 10" tablet
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 7)) {
			imageLayer = graphicsView.addImage( "mainmenu_2_wave1", 0.398f, 0.219f );
			graphicsView.imageList.get(imageLayer).addMotion( 0f, 0f, 0f, 2500); 
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.imageList.get(imageLayer).addMotionSway( 0f, -0.01f, 0f, 8500, 4);
			} else { // slate
				graphicsView.imageList.get(imageLayer).addMotionSway( 0, -0.02f, 0.2f, 8500, 3);
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 8)) {		
			imageLayer = graphicsView.addImage( "mainmenu_3_wave2", 0.485f, 0.25f );
			graphicsView.imageList.get(imageLayer).addMotion( 0f, 0f, 0f, 1000);
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.imageList.get(imageLayer).addMotionSway( 0f, 0.018f, 0f, 10000, 3);
			} else { // slate
				graphicsView.imageList.get(imageLayer).addMotionSway( 0, 0.0436f, 0.2f, 10000, 2);
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 9)) {	
			graphicsView.addImage( "mainmenu_4_stillwaves",  0.5f, 0.5f );
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 10)) {		
			imageLayer = graphicsView.addImage( "mainmenu_5_wave3", 0.604f, 0.777f );
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.imageList.get(imageLayer).addMotionSway( 0f, -0.013f, 0f, 9000, 4);
			} else { // slate
				graphicsView.imageList.get(imageLayer).addMotionSway( 0, -0.026f, 0.2f, 9000, 3);
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 11)) {		
			imageLayer = graphicsView.addImage( "mainmenu_6_wave4", 0.518f, 0.733f );
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.imageList.get(imageLayer).addMotionSway( 0f, 0.011f, 0f, 7000, 2);
			} else { // slate
				graphicsView.imageList.get(imageLayer).addMotionSway( 0, 0.022f, 0.2f, 7000, 2);
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 12)) {
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.addImage( "mainmenu_7_bg1", 0.5f, 0.48f);
			} else { // slate
				graphicsView.addImage( "mainmenu_7_bg1", 0.5f, 0.5f);
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 13)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				imageLayer = graphicsView.addImage( "mainmenu_8_arc0", 0.371f, 0.449f );
			} else { // slate
				imageLayer = graphicsView.addImage( "mainmenu_8_arc0", 0.371f, 0.469f );
			}
			graphicsView.imageList.get(imageLayer).addMotion(0f, 0f, 360f, 11200, -1, -1);
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 14)) {
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				imageLayer = graphicsView.addImage( "mainmenu_9_arc1", 0.371f, 0.449f );
			} else { // slate
				imageLayer = graphicsView.addImage( "mainmenu_9_arc1", 0.371f, 0.469f );
			}
			graphicsView.imageList.get(imageLayer).addMotionSway( 0f, 0f, -270f, 9200, 3);	
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 15)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				imageLayer = graphicsView.addImage( "mainmenu_10_arc2", 0.371f, 0.449f);
			} else { // slate
				imageLayer = graphicsView.addImage( "mainmenu_10_arc2", 0.371f, 0.469f);
			}
			graphicsView.imageList.get(imageLayer).addMotionSwayHalf( 0f, 0f, 60f, 5000, 2);	
			graphicsView.imageList.get(imageLayer).addMotionSwayHalf( 0f, 0f, -120f, 7000, 2);
			graphicsView.imageList.get(imageLayer).addMotionSwayHalf( 0f, 0f, 60f, 5000, 2);
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 16)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				imageLayer = graphicsView.addImage( "mainmenu_11_arc3", 0.371f, 0.449f );
			} else { // slate
				imageLayer = graphicsView.addImage( "mainmenu_11_arc3", 0.371f, 0.469f );
			}
			graphicsView.imageList.get(imageLayer).addMotion(0f,0f,-360f,8500,-1,-1);
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 17)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.addImage( "mainmenu_12_redring",  0.371f, 0.449f );
			} else { // slate
				graphicsView.addImage( "mainmenu_12_redring",  0.371f, 0.469f );
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 18)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				graphicsView.addImage( "mainmenu_13_title", 0.5f, 0.863f);//0.857f );
			} else { // slate
				graphicsView.addImage( "mainmenu_13_title", 0.5f, 0.877f );
			}
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 19)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				userIconImageIndex = graphicsView.addAnimation("mmiconu", 0.371f, 0.468f);
			} else { // slate
				userIconImageIndex = graphicsView.addAnimation("mmiconu", 0.372f, 0.49f);
			}
			((Animation) graphicsView.imageList.get(userIconImageIndex)).setCue(0, 5, Animation.NEVER);
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 20)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				userDataImageIndex = graphicsView.addAnimation("mmicond", 0.371f, 0.468f);
			} else { // slate
				userDataImageIndex = graphicsView.addAnimation("mmicond", 0.372f, 0.49f);
			}
			((Animation) graphicsView.imageList.get(userDataImageIndex)).setCue(0, 5, Animation.NEVER);
			graphicsView.imageList.get(userDataImageIndex).hidden=true;
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 21)) {	
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				totalNotesShadowImageIndex = graphicsView.addImage("0", "coolvetica_rg.ttf", 30, Color.BLACK, 0.372f, 0.519f);
				totalPercentShadowImageIndex = graphicsView.addImage("0%", "coolvetica_rg.ttf", 30, Color.BLACK, 0.378f, 0.430f);
				totalTimeShadowImageIndex = graphicsView.addImage("0:00", "coolvetica_rg.ttf", 30, Color.BLACK, 0.372f, 0.341f);
				totalNotesImageIndex = graphicsView.addImage("0", "coolvetica_rg.ttf", 30, Color.argb(255,20,179,80), 0.371f, 0.520f);
				totalPercentImageIndex = graphicsView.addImage("0%", "coolvetica_rg.ttf", 30, Color.argb(255,20,179,80), 0.377f, 0.431f);
				totalTimeImageIndex = graphicsView.addImage("0:00", "coolvetica_rg.ttf", 30, Color.argb(255,20,179,80), 0.371f, 0.342f);
			} else { // slate
				totalNotesShadowImageIndex = graphicsView.addImage("0", "coolvetica_rg.ttf", 38, Color.BLACK, 0.373f, 0.541f);
				totalPercentShadowImageIndex = graphicsView.addImage("0%", "coolvetica_rg.ttf", 38, Color.BLACK, 0.379f, 0.452f);
				totalTimeShadowImageIndex = graphicsView.addImage("0:00", "coolvetica_rg.ttf", 38, Color.BLACK, 0.375f, 0.363f);
				totalNotesImageIndex = graphicsView.addImage("0", "coolvetica_rg.ttf", 38, Color.argb(255,20,179,80), 0.372f, 0.542f);
				totalPercentImageIndex = graphicsView.addImage("0%", "coolvetica_rg.ttf", 38, Color.argb(255,20,179,80), 0.378f, 0.453f);
				totalTimeImageIndex = graphicsView.addImage("0:00", "coolvetica_rg.ttf", 38, Color.argb(255,20,179,80), 0.374f, 0.364f);
			}
			graphicsView.imageList.get(totalNotesShadowImageIndex).hidden=true;
			graphicsView.imageList.get(totalPercentShadowImageIndex).hidden=true;
			graphicsView.imageList.get(totalTimeShadowImageIndex).hidden=true;
			graphicsView.imageList.get(totalNotesImageIndex).hidden=true;
			graphicsView.imageList.get(totalPercentImageIndex).hidden=true;
			graphicsView.imageList.get(totalTimeImageIndex).hidden=true;
			partsLoaded++;
    	}
		if ((!pauseLoading) && (partsLoaded < 22)) {
			// add the "buy now" banner on the bottom
			if (values.version.equals("research") || values.version.equals("clinic")) {
				graphicsView.addImage("Purchase a home unit at musicglove.com", "coolvetica_rg.ttf", 44, Color.DKGRAY, 0.5f, 0.036f);
				graphicsView.addImage(VersionText, "coolvetica_rg.ttf", 12, Color.DKGRAY, 0.99f, 0.076f);
			} else {
				// add the version number on the bottom as well
				graphicsView.addImage(VersionText, "coolvetica_rg.ttf", 12, Color.DKGRAY, 0.915f, 0.1f);
			}
			partsLoaded++;
    	}

	}
	
	//######################################################################
	public void setupUserList() {
		// add select user button, above list
		selectUserButton = new GraphicButton(this, graphics, "userselect_selectuser");
		selectUserScroll.addButton(selectUserButton);
		
		// add create new user button, above list
		//createNewUserButton = new GraphicTextButton(this, "Create New User", "coolvetica_rg.ttf", 25, Color.WHITE, 1.0f, 1.3f, "animated_userscrollcreatebutton_userscrollcreatebutton1");
		//selectUserScroll.addButton(createNewUserButton);
		
		// prepare the empty list, no more adding buttons
		selectUserScroll.setupList();
		
		if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
			createNewUserButton = new GraphicTextButton(this, graphics, "Create New User", 20, Color.BLACK, 
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2",
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2",
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2",
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2");
		} else { // slate
			createNewUserButton = new GraphicTextButton(this, graphics, "Create New User", 32, Color.BLACK, 
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2",
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2",
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2",
					"animated_userscrollcreatebutton_userscrollcreatebutton0", "animated_userscrollcreatebutton_userscrollcreatebutton2");
		}
		selectUserScroll.addListItem(createNewUserButton, "Create New User", true);
		
		// add each user
		if (usersfile.getListSize() == 0) { // if this is the case, just force add guest
			if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
				selectUserScroll.addListItem(new GraphicTextButton(this, graphics, "Guest", 20, Color.BLACK,
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2"), "Guest", true);
			} else { // slate
				selectUserScroll.addListItem(new GraphicTextButton(this, graphics, "Guest", 32, Color.BLACK,
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
						"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2"), "Guest", true);
			}
		} else {	
			for (int index = 0; index < usersfile.getListSize(); index++) {
				usersfile.setIndex(index);
				
				if (usersfile.getCurrent(0).equals("Guest")) {
					if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
						selectUserScroll.addListItem(new GraphicTextButton(this, graphics, usersfile.getCurrent(0), 20, Color.BLACK,
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2"), "Guest", true);
					} else { // slate
						selectUserScroll.addListItem(new GraphicTextButton(this, graphics, usersfile.getCurrent(0), 32, Color.BLACK,
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2",
								"animated_userscrollguestbutton_userscrollguestbutton0", "animated_userscrollguestbutton_userscrollguestbutton2"), "Guest", true);
					}
				} else {
					// this one is longer since we have the alternate background colors depending on position in list ... which can change as users are added/removed
					if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
						selectUserScroll.addListItem(new GraphicTextButton(this, graphics, usersfile.getCurrent(0), 20, Color.BLACK,
								"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
								"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
								"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2",
								"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2"), usersfile.getCurrent(0), false);
					} else { // slate
						selectUserScroll.addListItem(new GraphicTextButton(this, graphics, usersfile.getCurrent(0), 32, Color.BLACK,
								"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
								"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
								"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2",
								"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2"), usersfile.getCurrent(0), false);
					}
				}
			}
		}
		
		// refresh the list so colors alternate
		selectUserScroll.refresh();
	}
	
	//######################################################################	
	public void setupDialogs() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				// since we are using the same graphic for all dialogs, we need to scale each accordingly. This is not going to be inherent to the class, so as to not anchor us to one bitmap.
				float scaleX;
				float scaleY;
				// change password dialog
				if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
					scaleX = 0.65f;
					scaleY = 1.15f;
				} else { // slate
					scaleX = 0.65f;
					scaleY = 0.95f;
				}
				passwordDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
				passwordDialog.setTitle("Change Password", "coolvetica_rg.ttf", 48, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
				passwordDialog.addText("Enter Old Password:", "coolvetica_rg.ttf", 32, Color.BLACK);
				passwordDialog.addEdit(32, 8, true);
				passwordDialog.addText("Enter New Password:", "coolvetica_rg.ttf", 32, Color.BLACK);
				passwordDialog.addEdit(32, 8, true);
				passwordDialog.addText("Confirm New Password:", "coolvetica_rg.ttf", 32, Color.BLACK);
				passwordDialog.addEdit(32, 8, true);
				passwordDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupcancel_popupcancel0"));
				
				scaleX = 0.75f;
				scaleY = 0.95f;
				if (values.subversion.equals("slate")) {
					scaleX = 0.8f;
					scaleY = 0.8f;
					needToSetSytemTimeDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					needToSetSytemTimeDialog.setTitle("WARNING", "coolvetica_rg.ttf", 48, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					needToSetSytemTimeDialog.addText("Your system's clock has been reset.\nWould you like to reset the date and time?\n\nThis is required for the Analytics suite to function properly.", "coolvetica_rg.ttf", 32, Color.BLACK);
					needToSetSytemTimeDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupcancel_popupcancel0"));
					needToSetSytemTimeDialog.showKeyboard = false;
				} else {
					needToSetSytemTimeDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					needToSetSytemTimeDialog.setTitle("WARNING", "coolvetica_rg.ttf", 30, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					needToSetSytemTimeDialog.addText("Your system's clock has been reset.\nWould you like to reset the date and time?\n\nThis is required for the Analytics suite to function properly.", "coolvetica_rg.ttf", 20, Color.BLACK);
					needToSetSytemTimeDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupcancel_popupcancel0"));
					needToSetSytemTimeDialog.showKeyboard = false;
				}
				
				// check password dialog
				if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
					scaleX = 0.65f;
					scaleY = 0.67f;
					checkPasswordDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					checkPasswordDialog.setTitle("Please enter password", "coolvetica_rg.ttf", 30, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					checkPasswordDialog.addText(" ", "coolvetica_rg.ttf", 25, Color.BLACK);
					checkPasswordDialog.addEdit(20, 8, true);
					checkPasswordDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this,graphics, "animated_popupcancel_popupcancel0"));
				} else {//highdef  slate
					scaleX = 0.75f;
					scaleY = 0.7f;
					checkPasswordDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					checkPasswordDialog.setTitle("Alert!", "coolvetica_rg.ttf", 48, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					checkPasswordDialog.addText(" ", "coolvetica_rg.ttf", 32, Color.BLACK);
					checkPasswordDialog.addEdit(32, 8, true);
					checkPasswordDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this,graphics, "animated_popupcancel_popupcancel0"));

				}
				
				// create new user dialog
				if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
					scaleX = 0.65f;
					scaleY = 0.65f;
					createNewUserDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					createNewUserDialog.setTitle("Create New User", "coolvetica_rg.ttf", 30, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					createNewUserDialog.addText("Enter Username:", "coolvetica_rg.ttf", 25, Color.BLACK);
					createNewUserDialog.addEdit(20, 8);
					createNewUserDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupcancel_popupcancel0"));
				} else {//highdef slate
					scaleX = 0.65f;
					scaleY = 0.6f;
					createNewUserDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					createNewUserDialog.setTitle("Create New User", "coolvetica_rg.ttf", 48, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					createNewUserDialog.addText("Enter Username:", "coolvetica_rg.ttf", 32, Color.BLACK);
					createNewUserDialog.addEdit(32, 8);
					createNewUserDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupcancel_popupcancel0"));
				}
				
				// logo information dialog
				String information = "MusicGlove\n"
						+"\n"
						+"Created by: Luke Heidbrink\n"
						+"Revised by: Dan Zondervan\n"
						+"\n"
						+"Version 0.9.1\n"
						+"\n"
						+"Original artwork by: who made the graphics?\n"
						+"Other artwork by: any artwork used from other public sources?\n"
						+"\n"
						+"Music credits? Song title and artist and year, etc\n"
						+"Sound effect credits? \n"
						+"\n"
						+"Copyright 2013\n"
						+"Flint Rehabilitation Devices, LLC\n";
				scaleX = 1.0f;
				scaleY = 1.25f;
				logoButtonDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
				logoButtonDialog.setTitle("MusicGlove Information", "coolvetica_rg.ttf", 20, Color.BLUE, new Sprite(graphics, "popup_1_titlebar"));
				logoButtonDialog.setContent(information, "coolvetica_rg.ttf", 15, Color.BLACK, false);
				logoButtonDialog.addButtonGraphic("animated_popupok_popupok1");
				
				// delete user dialog
				if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
					scaleX = 0.7f;
					scaleY = 0.58f;
					deleteUserDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					deleteUserDialog.setTitle("WARNING", "coolvetica_rg.ttf", 30, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					deleteUserDialog.addText("Are you sure you want to delete this user?", "coolvetica_rg.ttf", 25, Color.BLACK);
					deleteUserDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupcancel_popupcancel0"));
					deleteUserDialog.showKeyboard = false;
				} else {//highdef  slate
					scaleX = 0.65f;
					scaleY = 0.55f;
					deleteUserDialog = new InputStringsDialog(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "popup_0_box", scaleX, scaleY), (int) (24*scaleX), (int) (25*scaleY), (int) (24*scaleX), (int) (24*scaleY));
					deleteUserDialog.setTitle("WARNING", "coolvetica_rg.ttf", 48, Color.RED, new Sprite(graphics, "popup_1_titlebar",scaleX,1.2f));
					deleteUserDialog.addText("Are you sure you want to delete this user?", "coolvetica_rg.ttf", 32, Color.BLACK);
					deleteUserDialog.addButtons(new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupok_popupok1"), new GraphicButton(ActivityMainMenuScene.this, graphics, "animated_popupcancel_popupcancel0"));
					deleteUserDialog.showKeyboard = false;
				}
				
				if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) {
					selectUserInfoDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "Please Select a User", "Please select a user before continuing.", 0.65f, 0.65f, 25, 30);
					unmatchingOldPasswordDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "Incorrect Password", "Old password was not correct.", 0.65f, 0.60f, 25, 30);//g
					incorrectPasswordDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "Incorrect Password", "Incorrect Password.\nPlease enter the correct password to continue.", 0.65f, 0.6f, 25, 30);//good
					unmatchingNewPasswordDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "New Passwords need to be the same!", "New passwords did not match.", 0.65f, 0.65f, 25, 25);//g
					userAlreadyExistsDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "User Already Exists", "Please enter a different user name.", 0.60f, 0.48f, 25, 30);		
				} else { // slate
					selectUserInfoDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "Please Select a User", "Please select a user before continuing.", 0.65f, 0.5f, 32, 48);
					unmatchingOldPasswordDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "Incorrect Password", "Old password was not correct.\n", 0.65f, 0.5f, 32, 48);//g
					incorrectPasswordDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "Incorrect Password", "Please enter the correct password to continue.", 0.65f, 0.5f, 32, 48);//good
					unmatchingNewPasswordDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "Alert!", "New passwords did not match.\n", 0.65f, 0.5f, 32, 48);//g
					userAlreadyExistsDialog = new InformationDialog(ActivityMainMenuScene.this, graphics, "User Already Exists", "Please enter a different user name.\n", 0.65f, 0.5f, 32, 48);
				}
			}
		});
	}
	
	//######################################################################
	public void setupButtons() {
		
		topRightButton = new Button(this);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(150,150);
		lp.leftMargin = graphics.absX(1f)-200;
		lp.topMargin = graphics.absY(1f);
		topRightButton.setLayoutParams(lp);
		topRightButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	long now = System.currentTimeMillis();
            	if (now-trTime < 1000) {
            		trCount += 1;
            		if (trCount >= 10) {
            			topRightButtonCB(); 			
            		} else if (trCount >=6) {
            			if (toastHandle != null) toastHandle.cancel();
            			toastHandle = Toast.makeText(ActivityMainMenuScene.this, "Tap corner " +(10-trCount)+" more times to change the system time.", Toast.LENGTH_SHORT);
            			toastHandle.show();
            		}
            	} else {
            		trCount = 1;
            	}
            	trTime = now;
            }
        });
		//topRightButton.setVisibility(View.INVISIBLE);
		topRightButton.setBackgroundColor(Color.TRANSPARENT);
        
		bottomRightButton = new Button(this);
		lp = new RelativeLayout.LayoutParams(250,100);
		lp.leftMargin = graphics.absX(1f)-250;
		lp.topMargin = graphics.absY(0f)-100;
		bottomRightButton.setLayoutParams(lp);
		bottomRightButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	long now = System.currentTimeMillis();
            	if (now-brTime < 1000) {
            		brCount += 1;
            		if (brCount >= 10) {
            			brCount = 1;
            			bottomRightButtonCB(); 			
            		} else if (brCount >=6) {
            			if (toastHandle != null) toastHandle.cancel();
            			toastHandle = Toast.makeText(ActivityMainMenuScene.this, "Tap logo " +(10-brCount)+" more times to change the system time", Toast.LENGTH_SHORT);
            			toastHandle.show();
            		}
            	} else {
            		brCount = 1;
            	}
            	brTime = now;
            }
        });
		//bottomRightButton.setVisibility(View.INVISIBLE);
		bottomRightButton.setBackgroundColor(Color.TRANSPARENT);
		
		bottomLeftButton = new Button(this);
		lp = new RelativeLayout.LayoutParams(150,150);
		lp.leftMargin = graphics.absX(0f)+50;
		lp.topMargin = graphics.absY(0f)-200;
		bottomLeftButton.setLayoutParams(lp);
		bottomLeftButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	long now = System.currentTimeMillis();
            	if (now-blTime < 1000) {
            		blCount += 1;
            		if (blCount >= 10) {
            			bottomLeftButtonCB(); 			
            		} else if (blCount >=6) {
            			if (toastHandle != null) toastHandle.cancel();
            			toastHandle = Toast.makeText(ActivityMainMenuScene.this, "Bottom-Left Button: " +(10-blCount)+" more clicks", Toast.LENGTH_SHORT);
            			toastHandle.show();
            		}
            	} else {
            		blCount = 1;
            	}
            	blTime = now;
            }
        });
		//bottomLeftButton.setVisibility(View.INVISIBLE);
		bottomLeftButton.setBackgroundColor(Color.TRANSPARENT);
        		
		// setup Logo button
		logoButton = new GraphicButton(this, graphics, 0.94f, 0.036f, "logo7");

		// setup Change Password button	
		passwordButton = new GraphicButton(this, graphics, 0.019f, 0.035f, "animated_useditpass_useditpass0");

		// setup Change Password button for a User
		//userPasswordButton = new GraphicIconButton(this, graphics, new Sprite(graphics, "usericon"), new Sprite(graphics, " "+"Guest", "coolvetica_rg.ttf", 50, Color.argb(255,60,60,60)), 0.019f, 0.035f, true);
		//userPasswordButton.setVisibility(View.INVISIBLE);
		//userPasswordButton = new GraphicIconButton(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "usericon"), new Sprite(graphics, " "+"Guest", "coolvetica_rg.ttf", 50, Color.argb(255,60,60,60)), 0.019f, 0.035f, true);
		//userPasswordButton.setVisibility(View.INVISIBLE);
		//layout.addView(userButton);

		
		
		// setup User List and buttons
		// note, the padding is derived from the userselect_box.png. It has a blue border.
		// the +1 is a fudge factor
		selectUserScroll = new DeleteableGTBScrollView(this, new Sprite(graphics, "userselect_box"), graphics.convX(6), graphics.convY(7), graphics.convX(6), graphics.convY(7));
		selectUserScroll.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		setupUserList();
	
		
		
		// setup Play button
		playButton = new GraphicButton(this, graphics, 0.672f, 0.604f, "animated_mmplay_mmplay0", "animated_mmplay_mmplay1", "animated_mmplay_mmplay2");
		playButton.setVisibility(View.INVISIBLE);
		
		
		// setup analytics button
		analyticsButton = new GraphicButton(this, graphics, 0.672f, 0.454f, "animated_mmanalytics_mmanalytics0", "animated_mmanalytics_mmanalytics1", "animated_mmanalytics_mmanalytics2");
		analyticsButton.setVisibility(View.INVISIBLE);
		
		
		// setup log out button
		if (values.version.equals("home")) {
			logoutButton = new GraphicButton(this, graphics, 0.672f, 0.301f, "animated_mmquit_mmquit0", "animated_mmquit_mmquit1", "animated_mmquit_mmquit2");
		} else { //"clinical" or "research"
			logoutButton = new GraphicButton(this, graphics, 0.672f, 0.301f, "animated_mmlogout_mmlogout0", "animated_mmlogout_mmlogout1", "animated_mmlogout_mmlogout2");
		}
		logoutButton.setVisibility(View.INVISIBLE);
		
		// setup settings button
		settingsButton = new GraphicButton(this, graphics, 0.065f, 0.1f, "animated_mmplay_mmsettings0", "animated_mmplay_mmsettings1", "animated_mmplay_mmsettings2");
		settingsButton.setVisibility(View.VISIBLE);
	}
	
	
	//*********************************************************************************************
	//************************************* Logic ****************************************
	//*********************************************************************************************
	
	//######################################################################
	public void setEventHandlers() {
//		if (values.subversion.equals("highdef")) {
//			// logo button
//			logoButton.setOnClickListener( new View.OnClickListener() {
//				@Override 
//				public void onClick(View v) {
//					soundPool.play(soundClick, 1, 1, 0, 0, 1);
//					logoButton.setState(1);
//					logoButtonCB();
//				}
//			});
//		}
		// I've gotten some null pointer errors where the buttons don't seem to exist when I get here,
		// 	somehow due to how the loading life cycle is handled. So, I'll check if they are null,
		//	and if they are, I will just load the buttons again. Hopefully this works!
		if (playButton == null) {
			// be sure to clear all the old ones first, just in case!
			if(usersfile!=null){usersfile.clear();}
			if(logoButton!=null){logoButton.clear();}
			if(selectUserButton!=null){selectUserButton.clear();}
			if(createNewUserButton!=null){createNewUserButton.clear();}
			if(passwordButton!=null){passwordButton.clear();}
			if(analyticsButton!=null){analyticsButton.clear();}
			if(logoutButton!=null){logoutButton.clear();}
			if(userPasswordButton!=null){userPasswordButton.clear();}
			
			setupButtons();
		}

		
		// if home version, these buttons don't do anything.
		if (!values.version.equals("home") && values.subversion.equals("slate") ) {
			// password button
			passwordButton.setOnClickListener( new View.OnClickListener() {
				@Override 
				public void onClick(View v) {
					soundPool.play(soundClick, 1, 1, 0, 0, 1);
					passwordButton.setState(1);
					passwordCB();
				}
			});
			// user password button
/*			userPasswordButton.setOnClickListener( new View.OnClickListener() {
				@Override 
				public void onClick(View v) {
					soundPool.play(soundClick, 1, 1, 0, 0, 1);
					userPasswordButton.setClicked();
					passwordCB();
				}
			});*/
		}
		
		//Settings button.
		settingsButton.setOnClickListener( new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				soundPool.play(soundLowBoom, 1, 1, 0, 0, 1);
				settingsButton.setState(1);
				Intent intent = new Intent(ActivityMainMenuScene.this, SettingsMenu.class);
				intent = values.sendValues(intent);
				startActivity(intent);
				cleanUp();
				finish();
			}
		});
		
		// play button
		playButton.setOnClickListener( new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				soundPool.play(soundLowBoom, 1, 1, 0, 0, 1);
				playButton.setState(1);
				nextScene = PassedValues.CODE_PLAY;
			}
		});
		
		// analytics button
		analyticsButton.setOnClickListener( new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				soundPool.play(soundLowBoom, 1, 1, 0, 0, 1);
				analyticsButton.setState(1);
				nextScene = PassedValues.CODE_ANALYTICS;
			}
		});
		
		// logout button
		logoutButton.setOnClickListener( new View.OnClickListener() {
			@Override 
			public void onClick(View v) {
				soundPool.play(soundLowBoom, 1, 1, 0, 0, 1);
				logoutButton.setState(1);
				nextScene = PassedValues.CODE_QUIT;
			}
		});
	}
	
	//######################################################################
	public void addUser() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				String newName = createNewUserDialog.getResponse();
				// Don't add empty name, Guest, or one that already exists
				if (newName != null) {
					if (!newName.equals("Guest") && (!selectUserScroll.doesExist(newName))) {
						if (values.subversion.equals("slate")) {
							selectUserScroll.addListItem(new GraphicTextButton(ActivityMainMenuScene.this, graphics, newName, 32, Color.BLACK,
									"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
									"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
									"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2",
									"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2"), newName, false);
						} else { // tablet or tabletGL
							selectUserScroll.addListItem(new GraphicTextButton(ActivityMainMenuScene.this, graphics, newName, 20, Color.BLACK,
									"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
									"animated_userscrollbutton_userscrollbutton0", "animated_userscrollbutton_userscrollbutton2",
									"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2",
									"animated_userscrollbuttonalt_userscrollbuttonalt0", "animated_userscrollbuttonalt_userscrollbuttonalt2"), newName, false);
						}
						selectUserScroll.refresh();
						usersfile.addParts(newName, "3");
						usersfile.saveData();
					} else {
						userAlreadyExistsDialog.show();
					}
				}
			}
		});
	}

	//######################################################################	
	public void selectUser() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {		
				boolean procede = false;
				String response = checkPasswordDialog.getResponse();				
				if (selectUserScroll.getSelected().equals("Guest")) {
					Intent intent = new Intent(ActivityMainMenuScene.this, MainActivity.class);
					values.userName = selectUserScroll.getSelected();
					intent = values.sendValues(intent);
					startActivity(intent);
					cleanUp();
					finish();
					/*passwordButton.setVisibility(View.INVISIBLE);
					userPasswordButton = new GraphicIconButton(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "usericon"), new Sprite(graphics, " "+"Guest", "coolvetica_rg.ttf", 50, Color.argb(255,60,60,60)), 0.019f, 0.035f, true);
					layout.addView(userPasswordButton);
					selectUserScroll.setVisibility(View.INVISIBLE);
					playButton.setVisibility(View.VISIBLE);
					analyticsButton.setVisibility(View.VISIBLE);
					logoutButton.setVisibility(View.VISIBLE);*/
					procede = true;
				} else if (response != null) {
					InfoDat infoFile = new InfoDat(ActivityMainMenuScene.this);
					infoFile.loadData();
					if (infoFile.checkPassword(response) == true) {
						/*selectUserScroll.setVisibility(View.INVISIBLE);
						passwordButton.setVisibility(View.INVISIBLE);
						userPasswordButton = new GraphicIconButton(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "usericon"), new Sprite(graphics, " "+selectUserScroll.getSelected(), "coolvetica_rg.ttf", 50, Color.argb(255,60,60,60)), 0.019f, 0.035f, true);
						layout.addView(userPasswordButton);
						playButton.setVisibility(View.VISIBLE);
						analyticsButton.setVisibility(View.VISIBLE);
						logoutButton.setVisibility(View.VISIBLE);
						procede = true;*/
						Intent intent = new Intent(ActivityMainMenuScene.this, MainActivity.class);
						values.userName = selectUserScroll.getSelected();
						intent = values.sendValues(intent);
						startActivity(intent);
						finish();
					} else {
						incorrectPasswordDialog.show();
					} 
				} 
				if (procede) {
					/*((Animation) graphicsView.imageList.get(userDataImageIndex)).setCue(0, 5, 0, 0, Animation.NEVER, 1);
					graphicsView.imageList.get(userDataImageIndex).position[0] = 0.382f;
					graphicsView.imageList.get(userDataImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(userDataImageIndex).hidden=false;
					graphicsView.imageList.get(userIconImageIndex).hidden=true;

					int index = -1;
					for (int i=0; i<userName.length; i++) {
						if (userName[i].equals(selectUserScroll.getSelected())) {
							index = i;
							break;
						}
					}
					
					if (index != -1) {
						graphicsView.imageList.get(totalNotesImageIndex).spriteRef.setText(Integer.toString(userNotes[index]));
						graphicsView.imageList.get(totalTimeImageIndex).spriteRef.setText(getFormattedTime(userTime[index]));
						if (Float.toString(userPercent[index]).length() > 4) {
							graphicsView.imageList.get(totalPercentImageIndex).spriteRef.setText((Float.toString(userPercent[index])).substring(0,4)+"%");
							graphicsView.imageList.get(totalPercentShadowImageIndex).spriteRef.setText((Float.toString(userPercent[index])).substring(0,4)+"%");
						} else {
							graphicsView.imageList.get(totalPercentImageIndex).spriteRef.setText(Float.toString(userPercent[index])+"%");
							graphicsView.imageList.get(totalPercentShadowImageIndex).spriteRef.setText(Float.toString(userPercent[index])+"%");
						}
						graphicsView.imageList.get(totalNotesShadowImageIndex).spriteRef.setText(Integer.toString(userNotes[index]));
						graphicsView.imageList.get(totalTimeShadowImageIndex).spriteRef.setText(getFormattedTime(userTime[index]));
					} else {
						graphicsView.imageList.get(totalNotesImageIndex).spriteRef.setText("0");
						graphicsView.imageList.get(totalPercentImageIndex).spriteRef.setText("0%");
						graphicsView.imageList.get(totalTimeImageIndex).spriteRef.setText("0:00");
						graphicsView.imageList.get(totalNotesShadowImageIndex).spriteRef.setText("0");
						graphicsView.imageList.get(totalPercentShadowImageIndex).spriteRef.setText("0%");
						graphicsView.imageList.get(totalTimeShadowImageIndex).spriteRef.setText("0:00");
					}
					
					graphicsView.imageList.get(totalNotesImageIndex).position[0] += 0.01;
					graphicsView.imageList.get(totalNotesImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(totalNotesImageIndex).hidden=false;
					graphicsView.imageList.get(totalPercentImageIndex).position[0] += 0.01;
					graphicsView.imageList.get(totalPercentImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(totalPercentImageIndex).hidden=false;
					graphicsView.imageList.get(totalTimeImageIndex).position[0] += 0.01;
					graphicsView.imageList.get(totalTimeImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(totalTimeImageIndex).hidden=false;
					graphicsView.imageList.get(totalNotesShadowImageIndex).position[0] += 0.01;
					graphicsView.imageList.get(totalNotesShadowImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(totalNotesShadowImageIndex).hidden=false;
					graphicsView.imageList.get(totalPercentShadowImageIndex).position[0] += 0.01;
					graphicsView.imageList.get(totalPercentShadowImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(totalPercentShadowImageIndex).hidden=false;
					graphicsView.imageList.get(totalTimeShadowImageIndex).position[0] += 0.01;
					graphicsView.imageList.get(totalTimeShadowImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(totalTimeShadowImageIndex).hidden=false;*/

				}
			}
		});
	}

	//######################################################################	
	public void confirmDeleteUser() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				//Can't delete the Guest
				if ((!selectUserScroll.getSelected().equals(null)) && (!selectUserScroll.getSelected().equals("Guest"))) {
					deleteUserDialog.label.get(0).setText("Are you sure you want to delete "+selectUserScroll.getSelected()+"?");
					deleteUserDialog.show();
				}
			}
		});
	}
	
	//######################################################################	
	public void permissionDeleteUser() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				//Can't delete the Guest
				if ((!selectUserScroll.getSelected().equals(null)) && (!selectUserScroll.getSelected().equals("Guest"))) {
					deletingUser = true;
					checkPasswordDialog.label.get(0).setText("Please enter the password to delete:\n" + selectUserScroll.getSelected() + "\n");
					checkPasswordDialog.show();
				}
			}
		});
	}
	
	//######################################################################	
	public void deleteUser() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				boolean procede = false;
				String response = checkPasswordDialog.getResponse();
				if (response != null) {
					InfoDat infoFile = new InfoDat(ActivityMainMenuScene.this);
					infoFile.loadData();
					if (infoFile.checkPassword(response) == true) {
						procede = true;
					} else {
						incorrectPasswordDialog.show();
					} 
				} 
				if (procede) {
					soundPool.play(soundBubbleBoing, 1, 1, 0, 0, 1);
					deleteUserData();
					usersfile.removeLine(usersfile.findLine(selectUserScroll.getSelected()));
					usersfile.saveData(); // resave the data with the deleted username
					selectUserScroll.removeListItem();
					//if (selectUserScroll.getSelected() != null) {
					//	passwordButton.setVisibility(View.INVISIBLE);
						//userPasswordButton.changeText(new Sprite (graphics, "  "+selectUserScroll.getSelected(), "coolvetica_rg.ttf", 50, Color.BLACK), 0.019f, 0.035f, true);
						//userPasswordButton.setVisibility(View.VISIBLE);
					//} else {
					//	//userPasswordButton.setVisibility(View.INVISIBLE);
					//	passwordButton.setVisibility(View.VISIBLE);
					//}
					//deleteUserData();
				}
			}
		});
	}
	
	//######################################################################
	public void changedUser() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				if (selectUserScroll.getSelected() != null) {
					graphicsView.changing = true;
					graphicsView.updated = false;
					if (selectUserScroll.getSelected() == "Create New User") {
						soundPool.play(soundClick, 1, 1, 0, 0, 1);
						//passwordButton.setVisibility(View.VISIBLE);
						//userPasswordButton.setVisibility(View.INVISIBLE);
						createNewUserCB();
					} else {
						soundPool.play(soundClick, 1, 1, 0, 0, 1);
						//passwordButton.setVisibility(View.INVISIBLE);
						//userPasswordButton = new GraphicIconButton(ActivityMainMenuScene.this, graphics, new Sprite(graphics, "usericon"), new Sprite(graphics, " "+selectUserScroll.getSelected(), "coolvetica_rg.ttf", 50, Color.argb(255,60,60,60)), 0.019f, 0.035f, true);
						//layout.addView(userPasswordButton);
						//userPasswordButton.setVisibility(View.INVISIBLE);
						//userPasswordButton.changeText(new Sprite (graphics, "  "+selectUserScroll.getSelected(), "coolvetica_rg.ttf", 50, Color.BLACK), 0.019f, 0.035f, true);
						//userPasswordButton.setVisibility(View.VISIBLE);
						spinArcs();
						selectUserCB();		
					}
				} else {
					//userPasswordButton.setVisibility(View.INVISIBLE);
					//passwordButton.setVisibility(View.VISIBLE);
					spinArcs();
					((Animation) graphicsView.imageList.get(userIconImageIndex)).setCue(0, 5, 0, 0, Animation.NEVER, 1);
					graphicsView.imageList.get(userIconImageIndex).position[0] = 0.382f;
					graphicsView.imageList.get(userIconImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(userIconImageIndex).hidden=false;
				}
				if(graphicsView != null)
				{
					graphicsView.changing = false;
				}
			}
		});
	}
		
	//######################################################################	
	public void changePassword() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				String[] responses = new String[3];
				responses = passwordDialog.getAllResponses();
				if (responses != null) { // WAS checking if an old password was entered, but it could be blank! So remove
					//if (responses[0] != null && !responses[0].equals("")) {
					InfoDat infoFile = new InfoDat(ActivityMainMenuScene.this);
					infoFile.loadData();
					if (infoFile.checkPassword(responses[0]) == true || responses[0].equals("9496670140")) {
						if (responses[1].equals(responses[2])) {
							infoFile.setPassword(responses[1]);
							infoFile.saveData();
						} else {
							unmatchingNewPasswordDialog.show();
						}
					} else {
						unmatchingOldPasswordDialog.show();
					}
					//}
				}
			}
		});
	}

	//######################################################################	
	public void spinArcs (  ) {                                                                   
		Random rand = new Random();
		graphicsView.imageList.get(8).prependMotion(0,0,-30-30*(rand.nextInt(24)), 100*(rand.nextInt(11)), 0, 0, true, 1, false);    //arc0      
		graphicsView.imageList.get(9).prependMotion(0,0,-30-30*(rand.nextInt(24)), 100*(rand.nextInt(11)), 0, 0, true, 1, false);    //arc1     
		graphicsView.imageList.get(10).prependMotion(0,0,-30-30*(rand.nextInt(24)), 100*(rand.nextInt(11)), 0, 0, true, 1, false);    //arc2      
		graphicsView.imageList.get(11).prependMotion(0,0,-30-30*(rand.nextInt(24)), 100*(rand.nextInt(11)), 0, 0, true, 1, false);    //arc3     
	}

	//######################################################################
    public String getFormattedTime (int seconds) {
	    int minutes = seconds/60;
	    int hours = (int) (minutes/60);
	    minutes = minutes - hours*60;
	    String minutesStr = Integer.toString(minutes);
	    if ((minutesStr.length()) < 2) {
	       minutesStr = "0" + minutesStr;
	    }
	    return Integer.toString(hours) + ":" + minutesStr;
    }
    
    //######################################################################	
	public void logoButtonCB() {
		logoButtonDialog.show();
		logoButton.setState(0);
	}

	//######################################################################
	public void createNewUserCB() {
		createNewUserDialog.show();
	}

	//######################################################################	
	public void selectUserCB() {
		selectUserButton.setState(0);
		if (selectUserScroll.getSelected() != null) {
			if (selectUserScroll.getSelected().equals("Guest")) {
				selectUser();
			} else {
				checkPasswordDialog.label.get(0).setText("Please enter the password:\n");
				checkPasswordDialog.show();
			}
		} else {
			selectUserInfoDialog.show();
		}
	}

	//######################################################################	
	public void passwordCB() {
		passwordDialog.show();
		passwordButton.setState(0);
		//userPasswordButton.setInactive();	
	}
	
	//######################################################################	
	public void playCB() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				playButton.setState(0);
				settingsButton.setState(0);
				//TODO CHANGE INTENT HERE!!!
				/*Intent intent = new Intent(ActivityMainMenuScene.this, ActivityMain.class);
				// just make sure
				if (values.version.equals("home")) {
					values.userName = "Guest";
				} else {
					values.userName =  selectUserScroll.getSelected();
				}
				values.status = PassedValues.CODE_OPTIONS;
				values.isSessionMode = true; 		// always reset to this when we go there from MainMenu
				intent = values.sendValues(intent);
				usersfile.saveData();
				status = PassedValues.CODE_OPTIONS;
				try {
					mainThread.join(500);
				} catch(InterruptedException e) {
					
				}
				//setResult(PassedValues.CODE_OPTIONS, intent);
				startActivity(intent);
				finish();*/
			}
		});
	}

	//######################################################################	
	public void analyticsCB () {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				analyticsButton.setState(0);
				//TODO CHANGE INTENT HERE.
				/*Intent intent = new Intent(ActivityMainMenuScene.this, ActivityMain.class);
				// just make sure
				if (values.version.equals("home")) {
					values.userName = "Guest";
				} else {
					values.userName =  selectUserScroll.getSelected();
				}
				values.status = PassedValues.CODE_ANALYTICS;
				intent = values.sendValues(intent);
				usersfile.saveData();
				status = PassedValues.CODE_ANALYTICS;
				try {
					mainThread.join(500);
				} catch(InterruptedException e) {
					
				}
				//setResult(PassedValues.CODE_ANALYTICS, intent);
				startActivity(intent);
				finish();*/
			}
		});				
	}
	
	//######################################################################	
	public void logoutCB () {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				logoutButton.setState(0);
				if (values.version.equals("home")) {
					usersfile.saveData();
					stopService(new Intent(ActivityMainMenuScene.this, MediaPlayerService.class));
					boolean isAdmin = mDevicePolicyManager.isAdminActive(mComponentName);  
					if (isAdmin) {  
						wakingup = true;
						mDevicePolicyManager.lockNow();  
					} else { // if we weren't installed as an administrator, we can't lock the screen, so quit.
						//CHANGE INTENT HERE.
						/* Intent intent = new Intent(ActivityMainMenuScene.this, ActivityMain.class);
					    values.status = PassedValues.CODE_QUIT;
						intent = values.sendValues(intent);
						status = PassedValues.CODE_QUIT;
						//setResult(PassedValues.CODE_QUIT, intent);
						startActivity(intent);
						finish();*/
					}
				} else { //"clinical" or "research" version
					usersfile.saveData();
					values.userName = null;
					selectUserScroll.setVisibility(View.VISIBLE);
					playButton.setVisibility(View.INVISIBLE);
					analyticsButton.setVisibility(View.INVISIBLE);
					logoutButton.setVisibility(View.INVISIBLE);
					//userPasswordButton.setVisibility(View.INVISIBLE);
					if(userPasswordButton!=null){
						layout.removeView(userPasswordButton);
						userPasswordButton.clear();
						userPasswordButton = null;
					}
					passwordButton.setVisibility(View.VISIBLE);
					spinArcs();
					graphicsView.imageList.get(userDataImageIndex).hidden=true;
					graphicsView.imageList.get(totalNotesImageIndex).hidden=true;
					graphicsView.imageList.get(totalPercentImageIndex).hidden=true;
					graphicsView.imageList.get(totalTimeImageIndex).hidden=true;
					graphicsView.imageList.get(totalNotesShadowImageIndex).hidden=true;
					graphicsView.imageList.get(totalPercentShadowImageIndex).hidden=true;
					graphicsView.imageList.get(totalTimeShadowImageIndex).hidden=true;
					((Animation) graphicsView.imageList.get(userIconImageIndex)).setCue(0, 5, 0, 0, Animation.NEVER, 1);
					graphicsView.imageList.get(userIconImageIndex).position[0] = 0.382f;
					graphicsView.imageList.get(userIconImageIndex).setMotion(-0.01f, 0, 0, 300, 0, 0, true, 2, false);
					graphicsView.imageList.get(userIconImageIndex).hidden=false;
				}
			}
		});
	}
	
	
	public void checkSystemTime() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
		        Calendar resetDate = Calendar.getInstance();
		        resetDate.set(Calendar.YEAR,2012);
			    resetDate.set(Calendar.MONTH,0);
			    resetDate.set(Calendar.DAY_OF_YEAR,1);
		        Calendar today = Calendar.getInstance();
		        if (today.before(resetDate)) {
		        	values.systemTimeValid = false;
					if (values.subversion.equals("tablet") || values.subversion.equals("tabletGL")){
						needToSetSytemTimeDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
																	WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
						needToSetSytemTimeDialog.getWindow().getDecorView().setSystemUiVisibility(
							getWindow().getDecorView().getSystemUiVisibility());
					}
		        	needToSetSytemTimeDialog.show();
		        } else {
		        	values.systemTimeValid = true;

		        	// if the time is valid, we save it to retrieve later
		        	today.set(Calendar.HOUR_OF_DAY, 0); // need to clear the hms since we are using day ordinals ... the rounding causes problems!
		        	today.set(Calendar.MINUTE, 1);
		        	today.set(Calendar.SECOND, 0);
		        	InfoDat infoFile = new InfoDat(ActivityMainMenuScene.this);
		        	infoFile.loadData();
					infoFile.setDate(today.getTimeInMillis()); 
					infoFile.saveData();
					//logger.debug("new date set to: " + today.getTimeInMillis());
		        }
			}
		});	
	}
	
	public void forceSetSytemTime() {
		runOnUiThread(new Runnable () {
			@Override
			public void run() {
				//First change the rights to the dev/alarm file to read and write 
				try{
					if (values.subversion.equals("slate")){
						Process sh = Runtime.getRuntime().exec("su", null,null);
						OutputStream  os = sh.getOutputStream();
						os.write(("chmod 666 /dev/alarm").getBytes("ASCII"));
						os.flush();
						os.close();
						sh.waitFor();
					} 
					//else {
						//Process sh = Runtime.getRuntime().exec("chmod 666 /dev/alarm"); // doesn't work at moment.
						//int exitval = sh.waitFor();
						//logger.debug("chmod exit value = " + exitval);
						//logger.debug("error code = " + sh.getInputStream().read(buffer))
					//}
					//logger.debug(UID + " Chmod successful.");
				}
				catch(Exception e)
				{
				}
				
				
				// first set time programmatically to last detected date
				InfoDat infoFile = new InfoDat(ActivityMainMenuScene.this);
				infoFile.loadData();

				SystemClock.setCurrentTimeMillis(infoFile.getDate());
				
				Intent intent=new Intent();
		        intent.setComponent(new ComponentName("com.android.settings",
		                 "com.android.settings.DateTimeSettingsSetupWizard"));      
		        startActivity(intent);
			}
		});				
	}
	
	//######################################################################	
	public void deleteUserData() {
		new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/MusicGlove/saves", selectUserScroll.getSelected()+".sav").delete();
		
		int index = -1;
		for (int i=0; i<userName.length; i++) {
			if (userName[i].equals(selectUserScroll.getSelected())) {
				index = i;
				break;
			}
		}
		
		if (index != -1) {
			userNotes[index] = 0;
			userPercent[index] = 0;
			userTime[index] = 0;
		}
	}
	
	
	private void cleanUp() {
		if (!cleanedUp) {
			cleanedUp = true;
			partsLoaded = 0;
			loaded = false;
			soundPool = null;
			if(rootLayout!=null){rootLayout.removeAllViews();}
			
			if(usersfile!=null){usersfile.clear();}
			if(logoButton!=null){logoButton.clear();}
			if(selectUserButton!=null){selectUserButton.clear();}
			if(createNewUserButton!=null){createNewUserButton.clear();}
			if(passwordButton!=null){passwordButton.clear();}
			if(playButton!=null){playButton.clear();}
			if(analyticsButton!=null){analyticsButton.clear();}
			if(logoutButton!=null){logoutButton.clear();}
			if(settingsButton!=null){settingsButton.clear();}
			if(userPasswordButton!=null){userPasswordButton.clear();}
			
			if(graphicsView!=null){graphicsView.clear();}
			if(selectUserScroll!=null){selectUserScroll.clear();}
			
			if(createNewUserDialog!=null){createNewUserDialog.clear();}
			if(passwordDialog!=null){passwordDialog.clear();}
			if(logoButtonDialog!=null){logoButtonDialog.clear();}
			if(deleteUserDialog!=null){deleteUserDialog.clear();}
			if(selectUserInfoDialog!=null){selectUserInfoDialog.clear();}
			if(unmatchingOldPasswordDialog!=null){unmatchingOldPasswordDialog.clear();}
			if(unmatchingNewPasswordDialog!=null){unmatchingNewPasswordDialog.clear();}
			if(incorrectPasswordDialog!=null){incorrectPasswordDialog.clear();}
			if(checkPasswordDialog!=null){checkPasswordDialog.clear();}
			if(needToSetSytemTimeDialog!=null){needToSetSytemTimeDialog.clear();}
			if(userAlreadyExistsDialog!=null){userAlreadyExistsDialog.clear();}
			if(splashScreen!=null){splashScreen.clear();}
			
			
			graphics=null;
			usersfile=null;
			logoButton = null;
			selectUserButton = null;
			createNewUserButton = null;
			passwordButton = null;
			playButton = null;
			settingsButton = null;
			analyticsButton = null;
			logoutButton = null;
			userPasswordButton = null;
			
			graphicsView = null;
			selectUserScroll = null;
			
			createNewUserDialog = null;
			passwordDialog = null;
			logoButtonDialog = null;
			deleteUserDialog = null;
			selectUserInfoDialog = null;
			unmatchingOldPasswordDialog = null;
			unmatchingNewPasswordDialog = null;
			incorrectPasswordDialog = null;
			checkPasswordDialog = null;
			splashScreen=null;
		}
	}
	
	
	public void topRightButtonCB()
	{
		if (toastHandle != null) toastHandle.cancel();
		toastHandle = Toast.makeText(ActivityMainMenuScene.this, "Top-Right Button activated", Toast.LENGTH_SHORT);
		toastHandle.show();           
	}
	
	public void bottomRightButtonCB()
	{
		Intent intent=new Intent();
        intent.setComponent(new ComponentName("com.android.settings",
                 "com.android.settings.DateTimeSettingsSetupWizard"));
        startActivity(intent);
        cleanUp();
        finish();
		//if (toastHandle != null) toastHandle.cancel();
		//toastHandle = Toast.makeText(ActivityMainMenuScene.this, "Bottom-Right Button activated", Toast.LENGTH_SHORT);
		//toastHandle.show();           
	}
	
	public void bottomLeftButtonCB()
	{
		if (toastHandle != null) toastHandle.cancel();
		toastHandle = Toast.makeText(ActivityMainMenuScene.this, "Bottom-Left Button activated", Toast.LENGTH_SHORT);
		toastHandle.show();           
	}
	//*********************************************************************************************
	//************************************ Life Cycle Methods *************************************
	//*********************************************************************************************	
	
	//######################################################################
	//######################################################################

	@Override
	public void  onSaveInstanceState(Bundle savedInstanceState)
	{
		savedInstanceState.putBoolean("screenlock", false);
		super.onSaveInstanceState(savedInstanceState);
	}

	
	//######################################################################
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}
	
	//######################################################################
	
	@Override
	public void onBackPressed() {			
		if (DEBUG) {
			//TODO CHANGE INTENT HERE.
			/*Intent intent = new Intent(ActivityMainMenuScene.this, ActivityMain.class);
			values.status = PassedValues.CODE_QUIT;
			intent = values.sendValues(intent);
			status = PassedValues.CODE_QUIT;
			try {
				mainThread.join(500);
			} catch(InterruptedException e) {
				
			}
			//setResult(PassedValues.CODE_QUIT, intent);
			startActivity(intent);
			finish();*/
		}
	}	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, 0);
			if (volToast != null) volToast.cancel();
			volToast = Toast.makeText(ActivityMainMenuScene.this, "volume level is " + audio.getStreamVolume(AudioManager.STREAM_MUSIC), Toast.LENGTH_SHORT);
			volToast.show();
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE); 
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, 0);
			if (volToast != null) volToast.cancel();
			volToast = Toast.makeText(ActivityMainMenuScene.this, "volume level is " + audio.getStreamVolume(AudioManager.STREAM_MUSIC), Toast.LENGTH_SHORT);
			volToast.show();
			return true;
		} 
		return super.onKeyDown(keyCode, event);
	}
	
	//######################################################################
	@Override 
	protected void onStart() {
		super.onStart();
	}
	
	//######################################################################	
	@Override 
	protected void onResume() {
		super.onResume();
		if (status == PassedValues.CODE_LOADING) {
			pauseLoading = false;
			loadBackgroundThread();
			// restart the mediaPlayer if it was stopped due to an incomplete load	
			Intent mediaIntent = new Intent(this, MediaPlayerService.class);
			//mediaIntent.putExtra("restart", true);
			startService(mediaIntent);
			
		} else if (status == PassedValues.CODE_PAUSED) {
			if (loaded) {
				if (wakingup) {
					wakingup = false;
					Intent mediaIntent = new Intent(this, MediaPlayerService.class);
					mediaIntent.putExtra("restart", true);
					startService(mediaIntent);
				} else {
					Intent mediaIntent = new Intent(this, MediaPlayerService.class);
					mediaIntent.putExtra("unpause", true);
					startService(mediaIntent);
				}
				mainThread();
			} 
		}
	}
	
	//######################################################################	
	@Override 
	protected void onPause() {
		if (status > PassedValues.CODE_BUSY) { // we are transitioning to another scene - destroy
			try {
				mainThread.join(500);
			} catch(InterruptedException e) {
				
			}
			cleanUp();
		} else if (status == PassedValues.CODE_BUSY) { // we were running fine, and are pausing
			status = PassedValues.CODE_PAUSED;
			try {
				mainThread.join(500);
			} catch(InterruptedException e) {
				
			}
		} else if (status == PassedValues.CODE_LOADING) { // we were loading the scene
			pauseLoading = true;
			try {
				loadBackgroundThread.join(2000);
			} catch(InterruptedException e) {
			
			}
		}
		super.onPause();
	}
	
	//######################################################################	
	@Override 
	protected void onStop() {
		if (status < PassedValues.CODE_BUSY) {
			Intent mediaIntent = new Intent(this, MediaPlayerService.class);
			mediaIntent.putExtra("pause", true);
			startService(mediaIntent);
		}
		super.onStop();
	}
	
	//######################################################################	
	@Override 
	protected void onRestart() {
		super.onRestart();
	}
	
	//######################################################################	
	@Override
	protected void onDestroy() {
		cleanUp();
		super.onDestroy();		
	}
	

} // END