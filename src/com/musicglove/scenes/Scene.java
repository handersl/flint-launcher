package com.musicglove.scenes;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.musicglove.graphics.Graphics;
import com.musicglove.helper.PassedValues;

// The base scene class, sets the landscape, shape of the screen, etc.
public class Scene extends Activity {
	protected Canvas canvas;
	protected Paint paint;
	protected BitmapFactory.Options opts;
	protected Resources r;
	protected Graphics graphics;
		
	protected LinearLayout rootLayout;
	protected LinearLayout layoutSpacer1;
	protected LinearLayout layoutContainer;
	protected LinearLayout layoutSpacer2;
	protected RelativeLayout layout;
	
	private PassedValues values;
	private boolean immersive;
	
	
	//######################################################################
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getIntent().getExtras() != null) { 
			values = new PassedValues();
			values.receiveValues(getIntent().getExtras());
			if (values.subversion != null) // have to check in case this gets called from a USB receive intent
				if ((values.subversion.equals("tablet") || values.subversion.equals("tabletGL")) && (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)) {  // tablet
					immersive = true;
				} else {  // slate
					immersive = false;
				}
			else {
				immersive = true; // just set it to true if subversion was not initialized.
			}
		} else {
			immersive = false;
		}
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LOW_PROFILE 
				| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
		if (immersive) {
			this.getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		}
		//this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		UiChangeListener();
		
		canvas = new Canvas();
		paint = new Paint();
		opts = new BitmapFactory.Options();
		opts.inScaled = false;
		r = getResources();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
			super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			this.getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LOW_PROFILE 
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
			if (immersive) {
				this.getWindow().getDecorView().setSystemUiVisibility(
						View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
			}
		}
	}
	
	public void UiChangeListener() // calling this function adds a listener to reset immersive mode after hardware volume buttons are pressed
    {
        final View decorView = getWindow().getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LOW_PROFILE 
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
                    if (immersive) {
                    	decorView.setSystemUiVisibility(
                    			View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                				| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                    }
                } else {
                	// here the nav bar just dissapeared
                }
            }
        });
    }
	
	protected void setupGraphics(Graphics inGraphics) {
		graphics = inGraphics;
		rootLayout = new LinearLayout(this);
		layoutSpacer1 = new LinearLayout(this);
		layoutContainer = new LinearLayout(this);
		layoutSpacer2 = new LinearLayout(this);
		layout = new RelativeLayout(this);
		
		rootLayout.setBackgroundColor(Color.BLACK);
		layoutSpacer1.setBackgroundColor(Color.BLACK);
		layoutContainer.setBackgroundColor(Color.BLACK);
		layoutSpacer2.setBackgroundColor(Color.BLACK);	
		layout.setBackgroundColor(Color.BLACK);
		
		if ( graphics.orientation == "portrait" ) {
			rootLayout.setOrientation(LinearLayout.VERTICAL);
			LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
			rootLayout.setLayoutParams(Lparams);
			Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, graphics.spacer);
			rootLayout.addView(layoutSpacer1, Lparams);
			layoutSpacer1.setBackgroundColor(Color.BLACK);
			Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, graphics.height);
			rootLayout.addView(layoutContainer, Lparams);
			Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, graphics.spacer);
			rootLayout.addView(layoutSpacer2, Lparams);
			layoutSpacer2.setBackgroundColor(Color.BLACK);		
		} else if ( graphics.orientation == "landscape" ) {
			rootLayout.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
			rootLayout.setLayoutParams(Lparams);
			Lparams = new LinearLayout.LayoutParams(graphics.spacer, LinearLayout.LayoutParams.MATCH_PARENT);
			rootLayout.addView(layoutSpacer1, Lparams);
			layoutSpacer1.setBackgroundColor(Color.BLACK);
			Lparams = new LinearLayout.LayoutParams(graphics.width, LinearLayout.LayoutParams.MATCH_PARENT);
			rootLayout.addView(layoutContainer, Lparams);
			Lparams = new LinearLayout.LayoutParams(graphics.spacer, LinearLayout.LayoutParams.MATCH_PARENT);
			rootLayout.addView(layoutSpacer2, Lparams);
			layoutSpacer2.setBackgroundColor(Color.BLACK);
		} else { // orientation is High definition
			LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
			rootLayout.setLayoutParams(Lparams);
			rootLayout.addView(layoutContainer, Lparams);
		}
		layoutContainer.addView(layout);
	}
	

	

	
	
} // END
