package com.musicglove.graphics;

public class AnimationCue {       
	public int timer;
	public int startFrame;
	public int endFrame;
	public int loop;
	public int proceed;
	public int step;
	
	final static int NEVER = 0;                            
	final static int ON_LOOP = 1;                
	final static int ON_TIME = 2;                  

	//######################################################################
	public AnimationCue ( int instartFrame, int inendFrame, int intime, int inloop, int inproceed, int instep ) {         
		timer = intime;                                                          
		startFrame = instartFrame;                                                        
		endFrame = inendFrame;                                                              
		loop = inloop;                                                                          
		proceed = inproceed;                                                                
		step = instep;                                                                         
	}
} // END


